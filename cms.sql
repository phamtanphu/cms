-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 19, 2016 at 11:21 AM
-- Server version: 5.6.24
-- PHP Version: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` varchar(512) DEFAULT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `order` int(4) NOT NULL DEFAULT '0',
  `status` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `description`, `parent`, `order`, `status`) VALUES
(1, 'Chưa Phân Loại', 'Chua-Phan-Loai', '', 0, 0, 1),
(2, 'Thời Sự', 'Thoi-Su', '', 0, 0, 1),
(3, 'Thế Giới', 'The-Gioi', '', 0, 0, 1),
(4, 'Kinh Doanh', 'Kinh-Doanh', '', 0, 0, 1),
(5, 'Giải Trí', 'Giai-Tri', '', 0, 0, 1),
(6, 'Thể Thao', 'The-Thao', '', 0, 0, 1),
(7, 'Pháp Luật', 'Phap-Luat', '', 0, 0, 1),
(8, 'Giáo Dục', 'Giao-Duc', '', 0, 0, 1),
(9, 'Sức Khỏe', 'Suc-Khoe', '', 0, 0, 1),
(10, 'Gia Đình', 'Gia-Dinh', '', 0, 0, 1),
(11, 'Du Lịch', 'Du-Lich', '', 0, 0, 1),
(12, 'Khoa Học', 'Khoa-Hoc', '', 0, 0, 1),
(13, 'Số Hóa', 'So-Hoa', '', 0, 0, 1),
(14, 'Xe', 'Xe', '', 0, 0, 1),
(15, 'Cộng Đồng', 'Cong-Dong', '', 0, 0, 1),
(16, 'Tâm Sự', 'Tam-Su', '', 0, 0, 1),
(17, 'Video', 'Video', '', 0, 0, 1),
(18, 'Cười', 'Cuoi', '', 0, 0, 1),
(19, 'Rao Vặt', 'Rao-Vat', '', 0, 0, 1),
(20, 'Cửa Sổ Blog', 'Cua-So-Blog', '', 16, 0, 1),
(21, 'Lời Yêu', 'Loi-Yeu', '', 16, 0, 1),
(22, 'Tổ Ấm', 'To-Am', '', 10, 0, 1),
(23, 'Chăm Con', 'Cham-Con', '', 10, 0, 1),
(24, 'Nhà Đẹp', 'Nha-Dep', '', 10, 0, 1),
(25, 'Tiêu Dùng', 'Tieu-Dung', '', 10, 0, 1),
(26, 'Nội Trợ', 'Noi-Tro', '', 10, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `target` int(2) NOT NULL DEFAULT '1',
  `icon` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `css` varchar(255) DEFAULT NULL,
  `order` int(2) NOT NULL DEFAULT '0',
  `parent` int(11) NOT NULL DEFAULT '0',
  `root` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=154 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `title`, `url`, `target`, `icon`, `thumbnail`, `css`, `order`, `parent`, `root`, `status`) VALUES
(142, 'main-menu', NULL, 1, NULL, NULL, NULL, 0, 0, 0, 1),
(143, 'footer-menu', NULL, 1, NULL, NULL, NULL, 0, 0, 0, 1),
(144, 'Home', 'l', 1, 'kl', 'kl', '', 2, 143, 143, 1),
(145, 'Introduction', 'l', 1, '1', 'kl', '', 1, 143, 143, 1),
(152, 'Trang Chủ', 'base_url()', 1, '', '', '', 1, 142, 142, 1),
(153, 'Giới Thiệu', '/', 1, '', '', '', 2, 142, 142, 1);

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE IF NOT EXISTS `options` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `group` varchar(32) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `name`, `value`, `group`, `status`) VALUES
(1, 'useragent', 'CodeIgniter', 'email', 0),
(2, 'protocol', 'smtp', 'email', 0),
(3, 'mailpath', '/usr/sbin/sendmail', 'email', 0),
(4, 'smtp_host', 'ssl://smtp.gmail.com', 'email', 0),
(5, 'smtp_user', 'phamphu232@gmail.com', 'email', 0),
(6, 'smtp_pass', 'yxhslvapwbvxqsfp1', 'email', 0),
(9, 'smtp_port', '465', 'email', 0),
(10, 'smtp_timeout', '7', 'email', 0),
(11, 'wordwrap', '', 'email', 0),
(12, 'wrapchars', '', 'email', 0),
(13, 'mailtype', 'text', 'email', 0),
(14, 'charset', '', 'email', 0),
(15, 'validate', 'TRUE', 'email', 0),
(16, 'priority', '', 'email', 0),
(17, 'crlf', '', 'email', 0),
(18, 'newline', '\\r\\n', 'email', 0),
(19, 'bcc_batch_mode', '', 'email', 0),
(20, 'bcc_batch_size', '', 'email', 0);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `detail` text,
  `type` int(2) NOT NULL DEFAULT '1' COMMENT '1:text; 2:video; 3:audio; 4:image',
  `created_user` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edit_user` int(11) NOT NULL DEFAULT '0',
  `edit_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `viewed` int(11) NOT NULL DEFAULT '0',
  `status` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `slug`, `thumbnail`, `description`, `detail`, `type`, `created_user`, `created_date`, `edit_user`, `edit_date`, `viewed`, `status`) VALUES
(1, 'Dấu hiệu vợ bạn đang ngoại tình', 'Dau-hieu-vo-ban-dang-ngoai-tinh', '', 'Nếu cô ấy kể rất nhiều về một người bạn mới nào đó tên Mai thì rất có thể cô ấy thực sự đang nói về một người tên Mạnh nào đó....', '<p class="subtitle">Thay đổi thái độ với các mối quan hệ xã hội</p>\r\n<p class="Normal">Quan sát cách cô ấy đối xử với những người quan tâm đến bạn.</p>\r\n<p class="Normal">Nếu vợ bạn dần trở nên xa cách với những thành viên trong gia đình bạn hoặc bạn bè bạn, người mà cô ấy từng rất thân thiết, đó là dấu hiệu cần chú ý.</p>\r\n<p class="Normal">- Nếu cô ấy thân thiết với gia đình và bạn bè bạn, cô ấy sẽ hiểu rằng những người thân cận sẽ bị tổn thương bởi vụ bê bối này chứ không chỉ riêng bạn. Điều này sẽ khiến cô ấy cảm thấy tội lỗi hơn nên thường muốn tránh mặt càng nhiều càng tốt.</p>\r\n<p class="Normal">- Cô ấy lo ngại những người này sẽ nói cho bạn biết sự thật cho dù bạn không hề biết được điều gì đang diễn ra.</p>\r\n<p class="Normal">- Đôi khi, gia đình là một phần nguyên nhân của vấn đề, chứ không chỉ là ngoại tình. Nếu vợ bạn không có mối quan hệ tốt với gia đình chồng, bạn cũng coi trọng ý kiến của họ hơn là quan tâm đến suy nghĩ của cô ấy, thì cô ấy sẽ cảm thấy rằng không hề có đồng minh, và muốn lánh mặt.</p>\r\n<table class="tplCaption" border="0" cellspacing="0" cellpadding="3" align="center">\r\n<tbody>\r\n<tr>\r\n<td><img src="http://img.f13.giadinh.vnecdn.net/2016/01/03/ngoaitinh-8544-1451826424.jpg" alt="dau-hieu-vo-ban-dang-ngoai-tinh" data-natural-width="500" data-width="500" data-pwidth="470.40625"></td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p class="Image">Ảnh: <em>tellyouall.com.</em></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p class="subtitle">Cô ấy thường xuyên đi chơi riêng</p>\r\n<p class="Normal">Nếu cô ấy bỗng nhiên dành nhiều thời gian đi chơi với bạn bè riêng mà không có bạn, đó có thể là dấu hiệu xấu. Ngay cả khi cô ấy nói rằng mình đi chơi với nhóm bạn gái thân thì sự thật chưa chắc đã là như vậy.</p>\r\n<p class="Normal">- Việc thỉnh thoảng gặp gỡ nhóm bạn gái thân là điều hoàn toàn bình thường, vì vậy, bạn không cần quá lo lắng nếu việc này không diễn ra thường xuyên.</p>\r\n<p class="Normal">- Mọi việc sẽ trở nên nghiêm trọng hơn nếu cô ấy đặt việc này lên trên cả mối quan hệ với bạn, nếu cô ấy thay đổi câu chuyện về việc gặp ai, đi đâu hoặc bạn không thể liên lạc khi cô ấy ra ngoài.</p>\r\n<p class="Normal">- Kể cả cô ấy thực sự đi gặp các bạn gái, có khả năng cô ấy tìm kiếm sự ủng hộ về mặt tinh thần và sẵn sàng chia sẻ những chuyện riêng tư mà không thể thú nhận với bạn. Đó chưa chắc là dấu hiệu cô ấy lừa dối bạn nhưng là dấu hiệu cuộc hôn nhân của bạn có vấn đề.</p>\r\n<p class="Normal">- Nếu cô ấy nói cần ra ngoài mà bạn cảm thấy nghi ngờ cô ấy đi gặp một người đàn ông khác, hãy đề nghị đi cùng cô ấy. Nếu cô ấy thay đổi quyết định không đi nữa, đó có thể là dấu hiệu cô ấy muốn một mình dành thời gian cho ai đó.</p>\r\n<p class="Normal">- Nếu vợ bạn biến mất nhiều giờ liền và khẳng định mình chỉ đi mua sắm, hãy thăm dò bằng các câu hỏi kiểu như có thấy món đồ nào thú vị không, ghé thăm cửa hàng nào… đặc biệt khi cô ấy đi về tay không. Phụ nữ thường thích kể lể về chuyện mua sắm nhưng nếu cô ấy thoái thác và né tránh câu hỏi này thì chứng tỏ cô ấy đã dành thời gian làm việc khác.</p>\r\n<p class="subtitle">Lắng nghe khi cô ấy nói về người bạn mới</p>\r\n<p class="Normal">Phụ nữ luôn hào hứng chia sẻ về những điều mới mẻ và thú vị trong cuộc sống, ngay cả đó là chuyện cô ấy muốn giữ bí mật. Nếu cô ấy nói về một người bạn mới, có thể đó không chỉ đơn thuần là một người bạn.</p>\r\n<p class="Normal">- Vợ bạn có thể bịa ra một tên giả để đánh lạc hướng bạn. Nếu cô ấy kể rất nhiều về một người nào đó tên Mai thì rất có thể cô ấy thực sự đang nói về một người tên Mạnh nào đó, cái tên chỉ để tránh sự nghi ngờ từ bạn mà thôi.</p>\r\n<p class="subtitle">Thay đổi về thói quen uống bia rượu và hút thuốc</p>\r\n<p class="Normal">Nếu cô ấy là người có tửu lượng tốt hoặc hút thuốc thì bạn sẽ khó nhận ra sự thay đổi. Nhưng trước đây cô ấy không bao giờ hứng thú với rượu bia hay thuốc lá mà lại bắt đầu tập hút thuốc hay uống rượu, có thể ai đó đang truyền cho cô ấy thói quen này.</p>\r\n<p class="Normal">- Ngay cả khi vợ bạn thỉnh thoảng cũng uống vài ly rượu, hãy chú ý đến hơi men trên cơ thể cô ấy. Nếu bạn ngửi thấy mùi thuốc lá hay rượu từ cô ấy sau khi trở về từ buổi làm tăng ca hay những thời điểm bất thường như giữa buổi chiều, hẳn là cô ấy đang che giấu điều gì đó.</p>\r\n<p class="subtitle">Chú ý đến mật độ làm việc và đi công tác</p>\r\n<p class="Normal">Cô ấy thường viện lý do làm thêm giờ trong khi không phải là người nghiện công việc thì có lẽ cô ấy dành thời gian ở nơi khác chứ không phải công sở.</p>\r\n<p class="Normal">- Bất kể thời gian làm việc gì đó dài hơn dự kiến đều có thể là tin xấu. Chẳng hạn như thời gian đi chợ, đi ngân hàng, đi làm tóc kéo dài hơn bình thường thì cô ấy có thể tận dụng để làm việc khác.</p>\r\n<p class="Normal">- Cân nhắc những chuyến đi xa nhà mà không liên quan đến công việc. Chẳng hạn như bỗng nhiên cô ấy nổi hứng đi thăm họ hàng ở xa mà không có bạn đi cùng, đặc biệt là ở khách sạn thay vì ở nhà người thân. Nếu cô ấy ở khách sạn, hãy tế nhị hỏi tên khách sạn, sau đó kiểm tra hóa đơn thẻ tín dụng. Nếu bạn không thấy khoản phí nào xuất hiện, hẳn là người khác đã chi trả khoản phí đó.</p>\r\n<p class="subtitle">Xem xét khoản tiền cô ấy chi tiêu khi đi ra ngoài</p>\r\n<p class="Normal">Ngoại tình thường dẫn đến nhiều khoản lệ phí tốn kém. Bạn hãy kín đáo kiểm tra các hóa đơn, sao kê thẻ ngân hàng. Nếu số tiền chi tiêu vượt trội so với bình thường thì có thể cô ấy đang dùng tiền mua sắm cho ai đó.</p>\r\n<p class="Normal">- Cuối cùng, hãy kiểm tra đồng hồ cây số trên xe máy/ô tô của cô ấy. Số cây số vượt trội có thể tố cáo việc cô ấy không chỉ đi từ nhà đến công sở và ngược lại.</p>\r\n<p class="subtitle">Thay đổi hành vi trong gia đình</p>\r\n<p class="Normal">Xem lại sự thay đổi tình cảm dành cho bạn</p>\r\n<p class="Normal">Bạn có thể cảm thấy nghi ngờ khi cô ấy trở nên xa cách bạn hơn nhưng ngược lại, nếu cô ấy tỏ ra thân mật hơn với bạn một cách khó hiểu, thì đó có thể dấu hiệu xấu.</p>\r\n<p class="Normal">- Vợ bỗng dưng quan tâm hơn đến bạn là cách để giảm bớt cảm giác tội lỗi khi đang ngoại tình. Nếu cô ấy tỏ ra chú ý hơn khi hai người ở cùng nhau, cô ấy chỉ cố đóng vai người vợ tuyệt vời trong mắt bạn và che giấu chuyện bên ngoài.</p>\r\n<p class="subtitle">Chú ý đến thói quen dùng điện thoại</p>\r\n<p class="Normal">Phụ nữ thường dành nhiều thời gian dùng điện thoại nhưng những thay đổi đột ngột như cô ấy dành nhiều thời gian nói chuyện, nhắn tin hơn và ngay lập tức ngừng lại khi bạn xuất hiện, bạn hãy chú ý bởi có thể cô ấy đang có ai khác ngoài bạn.</p>\r\n<p class="Normal">- Hãy thử hỏi ai gọi điện thoại cho vợ bạn, đặc biệt là khi cô ấy phải ra ngoài nghe điện hoặc dập máy ngay khi bạn bước vào phòng. Xem xét thái độ và phản ứng của cô ấy khi trả lời.</p>\r\n<p class="Normal">- Bạn cũng nên chú ý đến mật độ sử dụng điện thoại, thời gian sử dụng và giọng điệu khi nói chuyện.</p>\r\n<p class="subtitle">Vợ bạn hay bắt lỗi bạn</p>\r\n<p class="Normal">Nhiều ông chồng nghĩ rằng vợ thường hay càu nhàu, than vãn nhưng nếu vợ bạn bỗng trở nên khó tính và hay phê bình hành động của bạn, đó là tâm lý của người ngoại tình đang tìm cách bào chữa cho hành động tội lỗi của mình.</p>\r\n<p class="Normal">- Nếu cô ấy bỗng nhiên cư xử như thể mọi điều bạn làm điều không đúng đắn trong mắt cô ấy, đó là vấn đề nghiêm trọng cần xem xét, ngay cả khi cô ấy không ngoại tình.</p>\r\n<p class="Normal">- Mặt khác, nếu cô ấy không còn ca cẩm về bạn nữa cũng là dấu hiệu cần lưu ý. Nếu bạn có thể đưa ra lý do thuyết phục, chẳng hạn như, bạn thay đổi và cải thiện những tật xấu mà cô ấy thường phàn nàn thì điều này thật tuyệt. Nhưng nếu bạn vẫn vậy thì chứng tỏ cô ấy trở nên hờ hững, thiếu quan tâm hơn tới bạn. Cô ấy đã tìm được sự bù đắp tình cảm ở một người đàn ông khác.</p>\r\n<p class="subtitle">Thay đổi thói quen sinh hoạt tình dục</p>\r\n<p class="Normal">Bạn thường nghĩ rằng dấu hiệu ngoại tình là nửa kia ít gần gũi với bạn hơn. Nhưng không phải lúc nào cũng vậy, đôi khi, cô ấy tỏ ra hứng thú hơn với chuyện ấy hơn trước đây cũng là dấu hiệu bạn cần lưu tâm.</p>\r\n<p class="Normal">- Cô ấy chỉ muốn làm giảm cảm giác tội lỗi khi lừa dối bạn.</p>\r\n<p class="Normal">- Nếu ham muốn tình dục của cô ấy bất ngờ thay đổi, đó có thể dấu hiệu cô ấy đang ngoại tình và chỉ muốn sử dụng bạn để thỏa mãn ham muốn với người đàn ông khác trong tâm tưởng.</p>\r\n<p class="Normal">- Ngược lại, nếu đời sống tình dục bạn đang rất tuyệt mà cô ấy trở nên lạnh nhạt hơn và tần suất "yêu" thưa thớt thì có thể cô ấy đã tìm được niềm vui mới bên ngoài.</p>\r\n<p class="subtitle">Giao tiếp bằng mắt</p>\r\n<p class="Normal">Thông thường, người ta sẽ tránh nhìn vào mắt nhau khi cảm thấy có lỗi hoặc có điều gì đó cần che giấu. Bạn hãy thử giao tiếp bằng mắt với cô ấy nhiều lần. Nếu cô ấy đang lừa dối bạn, khả năng là cô ấy sẽ né tránh ánh mắt của bạn.</p>\r\n<p class="Normal">- Đặc biệt khi bạn hỏi cô ấy đi đâu, làm gì, với ai, hãy nhìn thẳng vào mắt cô ấy. Nếu cô ấy thực sự làm điều gì tội lỗi sau lưng bạn thì cô ấy sẽ cảm thấy khó khăn khi nhìn vào mắt bạn và trả lời.</p>\r\n<p class="subtitle">Dành nhiều thời gian trước máy tính</p>\r\n<p class="Normal">Máy tính cũng là công cụ liên lạc phổ biến không kém điện thoại với nhiều ứng dụng. Nếu cô ấy dành cả buổi tối trước máy tính và tỏ ra khó chịu khi bạn hỏi cô ấy làm gì, có thể cô ấy đã gửi email hay chat với người tình qua máy tính.</p>\r\n<p class="Normal">- Hãy bí mật kiểm tra lịch sử sử dụng máy tính khi cô ấy không ở đó. Nếu lịch sử các trang web được sử dụng đã bị xóa đi thì bạn hoàn toàn có quyền nghi ngờ.</p>\r\n<p class="Normal">- Với điện thoại cũng vậy, hãy yêu cầu xem điện thoại của cô ấy. Nếu cô ấy tỏ ra lo lắng và từ chối thì hẳn là cô ấy đang che giấu gì đó. Khi có thể dùng điện thoại của cô ấy, hãy lướt qua các ứng dụng liên lạc trên máy như Skype, Viber… bởi đó có thể kênh liên lạc giữa cô ấy và người thứ ba, đặc biệt là nếu lịch sử tin nhắn đã bị xóa đi.</p>\r\n<p class="Normal">- Xem hóa đơn điện thoại, kiểm tra các số điện thoại thường xuyên liên lạc với vợ bạn. Số điện thoại đó có quen thuộc không? Hãy cân nhắc dùng dịch vụ để tìm kiếm địa điểm và tên của số điện thoại đó.</p>\r\n<p class="subtitle">Thái độ lẩn tránh và thoái thác</p>\r\n<p class="Normal">Cô ấy bất ngờ trở nên im lặng hơn, ít chia sẻ suy nghĩ và chi tiết về công việc, suy nghĩ của mình – đây là dấu hiệu cô ấy đang che giấu gì đó hoặc đã tìm được nguồn động viên tinh thần từ người khác thay vì bạn.</p>\r\n<p class="Normal">- Trong khi đàn ông thường ít bộc lộ suy nghĩ và thể hiện tâm trạng ra bên ngoài thì phụ nữ thường thích kể lể, tâm sự mọi chuyện. Trừ khi cô ấy có điều gì cần che giấu, cô ấy sẽ không giữ im lặng hay lẩn tránh bạn.</p>\r\n<p class="subtitle">Thay đổi thói quen làm đẹp</p>\r\n<p class="Normal">Để ý đến thay đổi về ngoại hình của vợ</p>\r\n<p class="Normal">Cuộc hôn nhân càng kéo dài thì cả hai dường như không còn thói quen chăm chút ngoại hình để đẹp trong mắt nhau nữa. Nhưng nếu cô ấy bất ngờ thay đổi ngoại hình như nhuộm tóc, làm tóc xoăn… thì chứng tỏ cô ấy đang cố gây ấn tượng với ai đó.</p>\r\n<p class="Normal">- Phụ nữ lúc nào cũng cố gắng chỉn chu vẻ bề ngoài nhất là khi bắt đầu một mối quan hệ mới, vì vậy, nếu cô ấy đang thay đổi hình thức một cách đáng kể thì bạn nên xem xét lại.</p>\r\n<p class="Normal">- Tuy nhiên, có khả năng là cô ấy làm điều này vì bạn bởi cảm thấy tình cảm giữa hai bạn nguội lạnh và cô ấy chỉ muốn hâm nóng mối quan hệ hiện tại với bạn.</p>\r\n<p class="subtitle">Mua sắm nhiều hơn</p>\r\n<p class="Normal">Hãy để ý cô ấy mua sắm những gì sau mỗi lần. Nếu cô ấy đang thay cả tủ quần áo mới thì có thể là cô ấy đang muốn hấp dẫn một người nào đó.</p>\r\n<p class="Normal">- Một dấu hiệu đáng ngờ đó là cô ấy mua những bộ đồ lót khiêu gợi hoặc những bộ cánh hấp dẫn nhưng không hề diện cho bạn ngắm.</p>\r\n<p class="subtitle">Tập thể dục chăm chỉ hơn</p>\r\n<p class="Normal">Bỗng nhiên vợ bạn lại nổi hứng với việc tập luyện để giữ dáng, trái ngược hẳn với trước đây. Liệu có phải cô ấy muốn mình hấp dẫn hơn trong mắt người mới?</p>\r\n<p class="Normal">- Tất nhiên, việc tập luyện có thể xuất phát từ việc ý thức hơn về việc giữ vóc dáng thon gọn, do bác sĩ khuyến cáo để tránh các nguy cơ với sức khỏe, quần áo cũ không còn vừa, hay đó là mục tiêu phấn đấu trong thời gian tới của cô ấy nhưng cũng không loại trừ khả năng cô ấy có nhân tình.</p>\r\n<p class="subtitle">Cô ấy thường xuyên tắm tại phòng tập</p>\r\n<p class="Normal">Nếu bạn hiếm khi thấy cô ấy tắm tại nhà với lý do đã tắm tại phòng tập thì có thể cô ấy đã làm việc đó ở nơi khác và việc tập luyện chỉ là cái cớ.</p>\r\n<p class="Normal">- Cô ấy chắc chắn sẽ tắm trước khi về nhà gặp bạn để tránh việc bị phát hiện ra mùi hương lạ trên cơ thể.</p>\r\n<p class="subtitle">Phát hiện mùi hương lạ trên cơ thể cô ấy</p>\r\n<p class="Normal">Hầu hết mọi người đều quen thuộc với mùi hương cơ thể của nửa kia. Vì vậy, nếu bạn không cảm nhận thấy mùi hương quen thuộc đó nữa hoặc ngửi thấy mùi hương lạ như nước hoa đàn ông loại mà bạn không dùng, có thể cô ấy đã gần gũi người đàn ông khác.</p>\r\n<p class="Normal"><strong>Hương Giang</strong> (theo <em>wikihow</em>)</p>', 1, 1, '2016-01-04 11:37:53', 1, '2016-01-13 16:26:26', 0, 1),
(2, 'Dấu hiệu vợ bạn đang ngoại tình 1', 'Dau-hieu-vo-ban-dang-ngoai-tinh-1', '', 'Nếu cô ấy kể rất nhiều về một người bạn mới nào đó tên Mai thì rất có thể cô ấy thực sự đang nói về một người tên Mạnh nào đó....', '<p class="subtitle">Thay đổi thái độ với các mối quan hệ xã hội</p>\r\n<p class="Normal">Quan sát cách cô ấy đối xử với những người quan tâm đến bạn.</p>\r\n<p class="Normal">Nếu vợ bạn dần trở nên xa cách với những thành viên trong gia đình bạn hoặc bạn bè bạn, người mà cô ấy từng rất thân thiết, đó là dấu hiệu cần chú ý.</p>\r\n<p class="Normal">- Nếu cô ấy thân thiết với gia đình và bạn bè bạn, cô ấy sẽ hiểu rằng những người thân cận sẽ bị tổn thương bởi vụ bê bối này chứ không chỉ riêng bạn. Điều này sẽ khiến cô ấy cảm thấy tội lỗi hơn nên thường muốn tránh mặt càng nhiều càng tốt.</p>\r\n<p class="Normal">- Cô ấy lo ngại những người này sẽ nói cho bạn biết sự thật cho dù bạn không hề biết được điều gì đang diễn ra.</p>\r\n<p class="Normal">- Đôi khi, gia đình là một phần nguyên nhân của vấn đề, chứ không chỉ là ngoại tình. Nếu vợ bạn không có mối quan hệ tốt với gia đình chồng, bạn cũng coi trọng ý kiến của họ hơn là quan tâm đến suy nghĩ của cô ấy, thì cô ấy sẽ cảm thấy rằng không hề có đồng minh, và muốn lánh mặt.</p>\r\n<table class="tplCaption" border="0" cellspacing="0" cellpadding="3" align="center">\r\n<tbody>\r\n<tr>\r\n<td><img src="http://img.f13.giadinh.vnecdn.net/2016/01/03/ngoaitinh-8544-1451826424.jpg" alt="dau-hieu-vo-ban-dang-ngoai-tinh" data-natural-width="500" data-width="500" data-pwidth="470.40625"></td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p class="Image">Ảnh: <em>tellyouall.com.</em></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p class="subtitle">Cô ấy thường xuyên đi chơi riêng</p>\r\n<p class="Normal">Nếu cô ấy bỗng nhiên dành nhiều thời gian đi chơi với bạn bè riêng mà không có bạn, đó có thể là dấu hiệu xấu. Ngay cả khi cô ấy nói rằng mình đi chơi với nhóm bạn gái thân thì sự thật chưa chắc đã là như vậy.</p>\r\n<p class="Normal">- Việc thỉnh thoảng gặp gỡ nhóm bạn gái thân là điều hoàn toàn bình thường, vì vậy, bạn không cần quá lo lắng nếu việc này không diễn ra thường xuyên.</p>\r\n<p class="Normal">- Mọi việc sẽ trở nên nghiêm trọng hơn nếu cô ấy đặt việc này lên trên cả mối quan hệ với bạn, nếu cô ấy thay đổi câu chuyện về việc gặp ai, đi đâu hoặc bạn không thể liên lạc khi cô ấy ra ngoài.</p>\r\n<p class="Normal">- Kể cả cô ấy thực sự đi gặp các bạn gái, có khả năng cô ấy tìm kiếm sự ủng hộ về mặt tinh thần và sẵn sàng chia sẻ những chuyện riêng tư mà không thể thú nhận với bạn. Đó chưa chắc là dấu hiệu cô ấy lừa dối bạn nhưng là dấu hiệu cuộc hôn nhân của bạn có vấn đề.</p>\r\n<p class="Normal">- Nếu cô ấy nói cần ra ngoài mà bạn cảm thấy nghi ngờ cô ấy đi gặp một người đàn ông khác, hãy đề nghị đi cùng cô ấy. Nếu cô ấy thay đổi quyết định không đi nữa, đó có thể là dấu hiệu cô ấy muốn một mình dành thời gian cho ai đó.</p>\r\n<p class="Normal">- Nếu vợ bạn biến mất nhiều giờ liền và khẳng định mình chỉ đi mua sắm, hãy thăm dò bằng các câu hỏi kiểu như có thấy món đồ nào thú vị không, ghé thăm cửa hàng nào… đặc biệt khi cô ấy đi về tay không. Phụ nữ thường thích kể lể về chuyện mua sắm nhưng nếu cô ấy thoái thác và né tránh câu hỏi này thì chứng tỏ cô ấy đã dành thời gian làm việc khác.</p>\r\n<p class="subtitle">Lắng nghe khi cô ấy nói về người bạn mới</p>\r\n<p class="Normal">Phụ nữ luôn hào hứng chia sẻ về những điều mới mẻ và thú vị trong cuộc sống, ngay cả đó là chuyện cô ấy muốn giữ bí mật. Nếu cô ấy nói về một người bạn mới, có thể đó không chỉ đơn thuần là một người bạn.</p>\r\n<p class="Normal">- Vợ bạn có thể bịa ra một tên giả để đánh lạc hướng bạn. Nếu cô ấy kể rất nhiều về một người nào đó tên Mai thì rất có thể cô ấy thực sự đang nói về một người tên Mạnh nào đó, cái tên chỉ để tránh sự nghi ngờ từ bạn mà thôi.</p>\r\n<p class="subtitle">Thay đổi về thói quen uống bia rượu và hút thuốc</p>\r\n<p class="Normal">Nếu cô ấy là người có tửu lượng tốt hoặc hút thuốc thì bạn sẽ khó nhận ra sự thay đổi. Nhưng trước đây cô ấy không bao giờ hứng thú với rượu bia hay thuốc lá mà lại bắt đầu tập hút thuốc hay uống rượu, có thể ai đó đang truyền cho cô ấy thói quen này.</p>\r\n<p class="Normal">- Ngay cả khi vợ bạn thỉnh thoảng cũng uống vài ly rượu, hãy chú ý đến hơi men trên cơ thể cô ấy. Nếu bạn ngửi thấy mùi thuốc lá hay rượu từ cô ấy sau khi trở về từ buổi làm tăng ca hay những thời điểm bất thường như giữa buổi chiều, hẳn là cô ấy đang che giấu điều gì đó.</p>\r\n<p class="subtitle">Chú ý đến mật độ làm việc và đi công tác</p>\r\n<p class="Normal">Cô ấy thường viện lý do làm thêm giờ trong khi không phải là người nghiện công việc thì có lẽ cô ấy dành thời gian ở nơi khác chứ không phải công sở.</p>\r\n<p class="Normal">- Bất kể thời gian làm việc gì đó dài hơn dự kiến đều có thể là tin xấu. Chẳng hạn như thời gian đi chợ, đi ngân hàng, đi làm tóc kéo dài hơn bình thường thì cô ấy có thể tận dụng để làm việc khác.</p>\r\n<p class="Normal">- Cân nhắc những chuyến đi xa nhà mà không liên quan đến công việc. Chẳng hạn như bỗng nhiên cô ấy nổi hứng đi thăm họ hàng ở xa mà không có bạn đi cùng, đặc biệt là ở khách sạn thay vì ở nhà người thân. Nếu cô ấy ở khách sạn, hãy tế nhị hỏi tên khách sạn, sau đó kiểm tra hóa đơn thẻ tín dụng. Nếu bạn không thấy khoản phí nào xuất hiện, hẳn là người khác đã chi trả khoản phí đó.</p>\r\n<p class="subtitle">Xem xét khoản tiền cô ấy chi tiêu khi đi ra ngoài</p>\r\n<p class="Normal">Ngoại tình thường dẫn đến nhiều khoản lệ phí tốn kém. Bạn hãy kín đáo kiểm tra các hóa đơn, sao kê thẻ ngân hàng. Nếu số tiền chi tiêu vượt trội so với bình thường thì có thể cô ấy đang dùng tiền mua sắm cho ai đó.</p>\r\n<p class="Normal">- Cuối cùng, hãy kiểm tra đồng hồ cây số trên xe máy/ô tô của cô ấy. Số cây số vượt trội có thể tố cáo việc cô ấy không chỉ đi từ nhà đến công sở và ngược lại.</p>\r\n<p class="subtitle">Thay đổi hành vi trong gia đình</p>\r\n<p class="Normal">Xem lại sự thay đổi tình cảm dành cho bạn</p>\r\n<p class="Normal">Bạn có thể cảm thấy nghi ngờ khi cô ấy trở nên xa cách bạn hơn nhưng ngược lại, nếu cô ấy tỏ ra thân mật hơn với bạn một cách khó hiểu, thì đó có thể dấu hiệu xấu.</p>\r\n<p class="Normal">- Vợ bỗng dưng quan tâm hơn đến bạn là cách để giảm bớt cảm giác tội lỗi khi đang ngoại tình. Nếu cô ấy tỏ ra chú ý hơn khi hai người ở cùng nhau, cô ấy chỉ cố đóng vai người vợ tuyệt vời trong mắt bạn và che giấu chuyện bên ngoài.</p>\r\n<p class="subtitle">Chú ý đến thói quen dùng điện thoại</p>\r\n<p class="Normal">Phụ nữ thường dành nhiều thời gian dùng điện thoại nhưng những thay đổi đột ngột như cô ấy dành nhiều thời gian nói chuyện, nhắn tin hơn và ngay lập tức ngừng lại khi bạn xuất hiện, bạn hãy chú ý bởi có thể cô ấy đang có ai khác ngoài bạn.</p>\r\n<p class="Normal">- Hãy thử hỏi ai gọi điện thoại cho vợ bạn, đặc biệt là khi cô ấy phải ra ngoài nghe điện hoặc dập máy ngay khi bạn bước vào phòng. Xem xét thái độ và phản ứng của cô ấy khi trả lời.</p>\r\n<p class="Normal">- Bạn cũng nên chú ý đến mật độ sử dụng điện thoại, thời gian sử dụng và giọng điệu khi nói chuyện.</p>\r\n<p class="subtitle">Vợ bạn hay bắt lỗi bạn</p>\r\n<p class="Normal">Nhiều ông chồng nghĩ rằng vợ thường hay càu nhàu, than vãn nhưng nếu vợ bạn bỗng trở nên khó tính và hay phê bình hành động của bạn, đó là tâm lý của người ngoại tình đang tìm cách bào chữa cho hành động tội lỗi của mình.</p>\r\n<p class="Normal">- Nếu cô ấy bỗng nhiên cư xử như thể mọi điều bạn làm điều không đúng đắn trong mắt cô ấy, đó là vấn đề nghiêm trọng cần xem xét, ngay cả khi cô ấy không ngoại tình.</p>\r\n<p class="Normal">- Mặt khác, nếu cô ấy không còn ca cẩm về bạn nữa cũng là dấu hiệu cần lưu ý. Nếu bạn có thể đưa ra lý do thuyết phục, chẳng hạn như, bạn thay đổi và cải thiện những tật xấu mà cô ấy thường phàn nàn thì điều này thật tuyệt. Nhưng nếu bạn vẫn vậy thì chứng tỏ cô ấy trở nên hờ hững, thiếu quan tâm hơn tới bạn. Cô ấy đã tìm được sự bù đắp tình cảm ở một người đàn ông khác.</p>\r\n<p class="subtitle">Thay đổi thói quen sinh hoạt tình dục</p>\r\n<p class="Normal">Bạn thường nghĩ rằng dấu hiệu ngoại tình là nửa kia ít gần gũi với bạn hơn. Nhưng không phải lúc nào cũng vậy, đôi khi, cô ấy tỏ ra hứng thú hơn với chuyện ấy hơn trước đây cũng là dấu hiệu bạn cần lưu tâm.</p>\r\n<p class="Normal">- Cô ấy chỉ muốn làm giảm cảm giác tội lỗi khi lừa dối bạn.</p>\r\n<p class="Normal">- Nếu ham muốn tình dục của cô ấy bất ngờ thay đổi, đó có thể dấu hiệu cô ấy đang ngoại tình và chỉ muốn sử dụng bạn để thỏa mãn ham muốn với người đàn ông khác trong tâm tưởng.</p>\r\n<p class="Normal">- Ngược lại, nếu đời sống tình dục bạn đang rất tuyệt mà cô ấy trở nên lạnh nhạt hơn và tần suất "yêu" thưa thớt thì có thể cô ấy đã tìm được niềm vui mới bên ngoài.</p>\r\n<p class="subtitle">Giao tiếp bằng mắt</p>\r\n<p class="Normal">Thông thường, người ta sẽ tránh nhìn vào mắt nhau khi cảm thấy có lỗi hoặc có điều gì đó cần che giấu. Bạn hãy thử giao tiếp bằng mắt với cô ấy nhiều lần. Nếu cô ấy đang lừa dối bạn, khả năng là cô ấy sẽ né tránh ánh mắt của bạn.</p>\r\n<p class="Normal">- Đặc biệt khi bạn hỏi cô ấy đi đâu, làm gì, với ai, hãy nhìn thẳng vào mắt cô ấy. Nếu cô ấy thực sự làm điều gì tội lỗi sau lưng bạn thì cô ấy sẽ cảm thấy khó khăn khi nhìn vào mắt bạn và trả lời.</p>\r\n<p class="subtitle">Dành nhiều thời gian trước máy tính</p>\r\n<p class="Normal">Máy tính cũng là công cụ liên lạc phổ biến không kém điện thoại với nhiều ứng dụng. Nếu cô ấy dành cả buổi tối trước máy tính và tỏ ra khó chịu khi bạn hỏi cô ấy làm gì, có thể cô ấy đã gửi email hay chat với người tình qua máy tính.</p>\r\n<p class="Normal">- Hãy bí mật kiểm tra lịch sử sử dụng máy tính khi cô ấy không ở đó. Nếu lịch sử các trang web được sử dụng đã bị xóa đi thì bạn hoàn toàn có quyền nghi ngờ.</p>\r\n<p class="Normal">- Với điện thoại cũng vậy, hãy yêu cầu xem điện thoại của cô ấy. Nếu cô ấy tỏ ra lo lắng và từ chối thì hẳn là cô ấy đang che giấu gì đó. Khi có thể dùng điện thoại của cô ấy, hãy lướt qua các ứng dụng liên lạc trên máy như Skype, Viber… bởi đó có thể kênh liên lạc giữa cô ấy và người thứ ba, đặc biệt là nếu lịch sử tin nhắn đã bị xóa đi.</p>\r\n<p class="Normal">- Xem hóa đơn điện thoại, kiểm tra các số điện thoại thường xuyên liên lạc với vợ bạn. Số điện thoại đó có quen thuộc không? Hãy cân nhắc dùng dịch vụ để tìm kiếm địa điểm và tên của số điện thoại đó.</p>\r\n<p class="subtitle">Thái độ lẩn tránh và thoái thác</p>\r\n<p class="Normal">Cô ấy bất ngờ trở nên im lặng hơn, ít chia sẻ suy nghĩ và chi tiết về công việc, suy nghĩ của mình – đây là dấu hiệu cô ấy đang che giấu gì đó hoặc đã tìm được nguồn động viên tinh thần từ người khác thay vì bạn.</p>\r\n<p class="Normal">- Trong khi đàn ông thường ít bộc lộ suy nghĩ và thể hiện tâm trạng ra bên ngoài thì phụ nữ thường thích kể lể, tâm sự mọi chuyện. Trừ khi cô ấy có điều gì cần che giấu, cô ấy sẽ không giữ im lặng hay lẩn tránh bạn.</p>\r\n<p class="subtitle">Thay đổi thói quen làm đẹp</p>\r\n<p class="Normal">Để ý đến thay đổi về ngoại hình của vợ</p>\r\n<p class="Normal">Cuộc hôn nhân càng kéo dài thì cả hai dường như không còn thói quen chăm chút ngoại hình để đẹp trong mắt nhau nữa. Nhưng nếu cô ấy bất ngờ thay đổi ngoại hình như nhuộm tóc, làm tóc xoăn… thì chứng tỏ cô ấy đang cố gây ấn tượng với ai đó.</p>\r\n<p class="Normal">- Phụ nữ lúc nào cũng cố gắng chỉn chu vẻ bề ngoài nhất là khi bắt đầu một mối quan hệ mới, vì vậy, nếu cô ấy đang thay đổi hình thức một cách đáng kể thì bạn nên xem xét lại.</p>\r\n<p class="Normal">- Tuy nhiên, có khả năng là cô ấy làm điều này vì bạn bởi cảm thấy tình cảm giữa hai bạn nguội lạnh và cô ấy chỉ muốn hâm nóng mối quan hệ hiện tại với bạn.</p>\r\n<p class="subtitle">Mua sắm nhiều hơn</p>\r\n<p class="Normal">Hãy để ý cô ấy mua sắm những gì sau mỗi lần. Nếu cô ấy đang thay cả tủ quần áo mới thì có thể là cô ấy đang muốn hấp dẫn một người nào đó.</p>\r\n<p class="Normal">- Một dấu hiệu đáng ngờ đó là cô ấy mua những bộ đồ lót khiêu gợi hoặc những bộ cánh hấp dẫn nhưng không hề diện cho bạn ngắm.</p>\r\n<p class="subtitle">Tập thể dục chăm chỉ hơn</p>\r\n<p class="Normal">Bỗng nhiên vợ bạn lại nổi hứng với việc tập luyện để giữ dáng, trái ngược hẳn với trước đây. Liệu có phải cô ấy muốn mình hấp dẫn hơn trong mắt người mới?</p>\r\n<p class="Normal">- Tất nhiên, việc tập luyện có thể xuất phát từ việc ý thức hơn về việc giữ vóc dáng thon gọn, do bác sĩ khuyến cáo để tránh các nguy cơ với sức khỏe, quần áo cũ không còn vừa, hay đó là mục tiêu phấn đấu trong thời gian tới của cô ấy nhưng cũng không loại trừ khả năng cô ấy có nhân tình.</p>\r\n<p class="subtitle">Cô ấy thường xuyên tắm tại phòng tập</p>\r\n<p class="Normal">Nếu bạn hiếm khi thấy cô ấy tắm tại nhà với lý do đã tắm tại phòng tập thì có thể cô ấy đã làm việc đó ở nơi khác và việc tập luyện chỉ là cái cớ.</p>\r\n<p class="Normal">- Cô ấy chắc chắn sẽ tắm trước khi về nhà gặp bạn để tránh việc bị phát hiện ra mùi hương lạ trên cơ thể.</p>\r\n<p class="subtitle">Phát hiện mùi hương lạ trên cơ thể cô ấy</p>\r\n<p class="Normal">Hầu hết mọi người đều quen thuộc với mùi hương cơ thể của nửa kia. Vì vậy, nếu bạn không cảm nhận thấy mùi hương quen thuộc đó nữa hoặc ngửi thấy mùi hương lạ như nước hoa đàn ông loại mà bạn không dùng, có thể cô ấy đã gần gũi người đàn ông khác.</p>\r\n<p class="Normal"><strong>Hương Giang</strong> (theo <em>wikihow</em>)</p>', 1, 1, '2016-01-04 13:47:12', 1, '2016-01-15 00:05:28', 0, 1),
(14, 'Dấu hiệu vợ bạn đang ngoại tình 1', 'Dau-hieu-vo-ban-dang-ngoai-tinh-1-2', '', 'Nếu cô ấy kể rất nhiều về một người bạn mới nào đó tên Mai thì rất có thể cô ấy thực sự đang nói về một người tên Mạnh nào đó....', '<p class="subtitle">Thay đổi thái độ với các mối quan hệ xã hội</p>\r\n<p class="Normal">Quan sát cách cô ấy đối xử với những người quan tâm đến bạn.</p>\r\n<p class="Normal">Nếu vợ bạn dần trở nên xa cách với những thành viên trong gia đình bạn hoặc bạn bè bạn, người mà cô ấy từng rất thân thiết, đó là dấu hiệu cần chú ý.</p>\r\n<p class="Normal">- Nếu cô ấy thân thiết với gia đình và bạn bè bạn, cô ấy sẽ hiểu rằng những người thân cận sẽ bị tổn thương bởi vụ bê bối này chứ không chỉ riêng bạn. Điều này sẽ khiến cô ấy cảm thấy tội lỗi hơn nên thường muốn tránh mặt càng nhiều càng tốt.</p>\r\n<p class="Normal">- Cô ấy lo ngại những người này sẽ nói cho bạn biết sự thật cho dù bạn không hề biết được điều gì đang diễn ra.</p>\r\n<p class="Normal">- Đôi khi, gia đình là một phần nguyên nhân của vấn đề, chứ không chỉ là ngoại tình. Nếu vợ bạn không có mối quan hệ tốt với gia đình chồng, bạn cũng coi trọng ý kiến của họ hơn là quan tâm đến suy nghĩ của cô ấy, thì cô ấy sẽ cảm thấy rằng không hề có đồng minh, và muốn lánh mặt.</p>\r\n<table class="tplCaption" border="0" cellspacing="0" cellpadding="3" align="center">\r\n<tbody>\r\n<tr>\r\n<td><img src="http://img.f13.giadinh.vnecdn.net/2016/01/03/ngoaitinh-8544-1451826424.jpg" alt="dau-hieu-vo-ban-dang-ngoai-tinh" data-natural-width="500" data-width="500" data-pwidth="470.40625"></td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p class="Image">Ảnh: <em>tellyouall.com.</em></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p class="subtitle">Cô ấy thường xuyên đi chơi riêng</p>\r\n<p class="Normal">Nếu cô ấy bỗng nhiên dành nhiều thời gian đi chơi với bạn bè riêng mà không có bạn, đó có thể là dấu hiệu xấu. Ngay cả khi cô ấy nói rằng mình đi chơi với nhóm bạn gái thân thì sự thật chưa chắc đã là như vậy.</p>\r\n<p class="Normal">- Việc thỉnh thoảng gặp gỡ nhóm bạn gái thân là điều hoàn toàn bình thường, vì vậy, bạn không cần quá lo lắng nếu việc này không diễn ra thường xuyên.</p>\r\n<p class="Normal">- Mọi việc sẽ trở nên nghiêm trọng hơn nếu cô ấy đặt việc này lên trên cả mối quan hệ với bạn, nếu cô ấy thay đổi câu chuyện về việc gặp ai, đi đâu hoặc bạn không thể liên lạc khi cô ấy ra ngoài.</p>\r\n<p class="Normal">- Kể cả cô ấy thực sự đi gặp các bạn gái, có khả năng cô ấy tìm kiếm sự ủng hộ về mặt tinh thần và sẵn sàng chia sẻ những chuyện riêng tư mà không thể thú nhận với bạn. Đó chưa chắc là dấu hiệu cô ấy lừa dối bạn nhưng là dấu hiệu cuộc hôn nhân của bạn có vấn đề.</p>\r\n<p class="Normal">- Nếu cô ấy nói cần ra ngoài mà bạn cảm thấy nghi ngờ cô ấy đi gặp một người đàn ông khác, hãy đề nghị đi cùng cô ấy. Nếu cô ấy thay đổi quyết định không đi nữa, đó có thể là dấu hiệu cô ấy muốn một mình dành thời gian cho ai đó.</p>\r\n<p class="Normal">- Nếu vợ bạn biến mất nhiều giờ liền và khẳng định mình chỉ đi mua sắm, hãy thăm dò bằng các câu hỏi kiểu như có thấy món đồ nào thú vị không, ghé thăm cửa hàng nào… đặc biệt khi cô ấy đi về tay không. Phụ nữ thường thích kể lể về chuyện mua sắm nhưng nếu cô ấy thoái thác và né tránh câu hỏi này thì chứng tỏ cô ấy đã dành thời gian làm việc khác.</p>\r\n<p class="subtitle">Lắng nghe khi cô ấy nói về người bạn mới</p>\r\n<p class="Normal">Phụ nữ luôn hào hứng chia sẻ về những điều mới mẻ và thú vị trong cuộc sống, ngay cả đó là chuyện cô ấy muốn giữ bí mật. Nếu cô ấy nói về một người bạn mới, có thể đó không chỉ đơn thuần là một người bạn.</p>\r\n<p class="Normal">- Vợ bạn có thể bịa ra một tên giả để đánh lạc hướng bạn. Nếu cô ấy kể rất nhiều về một người nào đó tên Mai thì rất có thể cô ấy thực sự đang nói về một người tên Mạnh nào đó, cái tên chỉ để tránh sự nghi ngờ từ bạn mà thôi.</p>\r\n<p class="subtitle">Thay đổi về thói quen uống bia rượu và hút thuốc</p>\r\n<p class="Normal">Nếu cô ấy là người có tửu lượng tốt hoặc hút thuốc thì bạn sẽ khó nhận ra sự thay đổi. Nhưng trước đây cô ấy không bao giờ hứng thú với rượu bia hay thuốc lá mà lại bắt đầu tập hút thuốc hay uống rượu, có thể ai đó đang truyền cho cô ấy thói quen này.</p>\r\n<p class="Normal">- Ngay cả khi vợ bạn thỉnh thoảng cũng uống vài ly rượu, hãy chú ý đến hơi men trên cơ thể cô ấy. Nếu bạn ngửi thấy mùi thuốc lá hay rượu từ cô ấy sau khi trở về từ buổi làm tăng ca hay những thời điểm bất thường như giữa buổi chiều, hẳn là cô ấy đang che giấu điều gì đó.</p>\r\n<p class="subtitle">Chú ý đến mật độ làm việc và đi công tác</p>\r\n<p class="Normal">Cô ấy thường viện lý do làm thêm giờ trong khi không phải là người nghiện công việc thì có lẽ cô ấy dành thời gian ở nơi khác chứ không phải công sở.</p>\r\n<p class="Normal">- Bất kể thời gian làm việc gì đó dài hơn dự kiến đều có thể là tin xấu. Chẳng hạn như thời gian đi chợ, đi ngân hàng, đi làm tóc kéo dài hơn bình thường thì cô ấy có thể tận dụng để làm việc khác.</p>\r\n<p class="Normal">- Cân nhắc những chuyến đi xa nhà mà không liên quan đến công việc. Chẳng hạn như bỗng nhiên cô ấy nổi hứng đi thăm họ hàng ở xa mà không có bạn đi cùng, đặc biệt là ở khách sạn thay vì ở nhà người thân. Nếu cô ấy ở khách sạn, hãy tế nhị hỏi tên khách sạn, sau đó kiểm tra hóa đơn thẻ tín dụng. Nếu bạn không thấy khoản phí nào xuất hiện, hẳn là người khác đã chi trả khoản phí đó.</p>\r\n<p class="subtitle">Xem xét khoản tiền cô ấy chi tiêu khi đi ra ngoài</p>\r\n<p class="Normal">Ngoại tình thường dẫn đến nhiều khoản lệ phí tốn kém. Bạn hãy kín đáo kiểm tra các hóa đơn, sao kê thẻ ngân hàng. Nếu số tiền chi tiêu vượt trội so với bình thường thì có thể cô ấy đang dùng tiền mua sắm cho ai đó.</p>\r\n<p class="Normal">- Cuối cùng, hãy kiểm tra đồng hồ cây số trên xe máy/ô tô của cô ấy. Số cây số vượt trội có thể tố cáo việc cô ấy không chỉ đi từ nhà đến công sở và ngược lại.</p>\r\n<p class="subtitle">Thay đổi hành vi trong gia đình</p>\r\n<p class="Normal">Xem lại sự thay đổi tình cảm dành cho bạn</p>\r\n<p class="Normal">Bạn có thể cảm thấy nghi ngờ khi cô ấy trở nên xa cách bạn hơn nhưng ngược lại, nếu cô ấy tỏ ra thân mật hơn với bạn một cách khó hiểu, thì đó có thể dấu hiệu xấu.</p>\r\n<p class="Normal">- Vợ bỗng dưng quan tâm hơn đến bạn là cách để giảm bớt cảm giác tội lỗi khi đang ngoại tình. Nếu cô ấy tỏ ra chú ý hơn khi hai người ở cùng nhau, cô ấy chỉ cố đóng vai người vợ tuyệt vời trong mắt bạn và che giấu chuyện bên ngoài.</p>\r\n<p class="subtitle">Chú ý đến thói quen dùng điện thoại</p>\r\n<p class="Normal">Phụ nữ thường dành nhiều thời gian dùng điện thoại nhưng những thay đổi đột ngột như cô ấy dành nhiều thời gian nói chuyện, nhắn tin hơn và ngay lập tức ngừng lại khi bạn xuất hiện, bạn hãy chú ý bởi có thể cô ấy đang có ai khác ngoài bạn.</p>\r\n<p class="Normal">- Hãy thử hỏi ai gọi điện thoại cho vợ bạn, đặc biệt là khi cô ấy phải ra ngoài nghe điện hoặc dập máy ngay khi bạn bước vào phòng. Xem xét thái độ và phản ứng của cô ấy khi trả lời.</p>\r\n<p class="Normal">- Bạn cũng nên chú ý đến mật độ sử dụng điện thoại, thời gian sử dụng và giọng điệu khi nói chuyện.</p>\r\n<p class="subtitle">Vợ bạn hay bắt lỗi bạn</p>\r\n<p class="Normal">Nhiều ông chồng nghĩ rằng vợ thường hay càu nhàu, than vãn nhưng nếu vợ bạn bỗng trở nên khó tính và hay phê bình hành động của bạn, đó là tâm lý của người ngoại tình đang tìm cách bào chữa cho hành động tội lỗi của mình.</p>\r\n<p class="Normal">- Nếu cô ấy bỗng nhiên cư xử như thể mọi điều bạn làm điều không đúng đắn trong mắt cô ấy, đó là vấn đề nghiêm trọng cần xem xét, ngay cả khi cô ấy không ngoại tình.</p>\r\n<p class="Normal">- Mặt khác, nếu cô ấy không còn ca cẩm về bạn nữa cũng là dấu hiệu cần lưu ý. Nếu bạn có thể đưa ra lý do thuyết phục, chẳng hạn như, bạn thay đổi và cải thiện những tật xấu mà cô ấy thường phàn nàn thì điều này thật tuyệt. Nhưng nếu bạn vẫn vậy thì chứng tỏ cô ấy trở nên hờ hững, thiếu quan tâm hơn tới bạn. Cô ấy đã tìm được sự bù đắp tình cảm ở một người đàn ông khác.</p>\r\n<p class="subtitle">Thay đổi thói quen sinh hoạt tình dục</p>\r\n<p class="Normal">Bạn thường nghĩ rằng dấu hiệu ngoại tình là nửa kia ít gần gũi với bạn hơn. Nhưng không phải lúc nào cũng vậy, đôi khi, cô ấy tỏ ra hứng thú hơn với chuyện ấy hơn trước đây cũng là dấu hiệu bạn cần lưu tâm.</p>\r\n<p class="Normal">- Cô ấy chỉ muốn làm giảm cảm giác tội lỗi khi lừa dối bạn.</p>\r\n<p class="Normal">- Nếu ham muốn tình dục của cô ấy bất ngờ thay đổi, đó có thể dấu hiệu cô ấy đang ngoại tình và chỉ muốn sử dụng bạn để thỏa mãn ham muốn với người đàn ông khác trong tâm tưởng.</p>\r\n<p class="Normal">- Ngược lại, nếu đời sống tình dục bạn đang rất tuyệt mà cô ấy trở nên lạnh nhạt hơn và tần suất "yêu" thưa thớt thì có thể cô ấy đã tìm được niềm vui mới bên ngoài.</p>\r\n<p class="subtitle">Giao tiếp bằng mắt</p>\r\n<p class="Normal">Thông thường, người ta sẽ tránh nhìn vào mắt nhau khi cảm thấy có lỗi hoặc có điều gì đó cần che giấu. Bạn hãy thử giao tiếp bằng mắt với cô ấy nhiều lần. Nếu cô ấy đang lừa dối bạn, khả năng là cô ấy sẽ né tránh ánh mắt của bạn.</p>\r\n<p class="Normal">- Đặc biệt khi bạn hỏi cô ấy đi đâu, làm gì, với ai, hãy nhìn thẳng vào mắt cô ấy. Nếu cô ấy thực sự làm điều gì tội lỗi sau lưng bạn thì cô ấy sẽ cảm thấy khó khăn khi nhìn vào mắt bạn và trả lời.</p>\r\n<p class="subtitle">Dành nhiều thời gian trước máy tính</p>\r\n<p class="Normal">Máy tính cũng là công cụ liên lạc phổ biến không kém điện thoại với nhiều ứng dụng. Nếu cô ấy dành cả buổi tối trước máy tính và tỏ ra khó chịu khi bạn hỏi cô ấy làm gì, có thể cô ấy đã gửi email hay chat với người tình qua máy tính.</p>\r\n<p class="Normal">- Hãy bí mật kiểm tra lịch sử sử dụng máy tính khi cô ấy không ở đó. Nếu lịch sử các trang web được sử dụng đã bị xóa đi thì bạn hoàn toàn có quyền nghi ngờ.</p>\r\n<p class="Normal">- Với điện thoại cũng vậy, hãy yêu cầu xem điện thoại của cô ấy. Nếu cô ấy tỏ ra lo lắng và từ chối thì hẳn là cô ấy đang che giấu gì đó. Khi có thể dùng điện thoại của cô ấy, hãy lướt qua các ứng dụng liên lạc trên máy như Skype, Viber… bởi đó có thể kênh liên lạc giữa cô ấy và người thứ ba, đặc biệt là nếu lịch sử tin nhắn đã bị xóa đi.</p>\r\n<p class="Normal">- Xem hóa đơn điện thoại, kiểm tra các số điện thoại thường xuyên liên lạc với vợ bạn. Số điện thoại đó có quen thuộc không? Hãy cân nhắc dùng dịch vụ để tìm kiếm địa điểm và tên của số điện thoại đó.</p>\r\n<p class="subtitle">Thái độ lẩn tránh và thoái thác</p>\r\n<p class="Normal">Cô ấy bất ngờ trở nên im lặng hơn, ít chia sẻ suy nghĩ và chi tiết về công việc, suy nghĩ của mình – đây là dấu hiệu cô ấy đang che giấu gì đó hoặc đã tìm được nguồn động viên tinh thần từ người khác thay vì bạn.</p>\r\n<p class="Normal">- Trong khi đàn ông thường ít bộc lộ suy nghĩ và thể hiện tâm trạng ra bên ngoài thì phụ nữ thường thích kể lể, tâm sự mọi chuyện. Trừ khi cô ấy có điều gì cần che giấu, cô ấy sẽ không giữ im lặng hay lẩn tránh bạn.</p>\r\n<p class="subtitle">Thay đổi thói quen làm đẹp</p>\r\n<p class="Normal">Để ý đến thay đổi về ngoại hình của vợ</p>\r\n<p class="Normal">Cuộc hôn nhân càng kéo dài thì cả hai dường như không còn thói quen chăm chút ngoại hình để đẹp trong mắt nhau nữa. Nhưng nếu cô ấy bất ngờ thay đổi ngoại hình như nhuộm tóc, làm tóc xoăn… thì chứng tỏ cô ấy đang cố gây ấn tượng với ai đó.</p>\r\n<p class="Normal">- Phụ nữ lúc nào cũng cố gắng chỉn chu vẻ bề ngoài nhất là khi bắt đầu một mối quan hệ mới, vì vậy, nếu cô ấy đang thay đổi hình thức một cách đáng kể thì bạn nên xem xét lại.</p>\r\n<p class="Normal">- Tuy nhiên, có khả năng là cô ấy làm điều này vì bạn bởi cảm thấy tình cảm giữa hai bạn nguội lạnh và cô ấy chỉ muốn hâm nóng mối quan hệ hiện tại với bạn.</p>\r\n<p class="subtitle">Mua sắm nhiều hơn</p>\r\n<p class="Normal">Hãy để ý cô ấy mua sắm những gì sau mỗi lần. Nếu cô ấy đang thay cả tủ quần áo mới thì có thể là cô ấy đang muốn hấp dẫn một người nào đó.</p>\r\n<p class="Normal">- Một dấu hiệu đáng ngờ đó là cô ấy mua những bộ đồ lót khiêu gợi hoặc những bộ cánh hấp dẫn nhưng không hề diện cho bạn ngắm.</p>\r\n<p class="subtitle">Tập thể dục chăm chỉ hơn</p>\r\n<p class="Normal">Bỗng nhiên vợ bạn lại nổi hứng với việc tập luyện để giữ dáng, trái ngược hẳn với trước đây. Liệu có phải cô ấy muốn mình hấp dẫn hơn trong mắt người mới?</p>\r\n<p class="Normal">- Tất nhiên, việc tập luyện có thể xuất phát từ việc ý thức hơn về việc giữ vóc dáng thon gọn, do bác sĩ khuyến cáo để tránh các nguy cơ với sức khỏe, quần áo cũ không còn vừa, hay đó là mục tiêu phấn đấu trong thời gian tới của cô ấy nhưng cũng không loại trừ khả năng cô ấy có nhân tình.</p>\r\n<p class="subtitle">Cô ấy thường xuyên tắm tại phòng tập</p>\r\n<p class="Normal">Nếu bạn hiếm khi thấy cô ấy tắm tại nhà với lý do đã tắm tại phòng tập thì có thể cô ấy đã làm việc đó ở nơi khác và việc tập luyện chỉ là cái cớ.</p>\r\n<p class="Normal">- Cô ấy chắc chắn sẽ tắm trước khi về nhà gặp bạn để tránh việc bị phát hiện ra mùi hương lạ trên cơ thể.</p>\r\n<p class="subtitle">Phát hiện mùi hương lạ trên cơ thể cô ấy</p>\r\n<p class="Normal">Hầu hết mọi người đều quen thuộc với mùi hương cơ thể của nửa kia. Vì vậy, nếu bạn không cảm nhận thấy mùi hương quen thuộc đó nữa hoặc ngửi thấy mùi hương lạ như nước hoa đàn ông loại mà bạn không dùng, có thể cô ấy đã gần gũi người đàn ông khác.</p>\r\n<p class="Normal"><strong>Hương Giang</strong> (theo <em>wikihow</em>)</p>', 1, 1, '2016-01-19 11:22:46', 0, '0000-00-00 00:00:00', 0, 1);
INSERT INTO `posts` (`id`, `title`, `slug`, `thumbnail`, `description`, `detail`, `type`, `created_user`, `created_date`, `edit_user`, `edit_date`, `viewed`, `status`) VALUES
(15, 'Dấu hiệu vợ bạn đang ngoại tình 1', 'Dau-hieu-vo-ban-dang-ngoai-tinh-1', '', 'Nếu cô ấy kể rất nhiều về một người bạn mới nào đó tên Mai thì rất có thể cô ấy thực sự đang nói về một người tên Mạnh nào đó....', '<p class="subtitle">Thay đổi thái độ với các mối quan hệ xã hội</p>\r\n<p class="Normal">Quan sát cách cô ấy đối xử với những người quan tâm đến bạn.</p>\r\n<p class="Normal">Nếu vợ bạn dần trở nên xa cách với những thành viên trong gia đình bạn hoặc bạn bè bạn, người mà cô ấy từng rất thân thiết, đó là dấu hiệu cần chú ý.</p>\r\n<p class="Normal">- Nếu cô ấy thân thiết với gia đình và bạn bè bạn, cô ấy sẽ hiểu rằng những người thân cận sẽ bị tổn thương bởi vụ bê bối này chứ không chỉ riêng bạn. Điều này sẽ khiến cô ấy cảm thấy tội lỗi hơn nên thường muốn tránh mặt càng nhiều càng tốt.</p>\r\n<p class="Normal">- Cô ấy lo ngại những người này sẽ nói cho bạn biết sự thật cho dù bạn không hề biết được điều gì đang diễn ra.</p>\r\n<p class="Normal">- Đôi khi, gia đình là một phần nguyên nhân của vấn đề, chứ không chỉ là ngoại tình. Nếu vợ bạn không có mối quan hệ tốt với gia đình chồng, bạn cũng coi trọng ý kiến của họ hơn là quan tâm đến suy nghĩ của cô ấy, thì cô ấy sẽ cảm thấy rằng không hề có đồng minh, và muốn lánh mặt.</p>\r\n<table class="tplCaption" border="0" cellspacing="0" cellpadding="3" align="center">\r\n<tbody>\r\n<tr>\r\n<td><img src="http://img.f13.giadinh.vnecdn.net/2016/01/03/ngoaitinh-8544-1451826424.jpg" alt="dau-hieu-vo-ban-dang-ngoai-tinh" data-natural-width="500" data-width="500" data-pwidth="470.40625"></td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p class="Image">Ảnh: <em>tellyouall.com.</em></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p class="subtitle">Cô ấy thường xuyên đi chơi riêng</p>\r\n<p class="Normal">Nếu cô ấy bỗng nhiên dành nhiều thời gian đi chơi với bạn bè riêng mà không có bạn, đó có thể là dấu hiệu xấu. Ngay cả khi cô ấy nói rằng mình đi chơi với nhóm bạn gái thân thì sự thật chưa chắc đã là như vậy.</p>\r\n<p class="Normal">- Việc thỉnh thoảng gặp gỡ nhóm bạn gái thân là điều hoàn toàn bình thường, vì vậy, bạn không cần quá lo lắng nếu việc này không diễn ra thường xuyên.</p>\r\n<p class="Normal">- Mọi việc sẽ trở nên nghiêm trọng hơn nếu cô ấy đặt việc này lên trên cả mối quan hệ với bạn, nếu cô ấy thay đổi câu chuyện về việc gặp ai, đi đâu hoặc bạn không thể liên lạc khi cô ấy ra ngoài.</p>\r\n<p class="Normal">- Kể cả cô ấy thực sự đi gặp các bạn gái, có khả năng cô ấy tìm kiếm sự ủng hộ về mặt tinh thần và sẵn sàng chia sẻ những chuyện riêng tư mà không thể thú nhận với bạn. Đó chưa chắc là dấu hiệu cô ấy lừa dối bạn nhưng là dấu hiệu cuộc hôn nhân của bạn có vấn đề.</p>\r\n<p class="Normal">- Nếu cô ấy nói cần ra ngoài mà bạn cảm thấy nghi ngờ cô ấy đi gặp một người đàn ông khác, hãy đề nghị đi cùng cô ấy. Nếu cô ấy thay đổi quyết định không đi nữa, đó có thể là dấu hiệu cô ấy muốn một mình dành thời gian cho ai đó.</p>\r\n<p class="Normal">- Nếu vợ bạn biến mất nhiều giờ liền và khẳng định mình chỉ đi mua sắm, hãy thăm dò bằng các câu hỏi kiểu như có thấy món đồ nào thú vị không, ghé thăm cửa hàng nào… đặc biệt khi cô ấy đi về tay không. Phụ nữ thường thích kể lể về chuyện mua sắm nhưng nếu cô ấy thoái thác và né tránh câu hỏi này thì chứng tỏ cô ấy đã dành thời gian làm việc khác.</p>\r\n<p class="subtitle">Lắng nghe khi cô ấy nói về người bạn mới</p>\r\n<p class="Normal">Phụ nữ luôn hào hứng chia sẻ về những điều mới mẻ và thú vị trong cuộc sống, ngay cả đó là chuyện cô ấy muốn giữ bí mật. Nếu cô ấy nói về một người bạn mới, có thể đó không chỉ đơn thuần là một người bạn.</p>\r\n<p class="Normal">- Vợ bạn có thể bịa ra một tên giả để đánh lạc hướng bạn. Nếu cô ấy kể rất nhiều về một người nào đó tên Mai thì rất có thể cô ấy thực sự đang nói về một người tên Mạnh nào đó, cái tên chỉ để tránh sự nghi ngờ từ bạn mà thôi.</p>\r\n<p class="subtitle">Thay đổi về thói quen uống bia rượu và hút thuốc</p>\r\n<p class="Normal">Nếu cô ấy là người có tửu lượng tốt hoặc hút thuốc thì bạn sẽ khó nhận ra sự thay đổi. Nhưng trước đây cô ấy không bao giờ hứng thú với rượu bia hay thuốc lá mà lại bắt đầu tập hút thuốc hay uống rượu, có thể ai đó đang truyền cho cô ấy thói quen này.</p>\r\n<p class="Normal">- Ngay cả khi vợ bạn thỉnh thoảng cũng uống vài ly rượu, hãy chú ý đến hơi men trên cơ thể cô ấy. Nếu bạn ngửi thấy mùi thuốc lá hay rượu từ cô ấy sau khi trở về từ buổi làm tăng ca hay những thời điểm bất thường như giữa buổi chiều, hẳn là cô ấy đang che giấu điều gì đó.</p>\r\n<p class="subtitle">Chú ý đến mật độ làm việc và đi công tác</p>\r\n<p class="Normal">Cô ấy thường viện lý do làm thêm giờ trong khi không phải là người nghiện công việc thì có lẽ cô ấy dành thời gian ở nơi khác chứ không phải công sở.</p>\r\n<p class="Normal">- Bất kể thời gian làm việc gì đó dài hơn dự kiến đều có thể là tin xấu. Chẳng hạn như thời gian đi chợ, đi ngân hàng, đi làm tóc kéo dài hơn bình thường thì cô ấy có thể tận dụng để làm việc khác.</p>\r\n<p class="Normal">- Cân nhắc những chuyến đi xa nhà mà không liên quan đến công việc. Chẳng hạn như bỗng nhiên cô ấy nổi hứng đi thăm họ hàng ở xa mà không có bạn đi cùng, đặc biệt là ở khách sạn thay vì ở nhà người thân. Nếu cô ấy ở khách sạn, hãy tế nhị hỏi tên khách sạn, sau đó kiểm tra hóa đơn thẻ tín dụng. Nếu bạn không thấy khoản phí nào xuất hiện, hẳn là người khác đã chi trả khoản phí đó.</p>\r\n<p class="subtitle">Xem xét khoản tiền cô ấy chi tiêu khi đi ra ngoài</p>\r\n<p class="Normal">Ngoại tình thường dẫn đến nhiều khoản lệ phí tốn kém. Bạn hãy kín đáo kiểm tra các hóa đơn, sao kê thẻ ngân hàng. Nếu số tiền chi tiêu vượt trội so với bình thường thì có thể cô ấy đang dùng tiền mua sắm cho ai đó.</p>\r\n<p class="Normal">- Cuối cùng, hãy kiểm tra đồng hồ cây số trên xe máy/ô tô của cô ấy. Số cây số vượt trội có thể tố cáo việc cô ấy không chỉ đi từ nhà đến công sở và ngược lại.</p>\r\n<p class="subtitle">Thay đổi hành vi trong gia đình</p>\r\n<p class="Normal">Xem lại sự thay đổi tình cảm dành cho bạn</p>\r\n<p class="Normal">Bạn có thể cảm thấy nghi ngờ khi cô ấy trở nên xa cách bạn hơn nhưng ngược lại, nếu cô ấy tỏ ra thân mật hơn với bạn một cách khó hiểu, thì đó có thể dấu hiệu xấu.</p>\r\n<p class="Normal">- Vợ bỗng dưng quan tâm hơn đến bạn là cách để giảm bớt cảm giác tội lỗi khi đang ngoại tình. Nếu cô ấy tỏ ra chú ý hơn khi hai người ở cùng nhau, cô ấy chỉ cố đóng vai người vợ tuyệt vời trong mắt bạn và che giấu chuyện bên ngoài.</p>\r\n<p class="subtitle">Chú ý đến thói quen dùng điện thoại</p>\r\n<p class="Normal">Phụ nữ thường dành nhiều thời gian dùng điện thoại nhưng những thay đổi đột ngột như cô ấy dành nhiều thời gian nói chuyện, nhắn tin hơn và ngay lập tức ngừng lại khi bạn xuất hiện, bạn hãy chú ý bởi có thể cô ấy đang có ai khác ngoài bạn.</p>\r\n<p class="Normal">- Hãy thử hỏi ai gọi điện thoại cho vợ bạn, đặc biệt là khi cô ấy phải ra ngoài nghe điện hoặc dập máy ngay khi bạn bước vào phòng. Xem xét thái độ và phản ứng của cô ấy khi trả lời.</p>\r\n<p class="Normal">- Bạn cũng nên chú ý đến mật độ sử dụng điện thoại, thời gian sử dụng và giọng điệu khi nói chuyện.</p>\r\n<p class="subtitle">Vợ bạn hay bắt lỗi bạn</p>\r\n<p class="Normal">Nhiều ông chồng nghĩ rằng vợ thường hay càu nhàu, than vãn nhưng nếu vợ bạn bỗng trở nên khó tính và hay phê bình hành động của bạn, đó là tâm lý của người ngoại tình đang tìm cách bào chữa cho hành động tội lỗi của mình.</p>\r\n<p class="Normal">- Nếu cô ấy bỗng nhiên cư xử như thể mọi điều bạn làm điều không đúng đắn trong mắt cô ấy, đó là vấn đề nghiêm trọng cần xem xét, ngay cả khi cô ấy không ngoại tình.</p>\r\n<p class="Normal">- Mặt khác, nếu cô ấy không còn ca cẩm về bạn nữa cũng là dấu hiệu cần lưu ý. Nếu bạn có thể đưa ra lý do thuyết phục, chẳng hạn như, bạn thay đổi và cải thiện những tật xấu mà cô ấy thường phàn nàn thì điều này thật tuyệt. Nhưng nếu bạn vẫn vậy thì chứng tỏ cô ấy trở nên hờ hững, thiếu quan tâm hơn tới bạn. Cô ấy đã tìm được sự bù đắp tình cảm ở một người đàn ông khác.</p>\r\n<p class="subtitle">Thay đổi thói quen sinh hoạt tình dục</p>\r\n<p class="Normal">Bạn thường nghĩ rằng dấu hiệu ngoại tình là nửa kia ít gần gũi với bạn hơn. Nhưng không phải lúc nào cũng vậy, đôi khi, cô ấy tỏ ra hứng thú hơn với chuyện ấy hơn trước đây cũng là dấu hiệu bạn cần lưu tâm.</p>\r\n<p class="Normal">- Cô ấy chỉ muốn làm giảm cảm giác tội lỗi khi lừa dối bạn.</p>\r\n<p class="Normal">- Nếu ham muốn tình dục của cô ấy bất ngờ thay đổi, đó có thể dấu hiệu cô ấy đang ngoại tình và chỉ muốn sử dụng bạn để thỏa mãn ham muốn với người đàn ông khác trong tâm tưởng.</p>\r\n<p class="Normal">- Ngược lại, nếu đời sống tình dục bạn đang rất tuyệt mà cô ấy trở nên lạnh nhạt hơn và tần suất "yêu" thưa thớt thì có thể cô ấy đã tìm được niềm vui mới bên ngoài.</p>\r\n<p class="subtitle">Giao tiếp bằng mắt</p>\r\n<p class="Normal">Thông thường, người ta sẽ tránh nhìn vào mắt nhau khi cảm thấy có lỗi hoặc có điều gì đó cần che giấu. Bạn hãy thử giao tiếp bằng mắt với cô ấy nhiều lần. Nếu cô ấy đang lừa dối bạn, khả năng là cô ấy sẽ né tránh ánh mắt của bạn.</p>\r\n<p class="Normal">- Đặc biệt khi bạn hỏi cô ấy đi đâu, làm gì, với ai, hãy nhìn thẳng vào mắt cô ấy. Nếu cô ấy thực sự làm điều gì tội lỗi sau lưng bạn thì cô ấy sẽ cảm thấy khó khăn khi nhìn vào mắt bạn và trả lời.</p>\r\n<p class="subtitle">Dành nhiều thời gian trước máy tính</p>\r\n<p class="Normal">Máy tính cũng là công cụ liên lạc phổ biến không kém điện thoại với nhiều ứng dụng. Nếu cô ấy dành cả buổi tối trước máy tính và tỏ ra khó chịu khi bạn hỏi cô ấy làm gì, có thể cô ấy đã gửi email hay chat với người tình qua máy tính.</p>\r\n<p class="Normal">- Hãy bí mật kiểm tra lịch sử sử dụng máy tính khi cô ấy không ở đó. Nếu lịch sử các trang web được sử dụng đã bị xóa đi thì bạn hoàn toàn có quyền nghi ngờ.</p>\r\n<p class="Normal">- Với điện thoại cũng vậy, hãy yêu cầu xem điện thoại của cô ấy. Nếu cô ấy tỏ ra lo lắng và từ chối thì hẳn là cô ấy đang che giấu gì đó. Khi có thể dùng điện thoại của cô ấy, hãy lướt qua các ứng dụng liên lạc trên máy như Skype, Viber… bởi đó có thể kênh liên lạc giữa cô ấy và người thứ ba, đặc biệt là nếu lịch sử tin nhắn đã bị xóa đi.</p>\r\n<p class="Normal">- Xem hóa đơn điện thoại, kiểm tra các số điện thoại thường xuyên liên lạc với vợ bạn. Số điện thoại đó có quen thuộc không? Hãy cân nhắc dùng dịch vụ để tìm kiếm địa điểm và tên của số điện thoại đó.</p>\r\n<p class="subtitle">Thái độ lẩn tránh và thoái thác</p>\r\n<p class="Normal">Cô ấy bất ngờ trở nên im lặng hơn, ít chia sẻ suy nghĩ và chi tiết về công việc, suy nghĩ của mình – đây là dấu hiệu cô ấy đang che giấu gì đó hoặc đã tìm được nguồn động viên tinh thần từ người khác thay vì bạn.</p>\r\n<p class="Normal">- Trong khi đàn ông thường ít bộc lộ suy nghĩ và thể hiện tâm trạng ra bên ngoài thì phụ nữ thường thích kể lể, tâm sự mọi chuyện. Trừ khi cô ấy có điều gì cần che giấu, cô ấy sẽ không giữ im lặng hay lẩn tránh bạn.</p>\r\n<p class="subtitle">Thay đổi thói quen làm đẹp</p>\r\n<p class="Normal">Để ý đến thay đổi về ngoại hình của vợ</p>\r\n<p class="Normal">Cuộc hôn nhân càng kéo dài thì cả hai dường như không còn thói quen chăm chút ngoại hình để đẹp trong mắt nhau nữa. Nhưng nếu cô ấy bất ngờ thay đổi ngoại hình như nhuộm tóc, làm tóc xoăn… thì chứng tỏ cô ấy đang cố gây ấn tượng với ai đó.</p>\r\n<p class="Normal">- Phụ nữ lúc nào cũng cố gắng chỉn chu vẻ bề ngoài nhất là khi bắt đầu một mối quan hệ mới, vì vậy, nếu cô ấy đang thay đổi hình thức một cách đáng kể thì bạn nên xem xét lại.</p>\r\n<p class="Normal">- Tuy nhiên, có khả năng là cô ấy làm điều này vì bạn bởi cảm thấy tình cảm giữa hai bạn nguội lạnh và cô ấy chỉ muốn hâm nóng mối quan hệ hiện tại với bạn.</p>\r\n<p class="subtitle">Mua sắm nhiều hơn</p>\r\n<p class="Normal">Hãy để ý cô ấy mua sắm những gì sau mỗi lần. Nếu cô ấy đang thay cả tủ quần áo mới thì có thể là cô ấy đang muốn hấp dẫn một người nào đó.</p>\r\n<p class="Normal">- Một dấu hiệu đáng ngờ đó là cô ấy mua những bộ đồ lót khiêu gợi hoặc những bộ cánh hấp dẫn nhưng không hề diện cho bạn ngắm.</p>\r\n<p class="subtitle">Tập thể dục chăm chỉ hơn</p>\r\n<p class="Normal">Bỗng nhiên vợ bạn lại nổi hứng với việc tập luyện để giữ dáng, trái ngược hẳn với trước đây. Liệu có phải cô ấy muốn mình hấp dẫn hơn trong mắt người mới?</p>\r\n<p class="Normal">- Tất nhiên, việc tập luyện có thể xuất phát từ việc ý thức hơn về việc giữ vóc dáng thon gọn, do bác sĩ khuyến cáo để tránh các nguy cơ với sức khỏe, quần áo cũ không còn vừa, hay đó là mục tiêu phấn đấu trong thời gian tới của cô ấy nhưng cũng không loại trừ khả năng cô ấy có nhân tình.</p>\r\n<p class="subtitle">Cô ấy thường xuyên tắm tại phòng tập</p>\r\n<p class="Normal">Nếu bạn hiếm khi thấy cô ấy tắm tại nhà với lý do đã tắm tại phòng tập thì có thể cô ấy đã làm việc đó ở nơi khác và việc tập luyện chỉ là cái cớ.</p>\r\n<p class="Normal">- Cô ấy chắc chắn sẽ tắm trước khi về nhà gặp bạn để tránh việc bị phát hiện ra mùi hương lạ trên cơ thể.</p>\r\n<p class="subtitle">Phát hiện mùi hương lạ trên cơ thể cô ấy</p>\r\n<p class="Normal">Hầu hết mọi người đều quen thuộc với mùi hương cơ thể của nửa kia. Vì vậy, nếu bạn không cảm nhận thấy mùi hương quen thuộc đó nữa hoặc ngửi thấy mùi hương lạ như nước hoa đàn ông loại mà bạn không dùng, có thể cô ấy đã gần gũi người đàn ông khác.</p>\r\n<p class="Normal"><strong>Hương Giang</strong> (theo <em>wikihow</em>)</p>', 1, 1, '2016-01-19 11:25:07', 0, '2016-01-19 07:51:21', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `post_category`
--

CREATE TABLE IF NOT EXISTS `post_category` (
  `post_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post_category`
--

INSERT INTO `post_category` (`post_id`, `category_id`) VALUES
(1, 10),
(1, 22),
(2, 10),
(2, 22),
(14, 10),
(14, 22),
(15, 10),
(15, 22);

-- --------------------------------------------------------

--
-- Table structure for table `post_tag`
--

CREATE TABLE IF NOT EXISTS `post_tag` (
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post_tag`
--

INSERT INTO `post_tag` (`post_id`, `tag_id`) VALUES
(1, 9),
(1, 10),
(2, 10),
(2, 12),
(14, 10),
(14, 12),
(15, 10),
(15, 12);

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
  `id` int(11) NOT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `status` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `thumbnail`, `title`, `description`, `url`, `order`, `status`) VALUES
(57, 'public/upload/slider/Slider_20160112110642.png', '', '', '', -108, 0),
(59, 'public/upload/slider/Slider_20160112110638.png', '', '', '', -110, 0),
(60, 'public/upload/slider/Slider_20160113163038.png', '', '', '', -109, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`) VALUES
(9, 'Chồng'),
(11, 'Công Sở'),
(1, 'Gia Đình'),
(12, 'ngoại'),
(10, 'Ngoại Tình'),
(2, 'Tổ Ấm');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `username` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login_count` int(2) NOT NULL DEFAULT '0',
  `left_time_login` int(11) NOT NULL DEFAULT '0',
  `left_time_reset` int(11) NOT NULL DEFAULT '0',
  `email` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_id` tinyint(2) NOT NULL DEFAULT '0',
  `realname` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `personalization` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `password_reset`, `login_count`, `left_time_login`, `left_time_reset`, `email`, `phone`, `role_id`, `realname`, `avatar`, `personalization`, `status`) VALUES
(1, 'phamtanphu', '8dcdafe2b31d29cae12690731d1281b9', '028fca500aafec92e09625222e475324', 0, 0, 1453213175, 'phupt@zanado.com', '0977201157', 1, 'Pham Tan Phu', '', '', 1),
(2, 'phamtanphu2', '8dcdafe2b31d29cae12690731d1281b9', NULL, 0, 0, 0, 'phamphu2322@gmail.com', '09772011572', 2, 'Pham Tan Phu', '', '', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `slug` (`slug`), ADD KEY `order` (`order`), ADD KEY `status` (`status`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`) COMMENT 'KEY';

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `key` (`name`), ADD UNIQUE KEY `key_2` (`name`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`) COMMENT 'post_id', ADD KEY `id` (`id`), ADD KEY `created_user` (`created_user`);

--
-- Indexes for table `post_category`
--
ALTER TABLE `post_category`
  ADD PRIMARY KEY (`post_id`,`category_id`), ADD KEY `post_id` (`post_id`), ADD KEY `category_id` (`category_id`), ADD KEY `post_id_2` (`post_id`);

--
-- Indexes for table `post_tag`
--
ALTER TABLE `post_tag`
  ADD PRIMARY KEY (`post_id`,`tag_id`), ADD KEY `post_id` (`post_id`), ADD KEY `tag_id` (`tag_id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`) COMMENT '1';

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `username` (`username`), ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=154;
--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`created_user`) REFERENCES `user` (`id`);

--
-- Constraints for table `post_category`
--
ALTER TABLE `post_category`
ADD CONSTRAINT `post_category_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`),
ADD CONSTRAINT `post_category_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Constraints for table `post_tag`
--
ALTER TABLE `post_tag`
ADD CONSTRAINT `post_tag_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`),
ADD CONSTRAINT `post_tag_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
