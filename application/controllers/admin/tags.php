<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of tags
 *
 * @author PHAMTANPHU
 */
class tags extends MY_Admin_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model("admin/m_tags");
    }

    public function index() {
        
    }

    public function get_tags() {

        $like = array();

        $key = trim($this->input->get_post("term", TRUE));
        if ($key != '') {
            $like['name'] = $key;
        }

        $data = $this->m_tags->get_data($field = array("name"), $where = array(), $like);

        $rs = array();
        foreach ($data as $item) {
            array_push($rs, $item['name']);
        }

        //sleep(5);
        echo json_encode($rs);
    }

}
