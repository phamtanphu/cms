<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of menu
 *
 * @author PHAMTANPHU
 */
class menus extends MY_Admin_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model("admin/m_menus");
    }

    public function index() {
        $this->load_root_menu();
    }

    public function load_root_menu() {
        $data = array();
        $data['title'] = 'Menu';

        $data['breadcrum'] = array(
            array('link' => '', 'text' => 'Menu')
        );

        $data['data'] = $this->m_menus->get_data("id, title", array('root' => 0));
        $this->layout->admin_view('menus/index', $data);
    }

    public function insert_root_menu() {

        $post_data = array(
            'title' => trim($this->input->get_post('title', TRUE)),
            'root' => 0
        );

        $r = array();
        if ($post_data['title'] == '') {
            $r['status'] = FALSE;
            $r['message'] = "Chưa nhập tên menu!";
        } else if (preg_match('/\'|\"|\`/', $post_data['title'])) {
            $r['status'] = FALSE;
            $r['message'] = "Tiêu đề không được chứa các ký tự đặc biệt!";
        } else {
            if (!$this->m_menus->is_exist(array('title' => $post_data['title'], 'root' => 0))) {
                $data = $this->m_menus->insert($post_data);
                if ($data) {
                    $r['status'] = TRUE;
                } else {
                    $r['status'] = FALSE;
                    $r['message'] = "Lỗi trong quá trình tạo menu admin::m_menu";
                }
            } else {
                $r['status'] = FALSE;
                $r['message'] = "Tên menu này đã tồn tại!";
            }
        }

        echo json_encode($r);
    }

    public function update_root_menu() {

        $post_data = array(
            'id' => intval(trim($this->input->get_post('id', TRUE))),
            'title' => trim($this->input->get_post('title', TRUE)),
        );

        $r = array();
        if (!isset($post_data['id']) || $post_data['id'] < 1) {
            $r['status'] = FALSE;
            $r['message'] = "Không lấy được id menu admin::menu->update_root_menu";
        } else if ($post_data['title'] == '') {
            $r['status'] = FALSE;
            $r['message'] = "Chưa nhập tên menu!";
        } else if (preg_match('/\'|\"|\`/', $post_data['title'])) {
            $r['status'] = FALSE;
            $r['message'] = "Tiêu đề không được chứa các ký tự đặc biệt!";
        } else {
            if (!$this->m_menus->is_exist(array('title' => $post_data['title'], 'id !=' => $post_data['id'], 'root' => 0))) {
                $data = $this->m_menus->update($post_data, array('id' => $post_data['id']));
                if ($data) {
                    $r['status'] = TRUE;
                } else {
                    $r['status'] = FALSE;
                    $r['message'] = "Lỗi trong quá trình tạo menu admin::m_menu";
                }
            } else {
                $r['status'] = FALSE;
                $r['message'] = "Tên menu này đã tồn tại!";
            }
        }

        echo json_encode($r);
    }

    public function delete_root_menu() {
        $id = intval(trim($this->input->get_post('id', TRUE)));
        $r = array();
        if ($id == 0) {
            $r['status'] = FALSE;
            $r['message'] = "Không lấy được thông tin menu::id";
        } else {
            $r2 = $this->m_menus->delete(array('root' => $id));
            $r2 &= $this->m_menus->delete(array('id' => $id));
            if (!$r2) {
                $r['status'] = FALSE;
                $r['message'] = "Lỗi trong admin::m_menu->delete";
            } else {
                $r['status'] = TRUE;
            }
        }
        echo json_encode($r);
    }

    public function load_menu() {

        $root = intval(trim($this->input->get_post('root', TRUE)));

        $r = array();

        if ($root == 0) {
            $r['status'] = FALSE;
            $r['mess'] = "Không lấy được root";
        }
        $menu_root = $this->m_menus->get_data("id, title", array('id' => $root));
        $menu = $this->m_menus->get_data("*", array('root' => $root, 'id !=' => $root), array(), $order = 'order ASC');
        $data = array(
            'menu_root' => $menu_root,
            'menu' => json_encode($menu),
        );
        $this->load->view('/admin/menus/menu', $data);
    }

    public function load_menu_data() {

        $root = intval(trim($this->input->get_post('root', TRUE)));

        $menu = $this->m_menus->get_data("*", array('root' => $root, 'id !=' => $root), array(), $order = 'order ASC');

        echo json_encode($menu);
    }

    public function insert_menu() {

        $post_data = array(
            'title' => trim($this->input->get_post('title', TRUE)),
            'url' => trim($this->input->get_post('url', TRUE)),
            'target' => intval(trim($this->input->get_post('target', TRUE))),
            'icon' => trim($this->input->get_post('icon', TRUE)),
            'thumbnail' => trim($this->input->get_post('thumbnail', TRUE)),
            'css' => trim($this->input->get_post('css', TRUE)),
            'root' => intval(trim($this->input->get_post('root', TRUE))),
            'parent' => intval(trim($this->input->get_post('parent', TRUE))),
                //'order' => intval(trim($this->input->get_post('order', TRUE))),
                //'status'=>intval(trim($this->input->get_post('status', TRUE))),
        );

        //print_r($post_data);

        $r = array();
        if ($this->_validate_menu($post_data) != '') {
            $r['status'] = FALSE;
            $r['message'] = $this->_validate_menu($post_data);
        } else {
            $r2 = $this->m_menus->insert($post_data);
            if ($r2 == 1) {
                $r['status'] = TRUE;
            } else {
                $r['status'] = FALSE;
                $r['message'] = "Lỗi trong m_menu!";
            }
        }

        echo json_encode($r);
    }

    public function update_menu() {
        //sleep(5);
        $post_data = array(
            'id' => intval(trim($this->input->get_post('id', TRUE))),
            'title' => trim($this->input->get_post('title', TRUE)),
            'url' => trim($this->input->get_post('url', TRUE)),
            'target' => intval(trim($this->input->get_post('target', TRUE))),
            'icon' => trim($this->input->get_post('icon', TRUE)),
            'thumbnail' => trim($this->input->get_post('thumbnail', TRUE)),
            'css' => trim($this->input->get_post('css', TRUE)),
                //'root' => intval(trim($this->input->get_post('root', TRUE))),
                //'parent' => intval(trim($this->input->get_post('parent', TRUE))),
                //'order' => intval(trim($this->input->get_post('order', TRUE))),
                //'status'=>intval(trim($this->input->get_post('status', TRUE))),
        );
        $r = array();
        if ($this->_validate_menu($post_data) != '') {
            $r['status'] = FALSE;
            $r['message'] = $this->_validate_menu($post_data);
        } else {
            $r2 = $this->m_menus->update($post_data, array('id' => $post_data['id']));
            if ($r2 == 1) {
                $r['status'] = TRUE;
            } else {
                $r['status'] = FALSE;
                $r['message'] = "Lỗi trong m_menu!";
            }
        }

        echo json_encode($r);
    }

    public function delete_menu() {

        $id = intval(trim($this->input->get_post('id', TRUE)));
        $root = intval(trim($this->input->get_post('root', TRUE)));

        $r = array();

        if (!isset($id) || $id == 0) {
            $r['status'] = FALSE;
            $r['message'] = "Không lấy được id";
        } else if (!isset($root) || $root == 0) {
            $r['status'] = FALSE;
            $r['message'] = "Không lấy được root";
        } else {
            $data = $this->m_menus->get_data("id, parent", array('root' => $root, 'id !=' => $id));
            $arr_id = array();
            if (is_array($data) && count($data) > 0) {
                $arr_id = $this->_get_child_menu_id($data, $id);
            }
            array_push($arr_id, $id);

            $rs = $this->m_menus->delete('id IN(' . implode(',', $arr_id) . ')');
            if (!$rs) {
                $r['status'] = FALSE;
                $r['message'] = "Lỗi trong admin:m_menu->delete_menu";
            } else {
                $r['status'] = TRUE;
            }
        }

        echo json_encode($r);
    }

    function save_order() {
        $menus = $this->input->get_post('menus', TRUE);
        $root = intval(trim($this->input->get_post('root', TRUE)));
        $r = array();
        if (!is_array($menus) || $root == 0) {
            $r['status'] = FALSE;
            $r['message'] = "Dữ liệu đưa lên không đúng!";
        } else {
            $this->_save_menu_order($menus, $root);
            $r['status'] = TRUE;
        }
        echo json_encode($r);
    }

    protected function _save_menu_order($menus, $parent) {
        $i = 0;
        foreach ($menus as $item) {
            if (isset($item['id']) && $item['id'] > 0) {
                $i++;
                $this->m_menus->update(array('order' => $i, 'parent' => $parent), array('id' => $item['id']));
                if (isset($item['children'])) {
                    $this->_save_menu_order($item['children'], $item['id']);
                }
            }
        }
    }

    protected function _get_child_menu_id($data, $parent) {

        $menus = array();

        foreach ($data as $k => $v) {
            if ($data[$k]['parent'] == $parent) {
                $tmp_parent = $data[$k]['id'];
                array_push($menus, $data[$k]['id']);
                unset($data[$k]);
                //$menus = array_unique(array_merge($menus, $this->_get_child_menu_id($data, $tmp_parent)));
                $menus = array_merge($menus, $this->_get_child_menu_id($data, $tmp_parent));
            }
        }

        return $menus;
    }

    protected function _validate_menu($post_data) {
        $mess = '';
        if ($mess == '') {
            if (isset($post_data['id']) && $post_data['id'] == 0) {
                $mess = "Không lấy được id menu!";
            }
        }
        if ($mess == '') {
            if (!isset($post_data['title']) || $post_data['title'] == '') {
                $mess = "Nhập tiêu đề!";
            } else {
                if (preg_match('/\'|\"|\`/', $post_data['title'])) {
                    $mess = "Tiêu đề không được chứa các ký tự đặc biệt!";
                }
            }
        }

        if ($mess == '') {
            if (isset($post_data['root']) && $post_data['root'] == 0) {
                $mess = "Không lấy được root menu! ";
            }
        }
        if ($mess == '') {
            if (isset($post_data['parent']) && $post_data['parent'] == 0) {
                $mess = "Không lấy được parent ";
            }
        }
        return $mess;
    }

}
