<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of login
 *
 * @author PHAMTANPHU
 */
class login extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model("admin/m_login");
    }

    public function index() {

        if ($this->session->userdata('logged_user')) {
            redirect(base_url() . 'admin/dashboard', 'location');
        } else {
            $cookie_user = json_decode(get_cookie('logged_user'), TRUE);
            $data['username'] = $cookie_user['username'];
            $data['remember'] = $cookie_user['remember'];
            $this->load->view('admin/login/login', $data);
        }
    }

    public function lost_password() {
        $this->load->view('admin/login/lost_password');
    }

    public function check_login() {

        $rs = array("status" => TRUE);
        $status = TRUE;
        $mess = '';

        $username = addslashes(trim($this->input->get_post("username", TRUE)));
        $password = md5(PASSWORD_PREFIX . $this->input->get_post("password", TRUE));
        $now_time = time();

        $where = "`username` = '" . $username . "' OR `email` = '" . $username . "' OR `phone` = '" . $username . "'";

        $user = $this->m_login->get_data("*", $where);

        if ($status) {
            if (count($user) < 1) {
                $status = FALSE;
                $mess = 'Tên đăng nhập hoặc mật khẩu không đúng!';
            }
        }

        if ($status) {
            $user = $user[0];
        }

        if ($status) {
            if ($user['status'] == 0) {
                $status = FALSE;
                $mess = 'Người dùng này đã bị khóa!';
            }
        }

        if ($status) {
            if ($password != $user['password_reset'] && $now_time < $user['left_time_login'] && $user['login_count'] > 5) {
                $status = FALSE;
                $mess = 'Bạn đã đăng nhập quá số lần qui định, Vui lòng chờ trong ' . ceil(($user['left_time_login'] - $now_time) / 60) . " phút!";
            }
        }

        if ($status) {
            // note: password and password_reset must different
            if ($password == $user['password_reset'] && $now_time > $user['left_time_reset']) {
                $status = FALSE;
                $mess = 'Mật khẩu khôi phục của đã hết hạn, Vui lòng khôi phục lại!';
            }
        }


        if ($status) {

            if ($password != $user['password'] && $password != $user['password_reset']) {
                $status = FALSE;
                $mess = 'Tên đăng nhập hoặc mật khẩu không đúng!';

                $data_set = array("login_count" => ($user['login_count'] + 1));

                if ($user['login_count'] >= 5) {
                    $data_set['left_time_login'] = time() + (15 * 60);
                }

                $this->m_login->update($data_set, array('id' => $user['id']));
            }
        }


        if ($status) {

            $data_set = array(
                'password' => $password,
                'password_reset' => '',
                'login_count' => 0,
                'left_time_login' => 0,
                'left_time_reset' => 0,
            );

            $this->m_login->update($data_set, array('id' => $user['id']));
            // set session
            $this->session->set_userdata("logged_user", $user);
        }

        if (!$status) {
            $rs['status'] = FALSE;
            $rs['message'] = $mess;
        }

        echo json_encode($rs);

        /* $ref = $this->input->get_post("ref", TRUE);
          $ref = ($ref=="")?base_url()."admin/" : $ref;
          return redirect($ref); */
    }

    public function reset_password() {

        $username = trim($this->input->get_post("username", TRUE));
        $email = trim($this->input->get_post("email", TRUE));

        $rs = array("status" => TRUE);
        $status = TRUE;
        $mess = '';

        if ($status) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $status = FALSE;
                $mess = 'Email không đúng!';
            }
        }

        if ($status) {
            $user = $this->m_login->get_data("*", array('username' => $username, 'email' => $email));
        }

        if ($status) {
            if (count($user) < 1) {
                $status = FALSE;
                $mess = 'Tên đăng nhập hoặc email không đúng!';
            }
        }

        if ($status) {
            if ($user[0]['status'] == 0) {
                $status = FALSE;
                $mess = 'Người dùng này đã bị khóa!';
            }
        }

        if ($status) {
            $new_pass = base_convert(rand(10e16, 10e20), 10, 36);
            $password_reset = md5(PASSWORD_PREFIX . md5($new_pass));
            while ($password_reset == $user[0]['password']) {
                $new_pass = base_convert(rand(10e16, 10e20), 10, 36);
                $password_reset = md5(PASSWORD_PREFIX . md5($new_pass));
            }

            $this->load->library('email');

            $config['protocol'] = 'smtp';
            $config['smtp_host'] = 'ssl://smtp.gmail.com';
            $config['smtp_port'] = '465';
            $config['smtp_timeout'] = '7';
            $config['smtp_user'] = 'phamphu232@gmail.com';
            $config['smtp_pass'] = 'yxhslvapwbvxqsfp';
            $config['charset'] = 'utf-8';
            $config['newline'] = "\r\n";
            $config['mailtype'] = 'text'; // or html
            $config['validation'] = TRUE; // bool whether to validate email or not      

            $this->email->initialize($config);

            $this->email->from('phamphu232@gmail.com', 'Phạm Tân Phú');
            $this->email->to($email);

            $this->email->subject('Khôi phục mật khẩu');
            $this->email->message("Mật khẩu khôi phục của bạn là: " . $new_pass . "\r\n Bạn vui lòng đổi lại mật khẩu trong 3 giờ!");

            $this->email->send();

            //print_r($this->email->print_debugger());

            $this->m_login->update(array("password_reset" => $password_reset, "left_time_reset" => (time() + (60 * 60 * 4))), array('username' => $username, 'email' => $email));
        }

        if (!$status) {
            $rs['status'] = FALSE;
            $rs['message'] = $mess;
        }

        echo json_encode($rs);
    }

    public function logout() {
        $this->session->sess_destroy();
        //$this->session->unset_userdata('logged_user');
        redirect(base_url() . 'admin/login', 'location');
    }

}
