<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of upload
 *
 * @author PHAMTANPHU
 */
class upload extends MY_Admin_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        echo "Access :(";
    }

    public function upload_image() {
        $config['upload_path'] = './public/upload/slider/';
        $config['file_name'] = "slider" . date("YmdHis");
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '5000';
        $config['max_width'] = '1024';
        $config['max_height'] = '768';

        print_r($config);

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload("file")) {
            print_r($this->upload->display_errors());
        } else {
            print_r($this->upload->data());
        }
    }

}
