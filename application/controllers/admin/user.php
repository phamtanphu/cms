<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of user
 *
 * @author PHAMTANPHU
 */
class user extends MY_Admin_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model("admin/m_user");
    }

    public function index() {

        $data = array();
        $data['title'] = 'Người Dùng';

        $this->layout->admin_view('user/index', $data);
    }

    public function get_user() {

        $where = array();
        $like = array();
        $start_index = intval($this->input->get_post('jtStartIndex'));
        $limit = $this->input->get_post('jtPageSize');
        $order_by = (trim($this->input->get_post('jtSorting')) == '') ? 'id ASC' : trim($this->input->get_post('jtSorting'));

        /* get param to WHERE OR LIKE */
        $arr_param = $this->input->post();
        if (isset($arr_param) && is_array($arr_param) > 0) {
            foreach ($arr_param as $k => $v) {
                $k = trim($k);
                $v = trim($v);
                if ($v != "") {
                    /* Tham số where */
                    $arrw = explode("_w_", $k);
                    if (isset($arrw[1]) && trim($arrw[1]) != "") {
                        $k = (trim($arrw[0]) != "") ? $arrw[0] . '.' . $arrw[1] : $arrw[1];
                        $where[$k] = $v;
                    }

                    /* Tham số like */
                    $arrl = explode("_l_", $k);
                    if (isset($arrl[1]) && trim($arrl[1]) != "") {
                        $k = (trim($arrl[0]) != "") ? $arrl[0] . '.' . $arrl[1] : $arrl[1];
                        //advangce
                        $operator_str = array('[[LIKE]]', '[[NOT LIKE]]', '[[>]]', '[[<]]', '[[=]]', '[[>=]]', '[[<=]]', '[[!=]]', '[[REGEXP]]', '[[REGEXP^$]]', '[[NOT REGEXP]]', '[[= ""]]', '[[!= ""]]', '[[IN]]', '[[NOT IN]]', '[[BETWEEN]]', '[[NOT BETWEEN]]', '[[IS NULL]]', '[[IS NOT NULL]]');
                        $operator = array('LIKE', 'NOT LIKE', '>', '<', '=', '>=', '<=', '!=', 'REGEXP', 'REGEXP^$', 'NOT REGEXP', '= ""', '!= ""', 'IN', 'NOT IN', 'BETWEEN', 'NOT BETWEEN', 'IS NULL', 'IS NOT NULL');
                        if ($v != ($v2 = str_replace($operator_str, $operator, $v))) {
                            $where[$k . ' ' . $v2] = NULL;
                        } else {
                            $like[$k] = $v;
                        }
                    }
                }
            }
        }

        $data = $this->m_user->get_data("*", $where, $like, $order_by, $start_index, $limit);

        $jtable = array();
        $jtable['Result'] = 'OK';
        $jtable['Records'] = $data;
        $jtable['TotalRecordCount'] = $this->m_user->get_num_rows($where, $like);

        //header('Content-Type: application/json');
        print json_encode($jtable);
    }

    public function insert_user() {

        $post_data = $this->input->post();

        $post_data = array_map(function($value) {
            return trim($value);
        }, $post_data);

        $r = array("Result" => "OK");
        $status = TRUE;
        $mess = '';

        if ($status) {
            if ($this->m_user->is_exist(array("username" => $post_data['username']))) {
                $status = FALSE;
                $mess = 'Tên đăng nhập: "' . $post_data['username'] . '" đã tồn tại!';
            }
        }

        if ($status) {
            if ($this->m_user->is_exist(array("email" => $post_data['email']))) {
                $status = FALSE;
                $mess = 'Email: "' . $post_data['email'] . '" đã tồn tại!';
            } else {
                if (!filter_var($post_data['email'], FILTER_VALIDATE_EMAIL)) {
                    $status = FALSE;
                    $mess = 'Email: "' . $post_data['email'] . '" không đúng';
                }
            }
        }

        if ($status && $post_data['phone'] != '') {
            if ($this->m_user->is_exist(array("phone" => $post_data['phone']))) {
                $status = FALSE;
                $mess = 'Số điện thoại: "' . $post_data['phone'] . '" đã tồn tại!';
            }
        }

        if ($status) {
            $post_data['password'] = md5(PASSWORD_PREFIX . $post_data['password']);
            $status = $this->m_user->insert($post_data);
        }

        if (!$status) {
            $r['Result'] = 'ERROR';
            $r['Message'] = $mess;
        } else {
            $r['Record'] = $post_data;
        }

        echo json_encode($r);
    }

    public function update_user() {

        $post_data = $this->input->post();

        $post_data = array_map(function($value) {
            return trim($value);
        }, $post_data);
        $id = intval($post_data['id']);
        unset($post_data['id']);

        $r = array("Result" => "OK");
        $status = TRUE;
        $mess = '';

        if ($status) {
            if ($id == 0) {
                $status = FALSE;
                $mess = 'Tên đăng nhập: "' . $post_data['username'] . '" đã tồn tại!';
            }
        }

        if ($status) {
            if ($this->m_user->is_exist(array("username" => $post_data['username'], 'id !=' => $id))) {
                $status = FALSE;
                $mess = 'Tên đăng nhập: "' . $post_data['username'] . '" đã tồn tại!';
            }
        }

        if ($status) {
            if ($this->m_user->is_exist(array("email" => $post_data['email'], 'id !=' => $id))) {
                $status = FALSE;
                $mess = 'Email: "' . $post_data['email'] . '" đã tồn tại!';
            } else {
                if (!filter_var($post_data['email'], FILTER_VALIDATE_EMAIL)) {
                    $status = FALSE;
                    $mess = 'Email: "' . $post_data['email'] . '" không đúng';
                }
            }
        }

        if ($status && $post_data['phone'] != '') {
            if ($this->m_user->is_exist(array("phone" => $post_data['phone'], 'id !=' => $id))) {
                $status = FALSE;
                $mess = 'Số điện thoại: "' . $post_data['phone'] . '" đã tồn tại!';
            }
        }

        if ($status) {
            $status = $this->m_user->update($post_data, array("id" => $id));
        }

        if (!$status) {
            $r['Result'] = 'ERROR';
            $r['Message'] = $mess;
        }

        echo json_encode($r);
    }

    public function reset_password() {

        $status = TRUE;
        $mess = '';

        $users = trim($this->input->get_post("users", TRUE));
        $password = md5(PASSWORD_PREFIX . $this->input->get_post("password", TRUE));

        if ($status) {
            if ($users == '') {
                $status = FALSE;
                $mess = 'Chọn người dùng!';
            }
        }

        if ($status) {
            if ($password == '') {
                $status = FALSE;
                $mess = 'Nhập mật khẩu!';
            }
        }

        if ($status) {
            $status = $this->m_user->update(array("password" => $password), "username IN('" . str_replace(",", "', '", $users) . "')");
            $mess = ($status) ? "Khôi phục mật khẩu thành công!" : "Có lỗi xảy ra trong quá trình khôi lục mật khẩu!";
        }

        echo json_encode(array("status" => $status, "message" => $mess));
    }

    public function change_password() {

        $data = array();
        $data['title'] = 'Đổi Mật Khẩu';

        $data['breadcrum'] = array(
            array('link' => '', 'text' => 'Đổi Mật Khẩu')
        );

        $this->layout->admin_view('user/change_password', $data);
    }

    public function change_password_action() {

        $rs = array("status" => TRUE);
        $status = TRUE;
        $mess = '';

        $username = $this->session->userdata('logged_user')['username'];
        $password = md5(PASSWORD_PREFIX . $this->input->get_post("password", TRUE));
        $new_password = md5(PASSWORD_PREFIX . $this->input->get_post("new_password", TRUE));

        if ($status) {
            if (!$this->m_user->is_exist(array("username" => $username, 'password' => $password))) {
                $status = FALSE;
                $mess = 'Mật khẩu cũ không đúng!';
            }
        }

        if ($status) {
            $status = $this->m_user->update(array("password" => $new_password), array("username" => $username, 'password' => $password));
            if (!$status) {
                $status = FALSE;
                $mess = 'Lỗi change_password_action!';
            }
        }

        if (!$status) {
            $rs['status'] = FALSE;
            $rs['message'] = $mess;
        }

        echo json_encode($rs);
    }

}
