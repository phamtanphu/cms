<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of email
 *
 * @author PHAMTANPHU
 */
class email extends MY_Admin_Controller {

//put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model("admin/m_email");
    }

    public function index() {

        $data = array();
        $data['title'] = 'Cấu Hình Email';

        $this->layout->admin_view('email/index', $data);
    }

    public function get_email() {

        $where = array();
        $like = array();
        $start_index = intval($this->input->get_post('jtStartIndex'));
        $limit = $this->input->get_post('jtPageSize');
        $order_by = (trim($this->input->get_post('jtSorting')) == '') ? 'id ASC' : trim($this->input->get_post('jtSorting'));

        /* get param to WHERE OR LIKE */
        $arr_param = $this->input->post();
        if (isset($arr_param) && is_array($arr_param) > 0) {
            foreach ($arr_param as $k => $v) {
                $k = trim($k);
                $v = trim($v);
                if ($v != "") {
                    /* Tham số where */
                    $arrw = explode("_w_", $k);
                    if (isset($arrw[1]) && trim($arrw[1]) != "") {
                        $k = (trim($arrw[0]) != "") ? $arrw[0] . '.' . $arrw[1] : $arrw[1];
                        $where[$k] = $v;
                    }

                    /* Tham số like */
                    $arrl = explode("_l_", $k);
                    if (isset($arrl[1]) && trim($arrl[1]) != "") {
                        $k = (trim($arrl[0]) != "") ? $arrl[0] . '.' . $arrl[1] : $arrl[1];
                        //advangce
                        $operator_str = array('[[LIKE]]', '[[NOT LIKE]]', '[[>]]', '[[<]]', '[[=]]', '[[>=]]', '[[<=]]', '[[!=]]', '[[REGEXP]]', '[[REGEXP^$]]', '[[NOT REGEXP]]', '[[= ""]]', '[[!= ""]]', '[[IN]]', '[[NOT IN]]', '[[BETWEEN]]', '[[NOT BETWEEN]]', '[[IS NULL]]', '[[IS NOT NULL]]');
                        $operator = array('LIKE', 'NOT LIKE', '>', '<', '=', '>=', '<=', '!=', 'REGEXP', 'REGEXP^$', 'NOT REGEXP', '= ""', '!= ""', 'IN', 'NOT IN', 'BETWEEN', 'NOT BETWEEN', 'IS NULL', 'IS NOT NULL');
                        if ($v != ($v2 = str_replace($operator_str, $operator, $v))) {
                            $where[$k . ' ' . $v2] = NULL;
                        } else {
                            $like[$k] = $v;
                        }
                    }
                }
            }
        }

        $select_field = array("id", "name", "value", "status");
        $where['group'] = 'email';
        $data = $this->m_email->get_data($select_field, $where, $like, $order_by, $start_index, $limit);

        $jtable = array();
        $jtable['Result'] = 'OK';
        $jtable['Records'] = $data;
        $jtable['TotalRecordCount'] = $this->m_email->get_num_rows($where, $like);

        //header('Content-Type: application/json');
        print json_encode($jtable);
    }

    public function insert_email() {

        $rs = array("Result" => "OK");
        $status = TRUE;
        $mess = '';

        $post_data = array(
            'name' => trim($this->input->get_post('name', TRUE)),
            'value' => trim($this->input->get_post('value', TRUE)),
            'group' => 'email',
            'status' => intval(trim($this->input->get_post('status', TRUE))),
        );

        if ($status) {
            if ($post_data['name'] == '') {
                $status = FALSE;
                $mess = 'name Không được rỗng!';
            }
        }

        if ($status) {
            $q = $this->m_email->insert($post_data);
            if (!$q) {
                $status = FALSE;
                $mess = 'Lỗi trong m_email->insert';
            }
        }

        if (!$status) {
            $rs['Result'] = "ERROR";
            $rs['Message'] = $mess;
        } else {
            $rs['Record'] = $post_data;
        }

        echo json_encode($rs);
    }

    public function update_email() {

        $rs = array("Result" => "OK");
        $status = TRUE;
        $mess = '';

        $id = intval(trim($this->input->get_post('id', TRUE)));

        $post_data = array(
            'name' => trim($this->input->get_post('name', TRUE)),
            'value' => trim($this->input->get_post('value', TRUE)),
            'status' => intval(trim($this->input->get_post('status', TRUE))),
        );

        if ($status) {
            if ($id == 0) {
                $status = FALSE;
                $mess = 'Không lấy được ID!';
            }
        }

        if ($status) {
            if ($post_data['name'] == '') {
                $status = FALSE;
                $mess = 'name Không được rỗng!';
            }
        }

        if ($status) {
            //print_r($post_data); exit();
            $q = $this->m_email->update($post_data, array("id" => $id, "group" => "email"));
            if (!$q) {
                $status = FALSE;
                $mess = 'Lỗi trong m_email->update';
            }
        }

        if (!$status) {
            $rs['Result'] = "ERROR";
            $rs['Message'] = $mess;
        }

        echo json_encode($rs);
    }

    public function delete_email() {
		
		return;

        $rs = array("Result" => "OK");
        $status = TRUE;
        $mess = '';

        $id = intval(trim($this->input->get_post('id', TRUE)));

        if ($status) {
            if ($id == 0) {
                $status = FALSE;
                $mess = 'Không lấy được ID!';
            }
        }

        if ($status) {
            $q = $this->m_email->delete(array("id" => $id));
            if (!$q) {
                $status = FALSE;
                $mess = 'Lỗi trong m_email->delete';
            }
        }

        if (!$status) {
            $rs['Result'] = "ERROR";
            $rs['Message'] = $mess;
        }

        echo json_encode($rs);
    }

}
