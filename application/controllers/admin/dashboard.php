<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author PHAMTANPHU
 */
class dashboard extends MY_Admin_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $data = array();
        $data['title'] = 'Bảng điều khiển';
        $data['breadcrum'] = array(
            array('' => '', 'text' => 'Admin Panel'),
        );
        $data['data'] = 'Chào mừng bạn đến với admin panel';

        $this->layout->admin_view('dashboard/index', $data);
		
		//print_r($this);
    }

}
