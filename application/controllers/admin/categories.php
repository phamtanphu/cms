<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of post
 *
 * @author PHAMTANPHU
 */
class categories extends MY_Admin_Controller {

//put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model("admin/m_categories");
    }

    public function index() {

        $data = array();
        $data['title'] = 'Chuyên mục';

        $data['breadcrum'] = array(
            //array('link' => 'admin', 'text' => 'Admin Panel'),
            array('link' => '', 'text' => 'Chuyên Mục')
        );

        $this->layout->admin_view('categories/index', $data);
    }

    public function get_data() {

        $where = array();
        $like = array();
        $start_index = intval($this->input->get_post('jtStartIndex'));
        $limit = $this->input->get_post('jtPageSize');
        $order_by = (trim($this->input->get_post('jtSorting')) == '') ? 'id ASC' : trim($this->input->get_post('jtSorting'));

        /* get param to WHERE OR LIKE */
        $arr_param = $this->input->post();
        if (isset($arr_param) && is_array($arr_param) > 0) {
            foreach ($arr_param as $k => $v) {
                $k = trim($k);
                $v = trim($v);
                if ($v != "") {
                    /* Tham số where */
                    $arrw = explode("_w_", $k);
                    if (isset($arrw[1]) && trim($arrw[1]) != "") {
                        $k = (trim($arrw[0]) != "") ? $arrw[0] . '.' . $arrw[1] : $arrw[1];
                        $where[$k] = $v;
                    }

                    /* Tham số like */
                    $arrl = explode("_l_", $k);
                    if (isset($arrl[1]) && trim($arrl[1]) != "") {
                        $k = (trim($arrl[0]) != "") ? $arrl[0] . '.' . $arrl[1] : $arrl[1];
                        //advangce
                        $operator_str = array('[[LIKE]]', '[[NOT LIKE]]', '[[>]]', '[[<]]', '[[=]]', '[[>=]]', '[[<=]]', '[[!=]]', '[[REGEXP]]', '[[REGEXP^$]]', '[[NOT REGEXP]]', '[[= ""]]', '[[!= ""]]', '[[IN]]', '[[NOT IN]]', '[[BETWEEN]]', '[[NOT BETWEEN]]', '[[IS NULL]]', '[[IS NOT NULL]]');
                        $operator = array('LIKE', 'NOT LIKE', '>', '<', '=', '>=', '<=', '!=', 'REGEXP', 'REGEXP^$', 'NOT REGEXP', '= ""', '!= ""', 'IN', 'NOT IN', 'BETWEEN', 'NOT BETWEEN', 'IS NULL', 'IS NOT NULL');
                        if ($v != ($v2 = str_replace($operator_str, $operator, $v))) {
                            $where[$k . ' ' . $v2] = NULL;
                        } else {
                            $like[$k] = $v;
                        }
                    }
                }
            }
        }

        $select_field = array("id", "name", "description", "slug", "parent");

        $data = $this->m_categories->get_data($select_field, $where, $like, $order_by, $start_index, $limit);

        $this->load->helper('child');
        $records = _order_parent_child($data, $parent = 0, $key = 'id', $parent_key = 'parent', $show_key = 'name', $sperator = '', $sperator_child = '__');

        $jtable = array();
        $jtable['Result'] = 'OK';
        $jtable['Records'] = $records;
        $jtable['TotalRecordCount'] = $this->m_categories->get_num_rows($where, $like);

        //header('Content-Type: application/json');
        print json_encode($jtable);
    }

    public function update_data() {

        $this->load->helper('slug');

        $post_data = array(
            'id' => intval($this->input->get_post('id', TRUE)),
            'name' => trim($this->input->get_post('name', TRUE)),
            'slug' => trim($this->input->get_post('slug', TRUE)),
            'description' => trim($this->input->get_post('description', TRUE)),
            'parent' => intval(trim($this->input->get_post('parent', TRUE))),
        );

        if ($post_data['slug'] == '') {
            $post_data['slug'] = to_slug($post_data['name']);
        } else {
            $post_data['slug'] = to_slug($post_data['slug']);
        }

        $jtResult = array();
        if ($this->_validate_category($post_data) != '') {
            $jtResult['Result'] = 'ERROR';
            $jtResult['Message'] = $this->_validate_category($post_data);
        } else {

            if ($num_slug = $this->m_categories->get_num_rows(array('id !=' => $post_data['id'], 'slug' => $post_data['slug'])) > 0) {
                $post_data['slug'] = to_slug($post_data['name']) . '-' . $post_data['id'];
            }

            $r = $this->m_categories->update($post_data, array('id' => $post_data['id']));
            if ($r == 1) {
                $jtResult['Result'] = 'OK';
            } else {
                $jtResult['Result'] = 'ERROR';
                $jtResult['Message'] = "Lỗi trong model!";
            }
        }

        echo json_encode($jtResult);
    }

    public function insert_data() {

        $this->load->helper('slug');

        $post_data = array(
            'name' => trim($this->input->get_post('name', TRUE)),
            'slug' => trim($this->input->get_post('slug', TRUE)),
            'description' => trim($this->input->get_post('description', TRUE)),
            'parent' => intval(trim($this->input->get_post('parent', TRUE))),
        );

        if ($post_data['slug'] == '') {
            $post_data['slug'] = to_slug($post_data['name']);
        } else {
            $post_data['slug'] = to_slug($post_data['slug']);
        }

        $jtResult = array();
        if ($this->_validate_category($post_data) != '') {
            $jtResult['Result'] = 'ERROR';
            $jtResult['Message'] = $this->_validate_category($post_data);
        } else {

            if ($num_slug = $this->m_categories->get_num_rows(array('slug' => $post_data['slug'])) > 0) {
                $maxid = $this->m_categories->get_maxid();
                $post_data['slug'] = $post_data['slug'] . '-' . ($maxid + 1);
            }

            $r = $this->m_categories->insert($post_data);
            if ($r == 1) {
                $jtResult['Result'] = 'OK';
                $jtResult['Record'] = $post_data;
            } else {
                $jtResult['Result'] = 'ERROR';
                $jtResult['Message'] = "Lỗi trong model!";
            }
        }

        echo json_encode($jtResult);
    }

    public function delete_data() {

        $id = intval(trim($this->input->get_post('id', TRUE)));

        $r = array();
        if (!isset($id) || $id == 0) {
            $r['Result'] = 'ERROR';
            $r['Message'] = "Không lấy được id";
        } else {
            $data = $this->m_categories->get_data("id, parent");
            $child_data = array();
            if (is_array($data) && count($data) > 0) {
                $this->load->helper('child');
                $child_data = _get_child($data, $id);
            }

            $arr_delete = array();
            foreach ($child_data as $item) {
                array_push($arr_delete, $item['id']);
            }
            array_push($arr_delete, $id);

            $this->load->model("admin/m_post_category", "m_post_category");

            if (!$this->m_post_category->is_exist('category_id IN(' . implode(',', $arr_delete) . ')')) {

                $rs = $this->m_categories->delete('id IN(' . implode(',', $arr_delete) . ')');

                if (!$rs) {
                    $r['Result'] = 'ERROR';
                    $r['Message'] = "Lỗi trong admin:m_categories->delete_data";
                } else {
                    $r['Result'] = 'OK';
                }
            } else {
                $r['Result'] = 'ERROR';
                $r['Message'] = "Vui lòng xóa những bài viết thuộc chuyên mục bạn muốn xóa trước!";
            }
        }

        echo json_encode($r);
    }

    protected function _validate_category($post_data) {
        $mess = '';
        if ($mess == '') {
            if (isset($post_data['id']) && $post_data['id'] == 0) {
                $mess = "Không lấy được thông tin chuyên mục::id";
            }
        }
        if ($mess == '') {
            if (!isset($post_data['name']) || $post_data['name'] == '') {
                $mess = "Nhập tên!";
            }
        }
        return $mess;
    }

}
