<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of slider
 *
 * @author PHAMTANPHU
 */
class slider extends MY_Admin_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model("admin/m_slider");
    }

    public function index() {

        $data = array();
        $data['title'] = 'Slider';

        $this->layout->admin_view('slider/index', $data);
    }

    public function get_slider() {
        
        $where = array();
        $like = array();
        $start_index = intval($this->input->get_post('jtStartIndex'));
        $limit = $this->input->get_post('jtPageSize');
        $order_by = (trim($this->input->get_post('jtSorting')) == '') ? 'order DESC' : trim($this->input->get_post('jtSorting'));

        /* get param to WHERE OR LIKE */
        $arr_param = $this->input->post();
        if (isset($arr_param) && is_array($arr_param) > 0) {
            foreach ($arr_param as $k => $v) {
                $k = trim($k);
                $v = trim($v);
                if ($v != "") {
                    /* Tham số where */
                    $arrw = explode("_w_", $k);
                    if (isset($arrw[1]) && trim($arrw[1]) != "") {
                        $k = (trim($arrw[0]) != "") ? $arrw[0] . '.' . $arrw[1] : $arrw[1];
                        $where[$k] = $v;
                    }

                    /* Tham số like */
                    $arrl = explode("_l_", $k);
                    if (isset($arrl[1]) && trim($arrl[1]) != "") {
                        $k = (trim($arrl[0]) != "") ? $arrl[0] . '.' . $arrl[1] : $arrl[1];
                        //advangce
                        $operator_str = array('[[LIKE]]', '[[NOT LIKE]]', '[[>]]', '[[<]]', '[[=]]', '[[>=]]', '[[<=]]', '[[!=]]', '[[REGEXP]]', '[[REGEXP^$]]', '[[NOT REGEXP]]', '[[= ""]]', '[[!= ""]]', '[[IN]]', '[[NOT IN]]', '[[BETWEEN]]', '[[NOT BETWEEN]]', '[[IS NULL]]', '[[IS NOT NULL]]');
                        $operator = array('LIKE', 'NOT LIKE', '>', '<', '=', '>=', '<=', '!=', 'REGEXP', 'REGEXP^$', 'NOT REGEXP', '= ""', '!= ""', 'IN', 'NOT IN', 'BETWEEN', 'NOT BETWEEN', 'IS NULL', 'IS NOT NULL');
                        if ($v != ($v2 = str_replace($operator_str, $operator, $v))) {
                            $where[$k . ' ' . $v2] = NULL;
                        } else {
                            $like[$k] = $v;
                        }
                    }
                }
            }
        }

        $data = $this->m_slider->get_data("*", $where, $like, $order_by, $start_index, $limit);

        $jtable = array();
        $jtable['Result'] = 'OK';
        $jtable['Records'] = $data;
        $jtable['TotalRecordCount'] = $this->m_slider->get_num_rows($where, $like);

        //header('Content-Type: application/json');
        print json_encode($jtable);
    }

    public function insert_slider() {

        $r = array("status" => TRUE);
        $status = TRUE;
        $mess = '';

        $post_data = $this->input->post();
        $get_maxorder = $this->m_slider->get_maxorder();
        $post_data['order'] = $get_maxorder + 1;

        $config['upload_path'] = './public/upload/slider/';
        $config['file_name'] = "Slider_" . date("YmdHis");
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '5000';
        $config['max_width'] = '1024';
        $config['max_height'] = '768';

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload("file")) {
            $status = FALSE;
            $mess = $this->upload->display_errors();
        } else {
            $post_data['thumbnail'] = 'public/upload/slider/' . $this->upload->data()['file_name'];
        }

        if ($status) {
            $status = $this->m_slider->insert($post_data);
        }

        if (!$status) {
            $r['status'] = FALSE;
            $r['message'] = $mess;
        }

        echo json_encode($r);
    }

    public function update_slider() {

        $r = array("status" => TRUE);
        $status = TRUE;
        $mess = '';

        $post_data = $this->input->post();
        unset($post_data['id']);
        $id = intval($this->input->get_post("id", TRUE));

        if ($status) {
            if ($id == 0) {
                $status = FALSE;
                $mess = 'Không lấy được ID Slider';
            }
        }

        if ($status) {
            $config['upload_path'] = './public/upload/slider/';
            $config['file_name'] = "Slider_" . date("YmdHis");
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '5000';
            $config['max_width'] = '1024';
            $config['max_height'] = '768';

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload("file")) {
                $status = FALSE;
                $mess = $this->upload->display_errors();
            } else {
                $post_data['thumbnail'] = 'public/upload/slider/' . $this->upload->data()['file_name'];
            }
        }

        if ($status) {
            $status = $this->m_slider->update($post_data, array("id" => $id));
        }

        if (!$status) {
            $r['status'] = FALSE;
            $r['message'] = $mess;
        }

        echo json_encode($r);
    }

    public function delete_slider() {

        $r = array("Result" => "OK");
        $status = TRUE;
        $mess = '';

        $id = intval($this->input->get_post("id", TRUE));

        if ($status) {
            if ($id == 0) {
                $status = FALSE;
                $mess = 'Không lấy được ID Slider';
            }
        }

        if ($status) {
            $rs = $this->m_slider->delete(array("id" => $id));
            if (!$rs) {
                $status = FALSE;
                $mess = 'Lỗi !';
            }
        }

        if (!$status) {
            $r['Result'] = "ERROR";
            $r['Message'] = $mess;
        }

        echo json_encode($r);
    }

    public function toggle_show_hide() {
        $r = array("status" => TRUE);
        $id = intval($this->input->get_post("id", TRUE));

        if ($id == 0) {
            $r['status'] = FALSE;
            $r['message'] = "Không lấy được ID Slider";
        } else {
            $act = $this->m_slider->toggle_show_hide($id);
            if (!$act) {
                $r['status'] = FALSE;
                $r['message'] = "Lỗi cập trong m_slider->toggle_show_hide";
            }
        }

        echo json_encode($r);
    }

    public function order_slider() {

        $r = array("status" => TRUE);
        $status = TRUE;
        $mess = '';

        $id = intval($this->input->get_post("id", TRUE));
        $action = trim($this->input->get_post("action", TRUE));

        if ($status) {
            if ($id == 0) {
                $status = FALSE;
                $mess = 'Không lấy được ID Slider!';
            }
        }

        if ($status) {
            if (!in_array($action, array('top', 'up', 'down', 'bottom'))) {
                $status = FALSE;
                $mess = 'Không nhận được "Yêu cầu" tác động lên Slider!';
            }
        }

        if ($status) {
            $d = $this->m_slider->order_slider($id, $action);
            if (!$d) {
                $status = FALSE;
                $mess = 'Lỗi trong m_slider->order_slider';
            }
        }

        if (!$status) {
            $r['status'] = FALSE;
            $r['message'] = $mess;
        }

        echo json_encode($r);
    }

}
