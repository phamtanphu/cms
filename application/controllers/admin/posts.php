<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of post
 *
 * @author PHAMTANPHU
 */
class posts extends MY_Admin_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        //$this->auth->check_logged();
        $this->load->model("admin/m_posts");
    }

    public function index() {

        $data = array();
        $data['title'] = 'Bài Viết';

        $this->layout->admin_view('posts/index', $data);
    }

    public function create() {

        $data = array();
        $data['title'] = 'Thêm Bài Viết';

        $data['breadcrum'] = array(
            array('link' => '', 'text' => 'Thêm Bài Viết')
        );

        $this->load->model("admin/m_categories");
        $data['categories'] = $this->m_categories->get_data(array("id", "name", "parent"));

        $this->load->helper('child');
        foreach ($data['categories'] as $k => $v) {
            $data['categories'][$k]['name'] = ' <input type="checkbox" value="' . $v['id'] . '" name="category[]" onclick="update_chk_parent(\'categoryAll\', \'category[]\')" id="category-' . $v['id'] . '" class="validate[required]" /> ' . $v['name'];
        }
        $data['categories'] = _order_parent_child($data['categories'], $parent = 0, $key = 'id', $parent_key = 'parent', $show_key = 'name', $sperator = '', $sperator_child = '__');


        $this->layout->admin_view('posts/create', $data);
    }

    public function edit() {

        $post_id = intval($this->input->get_post("post_id", TRUE));

        $data = array();

        $post = $this->m_posts->get_data("*", array("id" => $post_id));
        if (count($post) == 1) {
            $data['post'] = $post[0];
        } else {
            redirect(base_url() . "admin/posts/create");
        }

        $this->load->model("admin/m_categories");
        $data['categories'] = $this->m_categories->get_data(array("id", "name", "parent"));
        $categories_of_post = $this->m_posts->get_catecory_of_post(array("post_id" => $post_id));
        $categories_of_post = array_map(function($value_in_array) {
            return $value_in_array['category_id'];
        }, $categories_of_post);
        $this->load->helper('child');
        foreach ($data['categories'] as $k => $v) {
            $data['categories'][$k]['name'] = ' <input type="checkbox" name="category[]" onclick="update_chk_parent(\'categoryAll\', \'category[]\')" value="' . $v['id'] . '" ' . (in_array($v['id'], $categories_of_post) ? 'checked="checked"' : '') . ' id="category-' . $v['id'] . '" class="validate[required]" /> ' . $v['name'];
        }
        $data['categories'] = _order_parent_child($data['categories'], $parent = 0, $key = 'id', $parent_key = 'parent', $show_key = 'name', $sperator = '', $sperator_child = '__');

        $data['tags'] = $this->m_posts->get_tag_of_post(array("pt.post_id" => $post_id))['tag_name'];

        $data['title'] = 'Sửa Bài Viết';

        $data['breadcrum'] = array(
            array('link' => '', 'text' => 'Sửa Bài Viết')
        );

        $this->layout->admin_view('posts/edit', $data);
    }

    public function get_data() {

        $where = array();
        $like = array();
        $start_index = $this->input->get_post('jtStartIndex');
        $limit = $this->input->get_post('jtPageSize');
        $order_by = $this->input->get_post('jtSorting');

        /* get param to WHERE OR LIKE */
        $arr_param = $this->input->post();
        if (isset($arr_param) && is_array($arr_param) > 0) {
            foreach ($arr_param as $k => $v) {
                $k = trim($k);
                $v = trim($v);
                if (trim($v) != "") {
                    /* Tham số where */
                    $arrw = explode("_w_", $k);
                    if (isset($arrw[1]) && trim($arrw[1]) != "") {
                        $k = (trim($arrw[0]) != "") ? $arrw[0] . '.' . $arrw[1] : $arrw[1];
                        $where[$k] = $v;
                    }

                    /* Tham số like */
                    $arrl = explode("_l_", $k);
                    if (isset($arrl[1]) && trim($arrl[1]) != "") {
                        $k = (trim($arrl[0]) != "") ? $arrl[0] . '.' . $arrl[1] : $arrl[1];
                        //advangce
                        $operator_str = array('[[LIKE]]', '[[NOT LIKE]]', '[[>]]', '[[<]]', '[[=]]', '[[>=]]', '[[<=]]', '[[!=]]', '[[REGEXP]]', '[[REGEXP^$]]', '[[NOT REGEXP]]', '[[= ""]]', '[[!= ""]]', '[[IN]]', '[[NOT IN]]', '[[BETWEEN]]', '[[NOT BETWEEN]]', '[[IS NULL]]', '[[IS NOT NULL]]');
                        $operator = array('LIKE', 'NOT LIKE', '>', '<', '=', '>=', '<=', '!=', 'REGEXP', 'REGEXP^$', 'NOT REGEXP', '= ""', '!= ""', 'IN', 'NOT IN', 'BETWEEN', 'NOT BETWEEN', 'IS NULL', 'IS NOT NULL');
                        if ($v != ($v2 = str_replace($operator_str, $operator, $v))) {
                            $where[$k . ' ' . $v2] = NULL;
                        } else {
                            $like[$k] = $v;
                        }
                    }

                    /* Tham số cho trường datetime */
                    $arrd = explode("_d_", $k);
                    if (isset($arrd[1]) && trim($arrd[1]) != "" && trim($v) != '____-__-__') {
                        $k = (trim($arrd[0]) != "") ? $arrd[0] . '.' . $arrd[1] : $arrd[1];
                        $where["date(" . $k . ")"] = date('Y-m-d', strtotime(trim($v)));
                    }
                }
            }
            //print_r($where);
            //print_r($like);
        }

        $data = $this->m_posts->get_post($where, $like, $order_by, $start_index, $limit);

        $jtable = array();
        $jtable['Result'] = 'OK';
        $jtable['Records'] = $data;
        $jtable['TotalRecordCount'] = $this->m_posts->get_num_post($where, $like);

        //$this->m_posts->disconnect();

        header('Content-Type: application/json');
        print json_encode($jtable);
    }

    public function update() {

        $post_data = array(
            'id' => intval(trim($this->input->get_post('id', TRUE))),
            'title' => trim($this->input->get_post('title', TRUE)),
            'thumbnail' => trim($this->input->get_post('thumbnail', TRUE)),
            'description' => trim($this->input->get_post('description', TRUE)),
            'detail' => trim($this->input->get_post('detail', TRUE)),
            'status' => intval($this->input->get_post('status', TRUE)),
			'edit_user' => $this->session->userdata('logged_user')['id'],
        );

        $categories = $this->input->get_post('category', TRUE);

        if ($post_data['id'] == 0) {
            return redirect(base_url() . "admin/posts/");
        } else if ($post_data['title'] == '' || $categories == '') {
            return redirect(base_url() . "admin/posts/edit?post_id=" . $post_data['id']);
        }

        $tags = trim($this->input->get_post('tag', TRUE));

        $this->load->helper('slug');
        $post_data['slug'] = to_slug($post_data['title']);

        $this->m_posts->update_post($post_data, $categories, $tags);

        redirect(base_url() . "admin/posts");
    }

    public function quick_update() {

        $rs = array("Result" => "OK");
        $status = TRUE;
        $mess = '';

        $id = intval(trim($this->input->get_post('id', TRUE)));
        $post_data = array(
            'title' => trim($this->input->get_post('title', TRUE)),
            'status' => intval($this->input->get_post('status', TRUE)),
			'edit_user' => $this->session->userdata('logged_user')['id'],
        );
        $this->load->helper('slug');
        $post_data['slug'] = to_slug($post_data['title']);

        $tags = trim($this->input->get_post('tag_name', TRUE));


        if ($status) {
            if ($id == 0) {
                $status = FALSE;
                $mess = 'Không lấy được ID bài viết!';
            }
        }

        if ($status) {
            if ($post_data['title'] == '') {
                $status = FALSE;
                $mess = 'Nhập tiêu đề bài viết!';
            }
        }

        if ($status) {
            $q = $this->m_posts->quick_update_post($post_data, $tags, $id);
            if (!$q) {
                $status = FALSE;
                $mess = 'Lỗi trong m_posts->quick_update_post!';
            }
        }

        if (!$status) {
            $rs['Result'] = "ERROR";
            $rs['Message'] = $mess;
        }

        echo json_encode($rs);
    }

    public function insert() {

        $post_data = array(
            'title' => trim($this->input->get_post('title', TRUE)),
            'thumbnail' => trim($this->input->get_post('thumbnail', TRUE)),
            'description' => trim($this->input->get_post('description', TRUE)),
            'detail' => trim($this->input->get_post('detail', TRUE)),
            'status' => intval($this->input->get_post('status', TRUE)),
            'created_user' => $this->session->userdata('logged_user')['id'],
        );

        $categories = $this->input->get_post('category', TRUE);

        if ($post_data['title'] == '' || $categories == '') {
            return redirect(base_url() . "admin/posts/edit?post_id=" . $post_data['id']);
        }

        $tags = trim($this->input->get_post('tag', TRUE));

        $this->load->helper('slug');
        $post_data['slug'] = to_slug($post_data['title']);

        $this->m_posts->insert_post($post_data, $categories, $tags);

        redirect(base_url() . "admin/posts");
    }

    public function delete() {

        $id = intval($this->input->get_post('id', TRUE));

        $jtResult = array();

        $r = $this->m_posts->delete_post($id);

        if ($r) {
            $jtResult['Result'] = 'OK';
        } else {
            $jtResult['Result'] = 'ERROR';
            $jtResult['Message'] = "Lỗi trong trong quá trình xóa bài viết";
        }
        echo json_encode($jtResult);
    }

    function duplicate() {

        $id = intval($this->input->get_post('id', TRUE));

        $r = $this->m_posts->duplicate_post($id);

        $jtResult = array();

        if ($r) {
            $jtResult['Result'] = 'OK';
        } else {
            $jtResult['Result'] = 'ERROR';
            $jtResult['Message'] = "Lỗi trong model!";
        }
        echo json_encode($jtResult);
    }

}
