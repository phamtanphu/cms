<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('_get_child'))
{
	function _get_child($data = array(), $parent = 0, $key = 'id', $parent_key='parent'){
		
		$rs = array();

        foreach ($data as $k => $v) {
            if ($data[$k][$parent_key] == $parent) {
                $tmp_parent = $data[$k][$key];
                array_push($rs, $v);
                unset($data[$k]);
                $rs = array_merge($rs, _get_child($data, $tmp_parent));
            }
        }

        return $rs;
	}
}

if( ! function_exists('_order_parent_child'))
{
	function _order_parent_child($data = array(), $parent = 0,  $key = 'id', $parent_key='parent', $show_key='name', $sperator = '', $sperator_child = '') 
	{
        
        $rs = array();
		
        foreach ($data as $k => $item) {
			
            if (intval($item[$parent_key]) == $parent) {
				
                $tmp_parent = intval($item[$key]);
                
				$item[$show_key] = $sperator .$item[$show_key];
                
				array_push($rs, $item);
				
                unset($data[$k]);
				
				$child = _order_parent_child($data, $tmp_parent, $key, $parent_key, $show_key, $sperator . $sperator_child, $sperator_child);
				
				$rs = array_merge($rs,$child);
				
            }
        }
		
		return $rs;
    }
}