<div id="data"></div>
<script type="text/javascript">
    function get_data() {
        $('#data').jtable({
            title: "Slider",
            paging: true,
            pageSize: 5,
            sorting: false,
            defaultSorting: 'order DESC',
            selecting: true,
            multiselect: false,
            toolbarsearch: true,
            selectingCheckboxes: true,
            actions: {
                listAction: '<?php echo base_url(); ?>admin/slider/get_slider',
                createAction: '#',
                updateAction: '#',
                deleteAction: '<?php echo base_url(); ?>admin/slider/delete_slider'
            },
            toolbar: {
                items: [
                    {
                        'icon': '',
                        'text': '<span class="jtable-toolbar-item-icon ui-icon ui-icon-arrowthickstop-1-n"></span> Lên Đầu',
                        click: function (data) {
                            var selectedRows = $('#data').jtable('selectedRows');
                            var slider = new Array();
                            selectedRows.each(function () {
                                var record = $(this).data('record');
                                slider.push(record.id);
                            });
                            order_slider(slider, 'top');
                        },
                    },
                    {
                        'icon': '',
                        'text': '<span class="jtable-toolbar-item-icon ui-icon ui-icon-arrowthick-1-n"></span> Lên',
                        click: function () {
                            var selectedRows = $('#data').jtable('selectedRows');
                            var slider = new Array();
                            selectedRows.each(function () {
                                var record = $(this).data('record');
                                slider.push(record.id);
                            });
                            order_slider(slider, 'up');
                        },
                    },
                    {
                        'icon': '',
                        'text': '<span class="jtable-toolbar-item-icon ui-icon ui-icon-arrowthick-1-s"></span> Xuống',
                        click: function () {
                            var selectedRows = $('#data').jtable('selectedRows');
                            var slider = new Array();
                            selectedRows.each(function () {
                                var record = $(this).data('record');
                                slider.push(record.id);
                            });
                            order_slider(slider, 'down');
                        },
                    },
                    {
                        'icon': '',
                        'text': '<span class="jtable-toolbar-item-icon ui-icon ui-icon-arrowthickstop-1-s"></span> Xuống Cuối',
                        click: function () {
                            var selectedRows = $('#data').jtable('selectedRows');
                            var slider = new Array();
                            selectedRows.each(function () {
                                var record = $(this).data('record');
                                slider.push(record.id);
                            });
                            order_slider(slider, 'bottom');
                        },
                    },
                    {
                        'icon': '',
                        'text': '<span class="jtable-toolbar-item-icon ui-icon ui-icon-refresh"></span> Refresh',
                        click: function () {
                            $('#data').jtable('reload');
                        },
                        'cssClass': 'right'
                    }
                ]
            },
            fields: {
                id: {
                    key: true,
                    create: false,
                    edit: false,
                    list: false
                },
                thumbnail: {
                    title: 'Hình Ảnh',
                    width: '6%',
                    inputClass: 'validate[required]',
                    display: function (data) {
                        return (data.record.thumbnail != '') ? '<img src="<?php echo base_url() ?>' + data.record.thumbnail + '" />' : '';
                    },
                    inputClass: 'text-center'
                },
                title: {
                    title: 'Tiêu Đề',
                    width: '6%',
                },
                description: {
                    title: 'Mô Tả',
                    width: '6%',
                    type: 'textarea'

                },
                url: {
                    title: 'Url',
                    width: '6%',
                },
                status: {
                    title: 'Trạng Thái',
                    options: [{"Value": "1", "DisplayText": "Hiện"}, {"Value": "0", "DisplayText": "Ẩn"}],
                    width: '2%',
                    listClass: 'text-center',
                    display: function (data) {
                        return '<a class="toggle_show_hide" onclick="toggle_show_hide(' + data.record.id + ')" style="cursor: pointer;" title="Bấm vào để ' + ((data.record.status == 1) ? 'Ẩn' : 'Hiện') + ' Slide">' + ((data.record.status == 1) ? 'Hiện' : 'Ẩn') + '</a>';
                    }
                },
            },
            formCreated: function (event, data) {
                data.form[0].style.minWidth = "700px";
                var $dialogDiv = data.form.closest('.ui-dialog');
                $dialogDiv.position({
                    my: "center",
                    at: "center",
                    of: window
                });

                $('#Edit-thumbnail').attr("type", "file");
                $('#Edit-thumbnail').attr("accept", "image/*");

                // Import image
                var $inputImage = $('#Edit-thumbnail');
                $('#Edit-thumbnail').after('<div id="thumbnailPreview"></div>');

                if (data.formType == 'edit' && data.record.thumbnail.trim() != '') {
                    try {
                        $('#thumbnailPreview').html('<img src="<?php echo base_url(); ?>' + data.record.thumbnail + '" id="image" />');
                        $('#image').cropper({
                            viewMode: 0,
                            aspectRatio: 16 / 9,
                            dragMode: 'move',
                            autoCropArea: 1,
                            restore: false,
                            modal: false,
                            guides: true,
                            highlight: true,
                            cropBoxMovable: false,
                            cropBoxResizable: false,
                            minCropBoxWidth: 700,
                            minCropBoxHeight: 700 * 9 / 16
                        });
                    } catch (e) {
                        alert2('Không load được hình ảnh')
                    }
                    $('#Edit-title').attr('autofocus', true);
                }

                var URL = window.URL || window.webkitURL;
                var blobURL;

                if (URL) {
                    $inputImage.change(function () {
                        var files = this.files;
                        var file;
                        if (files && files.length) {
                            file = files[0];
                            if (/^image\/\w+$/.test(file.type)) {
                                blobURL = URL.createObjectURL(file);
                                $('#thumbnailPreview').html('<img src="' + blobURL + '" id="image" />');
                                $('#image').cropper({
                                    viewMode: 0,
                                    aspectRatio: 16 / 9,
                                    dragMode: 'move',
                                    autoCropArea: 1,
                                    restore: false,
                                    modal: false,
                                    guides: true,
                                    highlight: true,
                                    cropBoxMovable: false,
                                    cropBoxResizable: false,
                                    minCropBoxWidth: 700,
                                    minCropBoxHeight: 700 * 9 / 16
                                });
                            } else {
                                alert2('Chọn một hình ảnh');
                            }
                        }
                    });
                }
            },
            formSubmitting: function (event, data) {
                // Upload cropped image to server
                if ($('#thumbnailPreview').html() != '') {
                    try {
                        var canvas = $('#image').cropper('getCroppedCanvas', {'width': 700, 'height': 700 * 9 / 16}).toBlob(function (blob) {

                            var formAction = '<?php echo base_url(); ?>admin/slider/insert_slider';
                            var formData = new FormData();
                            formData.append('file', blob, "blob.png");

                            formData.append('title', $('#Edit-title').val().trim());
                            formData.append('description', $('#Edit-description').val().trim());
                            formData.append('url', $('#Edit-url').val().trim());
                            formData.append('status', $('#Edit-status option:selected').val());

                            if (data.formType == 'edit') {
                                formAction = '<?php echo base_url(); ?>admin/slider/update_slider';
                                formData.append('id', $('#Edit-id').val().trim());
                            }

                            $.ajax(formAction, {
                                method: "POST",
                                data: formData,
                                processData: false,
                                contentType: false,
                                success: function (data) {
                                    data = JSON.parse(data);
                                    if (data.status) {
                                        $('#data').jtable('reload');
                                        $('.ui-dialog-buttonpane').find('button:eq(0)').trigger('click');
                                    } else {
                                        if (data.message == 'Vui lòng đăng nhập!') {
                                            load_login_form();
                                            return;
                                        }
                                        alert2(data.message);
                                    }
                                },
                                error: function () {
                                    alert2("Lỗi! :(");
                                }
                            });

                        });
                    } catch (e) {
                        alert2('Lỗi :( không tải được hình ảnh!');
                    }
                } else {
                    alert2('Chọn một hình ảnh');
                }
                return false;

            },
            formClosed: function (event, data) {

            },
            rowUpdated: function () {

            },
            recordsLoaded: function () {
                $('.toggle_show_hide').tooltip();
            },
            rowInserted: function (event, data) {
                $(data.row).dblclick(function () {
                    $(this).find('.jtable-edit-command-button').trigger('click');
                    $(this).trigger('click');
                });
            }
        });
        get_data_filter();
    }

    function get_data_filter() {
        $('#data').jtable('load', {
            _l_title: get_value_element("jtable-toolbarsearch-title"),
            _l_description: get_value_element("jtable-toolbarsearch-description"),
            _l_url: get_value_element("jtable-toolbarsearch-url"),
            _w_status: get_value_element("jtable-toolbarsearch-status"),
        });
    }

    function toggle_show_hide(id) {
        $.ajax('<?php echo base_url() ?>admin/slider/toggle_show_hide', {
            method: "POST",
            data: {
                id: id,
            },
            success: function (data) {
                data = JSON.parse(data);
                if (data.status) {
                    $('#data').jtable('reload');
                } else {
                    if (data.message == 'Vui lòng đăng nhập!') {
                        load_login_form();
                        return;
                    }
                    alert2(data.message)
                }
            },
            error: function () {
                alert2("Lỗi! :(");
            }
        });
    }

    function order_slider(id, action) {
        if (id.length == 1) {
            $.ajax('<?php echo base_url() ?>admin/slider/order_slider', {
                method: "POST",
                data: {
                    id: id[0],
                    action: action
                },
                success: function (data) {
                    data = JSON.parse(data);
                    if (data.status) {
                        $('#data').jtable('reload');
                    } else {
                        if (data.message == 'Vui lòng đăng nhập!') {
                            load_login_form();
                            return;
                        }
                        alert2(data.message)
                    }
                },
                error: function () {
                    alert2("Lỗi! :(");
                }
            });
        } else {
            return (id.length == 0) ? alert2("Chọn Slider để di chuyển!") : alert2("Chỉ chọn được 1 Slide để duy chuyển!");
        }
    }

    get_data();
    $('#jtable-toolbarsearch-status').prop("placeholder", "1:Hiện, 0: Ẩn");
    $('#jtable-toolbarsearch-thumbnail').remove();

</script>
