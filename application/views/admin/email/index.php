<style type="text/css">
    button.jtable-edit-command-button{/* display:none!important; */}
</style>
<div id="data"></div>
<script type="text/javascript">
    function get_data() {
        $('#data').jtable({
            title: "Cấu Hình Email",
            paging: true,
            pageSize: 20,
            sorting: true,
            defaultSorting: 'created_date DESC',
            selecting: true,
            multiselect: false,
            toolbarsearch: true,
            selectingCheckboxes: true,
            actions: {
                listAction: '<?php echo base_url(); ?>admin/email/get_email',
                //createAction: '<?php echo base_url(); ?>admin/email/insert_email',
                updateAction: '<?php echo base_url(); ?>admin/email/update_email',
                //deleteAction: '<?php echo base_url(); ?>admin/email/delete_email'
            },
            toolbar: {
                items: [
                    {
                        'icon': '',
                        'text': '<span class="jtable-toolbar-item-icon ui-icon ui-icon-refresh"></span> Refresh',
                        click: function () {
                            $('#data').jtable('reload');
                        },
                        'cssClass': 'right'
                    }
                ]
            },
            fields: {
                id: {
                    title: 'ID',
                    key: true,
                    list: false
                },
                name: {
                    title: 'Tùy Chọn',
                    width: '20%',
                    inputClass: 'validate[required]',
                },
                value: {
                    title: 'Giá Trị',
                    width: '20%',
                },
                status: {
                    title: 'Trạng Thái',
                    options: [{"Value": "0", "DisplayText": "Không Kích Hoạt"}, {"Value": "1", "DisplayText": "Kích Hoạt"}],
                    width: '10%',
                    listClass: 'text-center'
                },
            },
            formCreated: function (event, data) {

            },
            formSubmitting: function (event, data) {
                return data.form.validationEngine('validate');
            },
            formClosed: function (event, data) {
                data.form.validationEngine('hide');
                data.form.validationEngine('detach');
            },
            rowUpdated: function () {
                $('#data').jtable('reload');
            },
            rowInserted: function (event, data) {
                $(data.row).dblclick(function () {
                    $(this).trigger('click');
                    $(this).find('.jtable-edit-command-button').trigger('click');
                });
            }
        });
        $('#data').jtable('reload');
    }

    function get_data_filter() {
        $('#data').jtable('load', {
            _l_name: get_value_element("jtable-toolbarsearch-name"),
            _l_value: get_value_element("jtable-toolbarsearch-value"),
            _w_status: get_value_element("jtable-toolbarsearch-status"),
        });
    }

    get_data();
</script>