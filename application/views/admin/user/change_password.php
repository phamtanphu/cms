﻿<style type="text/css">
    #chage_password_form{padding:20px 15px; border:3px solid #ddd; border-radius:2px;}
</style>
<div class="row">
    <div class="col col-4"></div>
    <div class="col col-4">
        <form action="<?php echo base_url() ?>admin/user/change_password_action" method="POST" id="chage_password_form" class="" onsubmit="chage_password_action();
                        return false;">

            <label>Mật Khẩu Cũ</label>
            <input type="password" name="password" id="password" class="form-control input-sm validate[required]" maxlength="255" />

            <label>Mật Khẩu Mới</label>
            <input type="password" name="new_password" id="new_password" class="form-control input-sm validate[required]" maxlength="255" />

            <label>Nhập Lại Mật Khẩu Mới</label>
            <input type="password" name="confirm_new_password" id="confirm_new_password" class="form-control input-sm validate[required,equals[new_password]]" maxlength="255" />

            <br /><br />
            <input type="submit" class="btn btn-primary btn-sm" value="Thực hiện" />

        </form>
    </div>
    <div class="col col-4"></div>
</div>

<script type="text/javascript">
    function chage_password_action() {

        if (!$("#chage_password_form").validationEngine('validate')) {
            return false;
        }

        $.ajax({
            url: '<?php echo base_url(); ?>admin/user/change_password_action',
            type: 'POST',
            data: {
                password: md5($('#password').val()),
                new_password: md5($('#new_password').val())
            },
            datatype: 'html',
            async: false,
            success: function (data) {
                try {
                    var data = JSON.parse(data);
                    if (data.status) {
                        $('#password, #new_password, #confirm_new_password').val('');
                        alert2("Thay đổi mật khẩu thành công!");
                    } else {
                        if (typeof (data.message) != 'undefine' && data.message == 'Vui lòng đăng nhập!') {
                            load_login_form();
                            return;
                        }
                        alert2(data.message);
                    }
                } catch (err) {
                    alert2("Lỗi: " + err.message);
                }
            }
        });

    }
</script>