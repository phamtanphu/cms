<div id="data"></div>
<script type="text/javascript">
    function get_data() {
        $('#data').jtable({
            title: "Người Dùng",
            paging: true,
            pageSize: 20,
            sorting: true,
            defaultSorting: 'created_date DESC',
            selecting: true,
            multiselect: true,
            toolbarsearch: true,
            selectingCheckboxes: true,
            actions: {
                listAction: '<?php echo base_url(); ?>admin/user/get_user',
                createAction: function (postData) {

                    var data = postData.split('&');
                    var pass = decodeURIComponent(data[2].replace("password=", ""));
                    data[2] = 'password=' + md5(pass);
                    data.splice(3, 1)
                    postData = data.join('&');

                    return $.Deferred(function ($dfd) {
                        $.ajax({
                            url: '<?php echo base_url(); ?>admin/user/insert_user',
                            type: 'POST',
                            dataType: 'json',
                            data: postData,
                            success: function (data) {
                                $dfd.resolve(data);
                            },
                            error: function () {
                                $dfd.reject();
                            }
                        });
                    });
                },
                updateAction: '<?php echo base_url(); ?>admin/user/update_user',
                //deleteAction: '<?php echo base_url(); ?>admin/user/delete_user'
            },
            toolbar: {
                items: [
                    {
                        'icon': '',
                        'text': '<span class="jtable-toolbar-item-icon ui-icon ui-icon-unlocked"></span> Reset Password',
                        click: function () {

                            var selectedRows = $('#data').jtable('selectedRows');
                            var user = new Array();
                            selectedRows.each(function () {
                                var record = $(this).data('record');
                                /* var row = {
                                 'id': record.id,
                                 'username': record.username,
                                 }; */
                                user.push(record.username);
                            });
                            if (user.length > 0) {
                                reset_password(user);
                            } else {
                                alert2('Chọn người dùng cần cài lại mật khẩu!');
                            }
                        }
                    },
                    {
                        'icon': '',
                        'text': '<span class="jtable-toolbar-item-icon ui-icon ui-icon-refresh"></span> Refresh',
                        click: function () {
                            get_data_filter();
                        },
                        'cssClass': 'right'
                    }
                ]
            },
            fields: {
                id: {
                    key: true,
                    create: false,
                    edit: false,
                    list: false
                },
                realname: {
                    title: 'Họ và Tên',
                    width: '6%',
                    inputClass: 'validate[required]',
                },
                username: {
                    title: 'Tên Đăng Nhập',
                    width: '6%',
                    inputClass: 'validate[required,custom[onlyLetterNumber]]',
                },
                password: {
                    title: 'Mật Khẩu',
                    width: '6%',
                    inputClass: 'validate[required]',
                    type: 'password',
                    list: false,
                    edit: false
                },
                confirm_password: {
                    title: 'Nhập Lại Mật Khẩu',
                    width: '6%',
                    inputClass: 'validate[required,equals[Edit-password]]',
                    type: 'password',
                    list: false,
                    edit: false
                },
                email: {
                    title: 'Email',
                    width: '6%',
                    inputClass: 'validate[required,custom[email]]',
                },
                phone: {
                    title: 'Điện Thoại',
                    width: '6%',
                    inputClass: 'validate[optional,maxSize[11],custom[onlyNumberSp]]',
                },
                role_id: {
                    title: 'Vai Trò',
                    options: [{"Value": "2", "DisplayText": "Người Dùng"}, {"Value": "1", "DisplayText": "Quản Trị"}],
                    width: '6%',
                    listClass: 'text-center'
                },
                status: {
                    title: 'Trạng Thái',
                    options: [{"Value": "1", "DisplayText": "Còn Hoạt Động"}, {"Value": "0", "DisplayText": "Không Hoạt Động"}],
                    width: '2%',
                    listClass: 'text-center'
                },
            },
            formCreated: function (event, data) {
                data.form[0].style.minWidth = "270px";
                data.form.validationEngine();
            },
            formSubmitting: function (event, data) {
                return data.form.validationEngine('validate');
            },
            formClosed: function (event, data) {
                data.form.validationEngine('hide');
                data.form.validationEngine('detach');
            },
            rowUpdated: function () {
                $('#data').jtable('reload');
            },
            rowInserted: function (event, data) {
                $(data.row).dblclick(function () {
                    $(this).find('.jtable-edit-command-button').trigger('click');
                    $(this).trigger('click');
                });
            }
        });
        $('#data').jtable('reload');

    }

    function get_data_filter() {
        $('#data').jtable('load', {
            _l_realname: get_value_element("jtable-toolbarsearch-realname"),
            _l_username: get_value_element("jtable-toolbarsearch-username"),
            _l_email: get_value_element("jtable-toolbarsearch-email"),
            _l_phone: get_value_element("jtable-toolbarsearch-phone"),
            _w_role_id: get_value_element("jtable-toolbarsearch-role_id_cb"),
            _w_status: get_value_element("jtable-toolbarsearch-status_cb"),
        });
    }

    function reset_password(users) {

        var htm = '';
        htm += '<form id="reset_password_form" class="small">';
        htm += '<label>Tên Đăng Nhập</label>';
        htm += '<input type="text" name="reset_users" id="reset_users" value="' + users.join(",") + '" class="validate[required]" readonly style="width:100%;"/>';
        htm += '<label>Mật Khẩu Mới</label>';
        htm += '<input type="password" name="new_password" id="new_password" value="" class="validate[required]" style="width:100%;" />';
        htm += '<label>Nhập Lại Mật Khẩu Mới</label>';
        htm += '<input type="password" name="confirm__new_password" value="" class="validate[required,equals[new_password]]" style="width:100%;" />';
        htm += '</form>';

        $('#form_dialog').html(htm);

        $('#reset_users').tagit();
        $('li.tagit-new').remove();

        $('#form_dialog').dialog({
            modal: true,
            title: 'Khôi phục mật khẩu',
            zIndex: 10000,
            autoOpen: true,
            width: '250',
            dialogClass: 'no-overflow',
            resizable: true,
            close: function () {
                $('#form_dialog').empty();
                $('#form_dialog').dialog("close");
                $('#reset_password_form').validationEngine('hide');
                $('#reset_password_form').validationEngine('detach');
            },
            buttons: {
                'Thực hiện': function () {

                    $('#reset_password_form').validationEngine();

                    if ($('#reset_password_form').validationEngine('validate')) {

                        if ($('#reset_users').val() == '') {
                            return alert2("Vui lòng chọn lại người dùng muốn khôi phục mật khẩu!");
                        }

                        $.ajax({
                            url: "<?php echo base_url(); ?>admin/user/reset_password",
                            type: "POST",
                            data: {
                                users: $('#reset_users').val(),
                                password: md5($('#new_password').val()),
                            },
                            async: false,
                            success: function (data) {
                                try {
                                    var data = JSON.parse(data);
                                    if (typeof (data.message) != 'undefine' && data.message == 'Vui lòng đăng nhập!') {
                                        load_login_form();
                                        return;
                                    }
                                    alert2(data.message);
                                    if (data.status) {
                                        $('#data').jtable('reload');
                                        $('#form_dialog').empty();
                                        $('#form_dialog').dialog("close");
                                        $('#reset_password_form').validationEngine('hide');
                                        $('#reset_password_form').validationEngine('detach');
                                    }
                                } catch (err) {
                                    $('#form_dialog').empty();
                                    $('#form_dialog').dialog("close");
                                    $('#data').jtable('reload');
                                }
                            }
                        });
                    }
                },
                'Đóng': function () {
                    $('#form_dialog').empty();
                    $('#form_dialog').dialog("close");
                    $('#reset_password_form').validationEngine('hide');
                    $('#reset_password_form').validationEngine('detach');
                    $('#data').jtable('reload');
                }
            }
        });
    }


    get_data();

    $(document).ready(function () {
        $('#jtable-toolbarsearch-role_id').after('<select name="status" id="jtable-toolbarsearch-role_id_cb" class="jtable-toolbarsearch"><option value="">Chọn</option><option value="1">Quản Trị</option><option value="2">Người Dùng</option></select>');
        $('#jtable-toolbarsearch-role_id').remove();

        $('#jtable-toolbarsearch-status').after('<select name="status" id="jtable-toolbarsearch-status_cb" class="jtable-toolbarsearch"><option value="">Chọn</option><option value="1">Còn Hoạt Động</option><option value="0">Không Hoạt Động</option></select>');
        $('#jtable-toolbarsearch-status').remove();
    })
</script>
