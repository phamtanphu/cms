<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>.:: <?php if (isset($title)) echo $title; ?> - Admin Panel ::.</title>

        <!-- Base require -->
        <script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>

        <!-- Include one of jquery-ui styles. -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>public/plugin/jquery-ui/jquery-ui.min.css"  type="text/css" />
        <!-- Include jquery-ui script file. -->
        <script src="<?php echo base_url(); ?>public/plugin/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

        <?php if (!in_array($view_name, array('slider/index'))): ?>
            <!-- Include one of validationengine styles. -->
            <link rel="stylesheet" href="<?php echo base_url(); ?>public/plugin/validationengine/css/validationEngine.jquery.css"  type="text/css" />
            <!-- Include validationengine script file. -->
            <script src="<?php echo base_url(); ?>public/plugin/validationengine/js/jquery.validationEngine.js" type="text/javascript"></script>
            <script src="<?php echo base_url(); ?>public/plugin/validationengine/js/languages/jquery.validationEngine-vi.js" type="text/javascript"></script>
        <?php endif ?>

        <!-- Include md5 script file. -->
        <script src="<?php echo base_url(); ?>public/plugin/JavaScript-MD5/js/md5.min.js" type="text/javascript"></script>

        <?php if (in_array($view_name, array('posts/index', 'categories/index', 'user/index', 'slider/index', 'email/index'))): ?>
            <!-- Include one of jTable styles. -->
            <link rel="stylesheet" href="<?php echo base_url(); ?>public/plugin/jtable/themes/lightcolor/gray/jtable.min.css"  type="text/css" />
            <!-- Include jTable script file. -->
            <script src="<?php echo base_url(); ?>public/plugin/jtable/jquery.jtable.js" type="text/javascript"></script>
            <script src="<?php echo base_url(); ?>public/plugin/jtable/localization/jquery.jtable.vi.js" type="text/javascript"></script>
            <script src="<?php echo base_url(); ?>public/plugin/jtable/extensions/jquery.jtable.toolbarsearch.js" type="text/javascript"></script>
            <script src="<?php echo base_url(); ?>public/plugin/jtable/extensions/jquery.jtable.loadloginform.js" type="text/javascript"></script>
        <?php endif ?>

        <?php if (in_array($view_name, array('menus/index'))): ?>
            <link rel="stylesheet" href="<?php echo base_url(); ?>public/plugin/nestable/jquery.nestable.css"  type="text/css" />
            <!-- Include nestable script file. -->
            <script src="<?php echo base_url(); ?>public/plugin/nestable/jquery.nestable.js" type="text/javascript"></script>
        <?php endif ?>

        <?php if (in_array($view_name, array('posts/create', 'posts/edit'))): ?>
            <!-- Include tinymce script file. -->
            <script src="<?php echo base_url(); ?>public/plugin/tinymce/tinymce.min.js" type="text/javascript"></script>
        <?php endif ?>

        <?php if (in_array($view_name, array('posts/index', 'posts/create', 'posts/edit', 'user/index'))): ?>
            <!-- Include one of tag-it styles. -->
            <link rel="stylesheet" href="<?php echo base_url(); ?>public/plugin/tag-it/css/jquery.tagit.css"  type="text/css" />
            <!-- Include tag-it script file. -->
            <script src="<?php echo base_url(); ?>public/plugin/tag-it/js/tag-it.min.js" type="text/javascript"></script>
        <?php endif ?>

        <?php if (in_array($view_name, array('posts/create', 'posts/edit', 'slider/index'))): ?>
            <script src="<?php echo base_url(); ?>public/plugin/canvas-toBlob/canvas-toBlob.js" type="text/javascript"></script>
        <?php endif ?>

        <?php if (in_array($view_name, array('posts/create', 'posts/edit', 'slider/index'))): ?>
            <link rel="stylesheet" href="<?php echo base_url(); ?>public/plugin/cropper/dist/cropper.min.css"  type="text/css" />
            <script src="<?php echo base_url(); ?>public/plugin/cropper/dist/cropper.min.js" type="text/javascript"></script>
        <?php endif ?>

        <link  rel="stylesheet" href="<?php echo base_url(); ?>public/css/global.css" />
        <link  rel="stylesheet" href="<?php echo base_url(); ?>public/css/style.css" />

        <script type="text/javascript">
            function alert2(mess) {
                $('#alert_dialog').empty();
                $('#alert_dialog').append(mess);
                $('#alert_dialog').dialog({
                    modal: true,
                    title: 'Thông Báo!',
                    dialogClass: "dialogzindex",
                    autoOpen: true,
                    width: 'auto',
                    resizable: false,
                    buttons: {
                        'Đóng': function () {
                            $(this).dialog("close");
                            $('#alert_dialog').empty();
                        }
                    }
                });
            }

            function load_login_form() {

                $('#form_login_dialog').empty();

                var htm = '';
                htm += '<form action="<?php echo base_url() ?>admin/check_login" method="POST" id="login_form" class="" onsubmit="return false;">';
                htm += '<label>Tên đăng nhập</label>';
                htm += '<input value="" type="text" name="username_dialog" id="username_dialog" class="form-control input-sm validate[required]" maxlength="255" autocomplete="off" autofocus />';

                htm += '<label>Mật khẩu</label>';
                htm += '<input value="" type="password" name="password_dialog" id="password_dialog" class="form-control input-sm validate[required]" maxlength="255" />';

                htm += '<input type="hidden" z-index = "-1" />';
                htm += '</form>';

                $('#form_login_dialog').append(htm);
                $('#form_login_dialog').dialog({
                    modal: true,
                    title: 'Đăng Nhập',
                    dialogClass: "dialogzindex",
                    autoOpen: true,
                    width: '250px',
                    resizable: false,
                    buttons: {
                        'Đăng nhập': function () {

                            if (!$("#login_form").validationEngine('validate')) {
                                return false;
                            }

                            $.ajax({
                                url: '<?php echo base_url(); ?>admin/check_login',
                                type: 'POST',
                                data: {
                                    username: $('#username_dialog').val().trim(),
                                    password: md5($('#password_dialog').val()),
                                },
                                datatype: 'json',
                                async: false,
                                success: function (data) {
                                    try {
                                        var data = JSON.parse(data);
                                        if (data.status) {
                                            $('#form_login_dialog').dialog("close");
                                            $('#form_login_dialog').empty();
                                        } else {
                                            alert2(data.message);
                                        }
                                    } catch (err) {
                                        alert2("Lỗi: " + err.message);
                                    }
                                }
                            });
                        },
                        'Hủy': function () {
                            $(this).dialog("close");
                            $('#form_login_dialog').empty();
                        }
                    },
                });
            }

            function get_value_element(id_el) {
                /* for input */
                return $('#' + id_el).val().trim();
            }

            function get_value_element2(id_el) {
                /* for input */
                return $('#' + id_el + ' option:selected').val().trim();
            }

            function toggle_all_checkbox(source, chk_child_name) {
                var checkboxes = document.getElementsByName(chk_child_name);
                for (var i = 0; i < checkboxes.length; i++) {
                    checkboxes[i].checked = source.checked;
                }
            }

            function update_chk_parent(id_parent_name, child_name) {
                var chk_parent = document.getElementById(id_parent_name);
                var chk_childs = document.getElementsByName(child_name);
                var count = 0; // Bien dem nhung checkbox da duoc checked
                for (var i = 0; i < chk_childs.length; i++) {
                    if (chk_childs[i].checked === true) {
                        count++;
                    }
                }

                chk_parent.checked = (count === chk_childs.length) ? true : false;

                chk_parent.indeterminate = (count > 0) && (count < chk_childs.length) ? true : false;
            }

            var delay = (function () {
                var timer = 0;
                return function (callback, ms) {
                    clearTimeout(timer);
                    timer = setTimeout(callback, ms);
                };
            })();
        </script>
    </head>

    <body>
        <div id="wraper">
            <div id="header">
                <div class="container">
                    <div class="row">
                        <div class="col col-4">
                            <div class="admin-info"><a href="<?php echo base_url();?>admin">Admin PTP</a></div>
                        </div>
                        <div class="col col-8">
                            <div class="admin-user right">Xin chào: <span class="logged_username"><?php if (isset($user['username'])) echo $user['username']; ?></span> | <a href="<?php echo base_url(); ?>admin/logout" class="logout">Thoát</a></div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="main">
                <div class="row">
                    <div class="col col-2" id="sidebar">
                        <?php $this->load->view("admin/nav"); ?>
                    </div>
                    <div class="col col-10" id="main-content">
                        <?php if (isset($breadcrum) && count((array) $breadcrum) > 0): ?>
                            <ul class="breadcrums">
                                <?php
                                foreach ($breadcrum as $item) {
                                    if (isset($item['link']) && $item['link'] != '' && isset($item['text']) && $item['text'] != '') {
                                        echo '<li><a href="' . base_url() . $item['link'] . '">' . $item['text'] . '</a></li>';
                                    } else if (isset($item['text']) && $item['text'] != '') {
                                        echo '<li>' . $item['text'] . '</li>';
                                    }
                                }
                                ?>
                            </ul>
                        <?php endif ?>

                        <?php if (isset($view)) echo $view; ?>                        
                    </div>
                </div>
            </div>

            <div id="footer">
                <div id="alert_dialog"></div>
                <div id="confirm_dialog"></div>
                <div id="form_dialog"></div>
                <div id="form_login_dialog"></div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {

                $('#admin-menu a[href="' + window.location.href + '"]').addClass("active");

                $('input.jtable-toolbarsearch').keyup(function (e) {
                    var keycode = e.keyCode || e.which;
                    if (keycode === 13) {
                        get_data_filter();
                    }
                });

                $('select.jtable-toolbarsearch').change(function () {
                    delay(function () {
                        get_data_filter();
                    }, 1);
                });

                $('body').on('keypress', 'form#jtable-edit-form input', function (e) {
                    var code = (e.keyCode ? e.keyCode : e.which);
                    if (code == 13) {
                        $('button#EditDialogSaveButton').trigger('click');
                    }
                });

                $('body').on('keypress', 'form#jtable-create-form input', function (e) {
                    var code = (e.keyCode ? e.keyCode : e.which);
                    if (code == 13) {
                        $('button#AddRecordDialogSaveButton').trigger('click');
                    }
                });

                $('body').on('keypress', '#form_dialog input', function (e) {
                    var code = (e.keyCode ? e.keyCode : e.which);
                    if (code == 13) {
                        console.log(2);
                        $(this).parents().siblings('.ui-dialog-buttonpane').find('button:eq(0)').trigger('click');
                    }
                });

                $('body').on('keypress', '#form_login_dialog input', function (e) {
                    var code = (e.keyCode ? e.keyCode : e.which);
                    if (code == 13) {
                        console.log(2);
                        $(this).parents().siblings('.ui-dialog-buttonpane').find('button:eq(0)').trigger('click');
                    }
                });
            })
        </script>
    </body>

</html>