<div id="tabs-">
    <form action="/admin/menus/update_root_menu" class="clearfix create_root_menu" onsubmit="update_root_menu(<?php echo $menu_root[0]['id']; ?>);
                return false" id="create_root_menu_<?php echo $menu_root[0]['id']; ?>">
        <input type="hidden" name="id" value="<?php echo $menu_root[0]['id']; ?>" />
        <input type="text" class="form-control input-xs validate[required]"  name="root_title_<?php echo $menu_root[0]['id']; ?>" id="root_title_<?php echo $menu_root[0]['id']; ?>" value="<?php echo $menu_root[0]['title']; ?>" placeholder="Tên menu" maxlength="255"/>
        <input type="button" value="Lưu" class="btn btn-xs" onclick="update_root_menu(<?php echo $menu_root[0]['id']; ?>)"/>
        <a class="right" href="javascript:delete_root_menu(<?php echo $menu_root[0]['id']; ?>)">Xóa</a>
    </form>
    <div class="menu-content">
        <div class="row">
            <div class="col col-6">
                <a href="javascript:create_menu(<?php echo $menu_root[0]['id']; ?>)" class="left">Thêm</a>
                <input type="button" id="btn-save-order-<?php echo $menu_root[0]['id']; ?>" onclick="save_order(<?php echo $menu_root[0]['id']; ?>)" value="Lưu" class="btn btn-sm btn-save-order btn-primary right"/>
            </div>
        </div>

        <div class="dd" id="nestable-<?php echo $menu_root[0]['id']; ?>">
        </div>
        <div class="clearfix"></div>

        <script type="text/javascript">

            load_menu(<?php echo $menu_root[0]['id']; ?>);

            function update_root_menu(id) {
                if (!$("#create_root_menu_" + id).validationEngine('validate')) {
                    return false;
                }
                process_menu();
                $.ajax({
                    url: '<?php echo base_url(); ?>admin/menus/update_root_menu',
                    type: 'POST',
                    async: false,
                    data: {
                        id: id,
                        title: get_value_element('root_title_' + id),
                    },
                    datatype: 'html',
                    success: function (data) {
                        try {
                            data = JSON.parse(data);
                            if (!data.status) {
                                if (data.message == 'Vui lòng đăng nhập!') {
                                    load_login_form();
                                    return;
                                }
                                alert2(data.message);
                            } else {
                                window.location = "<?php echo base_url() ?>admin/menus/";
                            }
                        } catch (err) {
                            alert2("Dữ liệu trả về không đúng định dạng!")
                        }
                        delay(function () {
                            process_menu_complete();
                        }, 300);
                    }
                });
            }

            function delete_root_menu(id) {
                $('#confirm_dialog').empty();
                $('#confirm_dialog').append("Menu này sẽ bị xóa toàn bộ bạn có chắc không?");
                $('#confirm_dialog').dialog({
                    modal: true,
                    title: 'Xác nhận',
                    dialogClass: "dialogzindex",
                    autoOpen: true,
                    width: 'auto',
                    resizable: false,
                    buttons: {
                        'Hủy': function () {
                            $(this).dialog("close");
                            $('#confirm_dialog').empty();
                        },
                        'Có': function () {
                            process_menu();
                            $.ajax({
                                url: '<?php echo base_url(); ?>admin/menus/delete_root_menu',
                                type: 'POST',
                                async: false,
                                data: {
                                    id: id,
                                },
                                datatype: 'html',
                                success: function (data) {
                                    try {
                                        data = JSON.parse(data);
                                        if (!data.status) {
                                            if (data.message == 'Vui lòng đăng nhập!') {
                                                load_login_form();
                                                return;
                                            }
                                            alert2(data.message);
                                        } else {
                                            window.location = "<?php echo base_url() ?>admin/menus/";
                                        }
                                    } catch (err) {
                                        alert2("Dữ liệu trả về không đúng định dạng!")
                                    }
                                }
                            });
                            process_menu_complete();
                            $(this).dialog("close");
                            $('#confirm_dialog').empty();
                        }
                    },
                });
            }

            function load_menu(root) {

                var data = new Array();

                process_menu();
                $.ajax({
                    url: '<?php echo base_url(); ?>admin/menus/load_menu_data?root=' + root,
                    type: 'POST',
                    async: false,
                    data: {
                        root: root,
                    },
                    datatype: 'html',
                    success: function (result) {
                        try {
                            data = JSON.parse(result);
                            if (data.status != 'underfine' && data.status) {
                                if (data.message == 'Vui lòng đăng nhập!') {
                                    load_login_form();
                                    return;
                                }
                                alert2(data.message);
                            }
                        } catch (err) {
                            alert2("Dữ liệu trả về không đúng định dạng!")
                        }
                    }
                });

                if (data.length > 0) {
                    $('#nestable-' + root).html(show_menu(data, root));
                    $('#nestable-' + root).nestable();
                    $(".dd3-content").click(function () {
                        $(this).parent().toggleClass("open", 400).fadeIn();
                    });
                    $('.advance_notice').click(function () {
                        $(this).siblings('.advance_custom').toggleClass('show', 100);
                        var notice = $(this).html();
                        if (notice == 'Hiện nâng cao') {
                            $(this).html('Ẩn nâng cao');
                        }
                        if (notice == 'Ẩn nâng cao') {
                            $(this).html('Hiện nâng cao');
                        }
                    });
                    $('#btn-save-order-<?php echo $menu_root[0]['id']; ?>').show();
                } else {
                    $('#nestable-<?php echo $menu_root[0]['id']; ?>').html('');
                    $('#btn-save-order-<?php echo $menu_root[0]['id']; ?>').hide();

                }

                delay(function () {
                    process_menu_complete();
                }, 300);

            }

            function show_menu(data, parent) {

                var htm = '';
                var i = 0;

                //for(i; i < data.length; i++){
                while (i < data.length) {
                    if (data[i]['parent'] == parent) {
                        var tmp_parent = data[i]['id'];
                        htm += '<li class="dd-item dd3-item" data-id="' + data[i]['id'] + '">';
                        htm += '<div class="dd-handle dd3-handle"> </div>';
                        htm += '<div class="dd3-content" id="menu_' + data[i]['id'] + '">' + data[i]['title'] + '</div>';
                        htm += '<form action="" id="update_menu_' + data[i]['id'] + '" method="post" class="menu-content-edit">';
                        htm += '<input type="hidden" id="id_' + data[i]['id'] + '" name="id_' + data[i]['id'] + '" value="' + data[i]['id'] + '" />';
                        htm += '<label>Tiêu Đề*</label>';
                        htm += '<input type="text" id="title_' + data[i]['id'] + '" name="title_' + data[i]['id'] + '" value="' + data[i]['title'] + '" class="form-control input-sm validate[required]" />';
                        htm += '<label>URL*</label>';
                        htm += '<input type="text" id="url_' + data[i]['id'] + '" name="url_' + data[i]['id'] + '" value="' + data[i]['url'] + '" class="form-control input-sm" />';
                        htm += '<label>Mở trong Tab*</label>';
                        htm += '<select name="" id="target_' + data[i]['id'] + '" name="target_' + data[i]['id'] + '" class="form-control input-sm">';
                        htm += '<option value="1" ' + ((data[i]['target'] == 1) ? 'selected' : '') + '>Hiện tại</option>';
                        htm += '<option value="2" ' + ((data[i]['target'] == 2) ? 'selected' : '') + '>Mới</option>';
                        htm += '</select>';
                        htm += '<div class="advance_custom">';
                        htm += '<label>Icon</label>';
                        htm += '<input type="text" id="icon_' + data[i]['id'] + '" name="icon_' + data[i]['id'] + '" value="' + data[i]['icon'] + '" class="form-control input-sm" />';
                        htm += '<label>URL Ảnh</label>';
                        htm += '<input type="text" id="thumbnail_' + data[i]['id'] + '" name="thumbnail_' + data[i]['id'] + '" value="' + data[i]['thumbnail'] + '" class="form-control input-sm" />';
                        htm += '<label>CSS</label>';
                        htm += '<textarea id="css_' + data[i]['id'] + '" name="css_' + data[i]['id'] + '" class="form-control" rows="3">' + data[i]['css'] + '</textarea>';
                        htm += '</div>';
                        htm += '<div class="clearfix"></div>';
                        htm += '<span class="italic small advance_notice" title="Chỉ áp dụng riêng cho từng loại theme">Hiện nâng cao</span>';
                        htm += '<div class="clearfix"></div><br />';
                        htm += '<input type="button" class="btn btn-primary btn-xs right" id="btn_menuluu_' + data[i]['id'] + '" value="Lưu" onclick="update_menu(' + data[i]['id'] + ')" />';
                        htm += '<input type="button" class="btn btn-xs right" value="Xóa" onclick="delete_menu(' + data[i]['id'] + ',\'' + data[i]['title'] + '\')"/>';
                        htm += '<div class="clearfix"></div>';
                        htm += '</form>';
                        data.splice(i, 1);
                        htm += show_menu(data, tmp_parent);
                        htm += '</li>';
                    } else {
                        i++;
                    }
                }
                if (htm != '') {
                    htm = '<ol class="dd-list">' + htm + '</ol>';
                }
                return htm;
            }

            function create_menu(root) {
                var htm = '';
                htm += '<form action="" id="create_menu_' + root + '" class="menu-content-create">';
                htm += '<label>Tiêu Đề*</label>';
                htm += '<input type="text" id="title_create" class="form-control input-sm validate[required]", />';
                htm += '<label>URL*</label>';
                htm += '<input type="text" id="url_create" class="form-control input-sm" />';
                htm += '<label>Mở trong Tab*</label>';
                htm += '<select id="target_create" class="form-control input-sm"><option value="1">Hiện tại</option><option value="2">Mới</option></select>';
                htm += '<label>Icon</label>';
                htm += '<input type="text" id="icon_create" class="form-control input-sm" />';
                htm += '<label>URL Ảnh</label>';
                htm += '<input type="text" id="thumbnail_create" class="form-control input-sm" />';
                htm += '<label>CSS</label>';
                htm += '<textarea id="css_create" class="form-control" rows="3"></textarea>';
                htm += '</form>';

                $('#form_dialog').html(htm);
                $('#form_dialog').dialog({
                    modal: true,
                    title: 'Thêm',
                    dialogClass: "dialogzindex",
                    autoOpen: true,
                    width: '450px',
                    resizable: false,
                    buttons: {
                        'Lưu': function () {
                            insert_menu(root);
                        },
                        'Đóng': function () {
                            $(this).dialog("close");
                            $('#form_dialog').empty();
                        }
                    }
                });
            }

            function insert_menu(root) {

                if (!$("#create_menu_" + root).validationEngine('validate')) {
                    return false;
                }

                process_menu();

                $.ajax({
                    url: '<?php echo base_url(); ?>admin/menus/insert_menu',
                    type: 'POST',
                    async: false,
                    data: {
                        title: get_value_element('title_create'),
                        url: get_value_element('url_create'),
                        target: get_value_element2('target_create'),
                        root: root,
                        parent: root,
                        icon: get_value_element('icon_create'),
                        thumbnail: get_value_element('thumbnail_create'),
                        css: get_value_element('css_create'),
                    },
                    datatype: 'html',
                    success: function (data) {
                        try {
                            data = JSON.parse(data);
                            if (!data.status) {
                                if (data.message == 'Vui lòng đăng nhập!') {
                                    load_login_form();
                                    return;
                                }
                                alert2(data.message);
                                return false;
                            } else {
                                load_menu(root);
                                process_menu_complete();
                                alert2("Thêm menu thành công!");
                            }
                        } catch (err) {
                            alert2("Dữ liệu trả về không đúng định dạng!");
                            return false;
                        }
                    }
                });
            }

            function update_menu(id) {
                if (!$("#update_menu_" + id).validationEngine('validate')) {
                    return false;
                }
                $('#btn_menuluu_' + id).attr('value', 'Lưu...');
                process_menu();
                $.ajax({
                    url: '<?php echo base_url(); ?>admin/menus/update_menu',
                    type: 'POST',
                    async: true,
                    data: {
                        id: id,
                        title: get_value_element('title_' + id),
                        url: get_value_element('url_' + id),
                        target: get_value_element2('target_' + id),
                        icon: get_value_element('icon_' + id),
                        thumbnail: get_value_element('thumbnail_' + id),
                        css: get_value_element('css_' + id),
                    },
                    datatype: 'html',
                    success: function (data) {
                        try {
                            data = JSON.parse(data);
                            if (!data.status) {
                                if (data.message == 'Vui lòng đăng nhập!') {
                                    load_login_form();
                                    return;
                                }
                                alert2(data.message);
                            } else {
                                $('#menu_' + id).html(get_value_element('title_' + id));
                            }
                        } catch (err) {
                            alert2("Dữ liệu trả về không đúng định dạng!");
                        }
                        delay(function () {
                            process_menu_complete();
                            $('#btn_menuluu_' + id).attr('value', 'Lưu');
                        }, 500);
                    }
                });
            }

            function delete_menu(id, title) {
                $('#confirm_dialog').empty();
                $('#confirm_dialog').append("Menu \"" + title + "\" và toàn bộ con của menu \"" + title + "\" sẽ bị xóa <br /> Bạn có chắc không?");
                $('#confirm_dialog').dialog({
                    modal: true,
                    title: 'Xác nhận',
                    dialogClass: "dialogzindex",
                    autoOpen: true,
                    width: 'auto',
                    resizable: false,
                    buttons: {
                        'Hủy': function () {
                            $(this).dialog("close");
                            $('#confirm_dialog').empty();
                        },
                        'Có': function () {
                            process_menu();
                            $.ajax({
                                url: '<?php echo base_url(); ?>admin/menus/delete_menu',
                                type: 'POST',
                                async: false,
                                data: {
                                    id: id,
                                    root: <?php echo $menu_root[0]['id']; ?>
                                },
                                datatype: 'html',
                                success: function (data) {
                                    try {
                                        data = JSON.parse(data);
                                        if (!data.status) {
                                            if (data.message == 'Vui lòng đăng nhập!') {
                                                load_login_form();
                                                return;
                                            }
                                            alert2(data.message);
                                        } else {
                                            //load lai menu
                                            load_menu(<?php echo $menu_root[0]['id']; ?>);
                                            //window.location = "<?php echo base_url() ?>admin/menus/";
                                        }
                                    } catch (err) {
                                        alert2("Dữ liệu trả về không đúng định dạng!")
                                    }
                                }
                            });
                            process_menu_complete();
                            $(this).dialog("close");
                            $('#confirm_dialog').empty();
                        }
                    },
                });

                delay(function () {
                    process_menu_complete();
                }, 500);
            }

            function save_order(root) {
                if ($('#nestable-' + root).nestable().length < 1) {
                    alert2("Không có menu nào");
                    return false;
                }
                process_menu();
                $('#btn-save-order-' + root).attr('value', "Lưu...");
                $.ajax({
                    url: '<?php echo base_url(); ?>admin/menus/save_order',
                    type: 'POST',
                    async: true,
                    data: {
                        root: root,
                        menus: $('#nestable-' + root).nestable('serialize'),
                    },
                    datatype: 'html',
                    success: function (data) {
                        try {
                            data = JSON.parse(data);
                            if (!data.status) {
                                if (data.message == 'Vui lòng đăng nhập!') {
                                    load_login_form();
                                    return;
                                }
                                alert2(data.message);
                            } else {
                                //load_menu(root);
                            }
                        } catch (err) {
                            alert2("Dữ liệu trả về không đúng định dạng!")
                        }
                        $('#btn-save-order-' + root).attr('value', "Lưu")
                        process_menu_complete();
                    }
                });

            }
        </script>
    </div>
    <div class="clearfix"></div>
</div>