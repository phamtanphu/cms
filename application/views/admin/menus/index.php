<div id="tabs-menu" class="admin-menu">
    <ul>
        <li class="menu-process" id="process-menu"><img src="<?php echo base_url() ?>public/css/img/process-menu.gif"/></li>
        <?php
        if (isset($data) && is_array($data)) {
            foreach ($data as $item) {
                echo '<li><a href="' . base_url() . 'admin/menus/load_menu?root=' . $item['id'] . '">' . $item['title'] . '</a></li>';
            }
        }
        ?>
        <li><a href="#tabs-add">+</a></li>
    </ul>
    <div id="tabs-add">
        <form action="/admin/menus/insert_root_menu" id = "create_root_menu" class="clearfix create_root_menu" onsubmit="insert_root_menu();
                        return false">
            <input type="text" class="form-control input-xs validate[required]"  name="root_title" id="root_title" placeholder="Tên menu"/>
            <input type="button" value="Thêm" class="btn btn-xs" onclick="insert_root_menu()"/>
        </form>
    </div>	
</div>
<script>

    $(function () {
        $("#tabs-menu").tabs({
            beforeLoad: function (event, ui) {
                ui.jqXHR.fail(function () {
                    ui.panel.html("Đang load ....");
                });
            }
        });
        process_menu_complete();
    });


    function insert_root_menu() {
        if (!$("#create_root_menu").validationEngine('validate')) {
            return false;
        }
        process_menu();
        $.ajax({
            url: '<?php echo base_url(); ?>admin/menus/insert_root_menu',
            type: 'POST',
            async: false,
            data: {
                title: get_value_element('root_title'),
            },
            datatype: 'html',
            success: function (data) {
                try {
                    data = JSON.parse(data);
                    if (!data.status) {
                        if (data.message == 'Vui lòng đăng nhập!') {
                            load_login_form();
                            return;
                        }
                        alert2(data.message);
                    } else {
                        window.location = "<?php echo base_url() ?>admin/menus/";
                    }
                } catch (err) {
                    alert2("Dữ liệu trả về không đúng định dạng!")
                }
                process_menu_complete();
            }
        });
    }

    function process_menu() {
        $('#process-menu').html('<img src="<?php echo base_url() ?>public/css/img/process-menu.gif"/>');
    }

    function process_menu_complete() {
        $('#process-menu').html('<img src="<?php echo base_url() ?>public/css/img/process-menu.png"/>');
    }
</script>