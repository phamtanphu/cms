<ul id="admin-menu" class="menu">

    <li><a href="<?php echo base_url(); ?>admin/posts/" class="cat">Bài Viết</a></li>
    <li><a href="<?php echo base_url(); ?>admin/posts/create">Thêm</a></li>
    <li><a href="<?php echo base_url(); ?>admin/posts/">Tất Cả</a></li>
    <li><a href="<?php echo base_url(); ?>admin/categories">Chuyên Mục</a></li>

    <li><a href="<?php echo base_url(); ?>admin/#products/" class="cat">Sản Phẩm</a></li>
    <li><a href="<?php echo base_url(); ?>admin/#products/create">Thêm</a></li>
    <li><a href="<?php echo base_url(); ?>admin/#products/">Tất Cả</a></li>
    <li><a href="<?php echo base_url(); ?>admin/#products/">Loại Sản Phẩm</a></li>

    <li><a href="<?php echo base_url(); ?>admin/order/" class="cat">Đơn Hàng</a></li>
    <li><a href="<?php echo base_url(); ?>admin/order/new">Đơn Hàng Mới</a></li>
    <li><a href="<?php echo base_url(); ?>admin/order/cart">Giỏ Hàng Chờ</a></li>
    <li><a href="<?php echo base_url(); ?>admin/order/complete">Đã Hoàn Thành</a></li>
    <li><a href="<?php echo base_url(); ?>admin/order">Tất Cả Đơn Hàng</a></li>

    <li><a href="<?php echo base_url(); ?>javascript:void(0);" class="cat">Tùy Chỉnh</a></li>
    <li><a href="<?php echo base_url(); ?>admin/slider/">Slider</a></li>
    <li><a href="<?php echo base_url(); ?>admin/menus/">Menu</a></li>
    <li><a href="<?php echo base_url(); ?>admin/#layout/">Giao Diện</a></li>

    <li><a href="javascript:void(0);" class="cat">Hệ Thống</a></li>
    <li><a href="<?php echo base_url(); ?>admin/#backup_restore/">Sao Lưu - Phục Hồi</a></li>
    <li><a href="<?php echo base_url(); ?>admin/user/change_password">Đổi Mật Khẩu</a></li>
    <li><a href="<?php echo base_url(); ?>admin/user/">Người Dùng</a></li>
    <li><a href="<?php echo base_url(); ?>admin/permision/">Phân Quyền</a></li>
    <li><a href="<?php echo base_url(); ?>admin/email/">Cấu Hình Email</a></li>
    <li><a href="<?php echo base_url(); ?>admin/logout">Thoát</a></li>
</ul>