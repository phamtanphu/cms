<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>.:: Đăng Nhập - Admin Panel ::.</title>

        <!-- Base require -->
        <link  rel="stylesheet" href="http://localhost/backend/public/css/global.css" />
        <script src="http://localhost/backend/public/js/jquery.min.js"></script>

        <!-- Include one of jquery-ui styles. -->
        <link rel="stylesheet" href="http://localhost/backend/public/plugin/jquery-ui/jquery-ui.min.css"  type="text/css" />
        <!-- Include jquery-ui script file. -->
        <script src="http://localhost/backend/public/plugin/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

        <!-- Include one of validationengine styles. -->
        <link rel="stylesheet" href="http://localhost/backend/public/plugin/validationengine/css/validationEngine.jquery.css"  type="text/css" />
        <!-- Include validationengine script file. -->
        <script src="http://localhost/backend/public/plugin/validationengine/js/jquery.validationEngine.js" type="text/javascript"></script>
        <script src="http://localhost/backend/public/plugin/validationengine/js/languages/jquery.validationEngine-vi.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>public/plugin/JavaScript-MD5/js/md5.min.js" type="text/javascript"></script>

        <script type="text/javascript">

            function alert2(mess) {
                $('#alert_dialog').empty();
                $('#alert_dialog').append(mess);
                $('#alert_dialog').dialog({
                    modal: true,
                    title: 'Thông Báo!',
                    dialogClass: "dialogzindex",
                    autoOpen: true,
                    width: 'auto',
                    resizable: false,
                    buttons: {
                        'Đóng': function () {
                            $(this).dialog("close");
                            $('#alert_dialog').empty();
                        }
                    }
                });
            }

            function check_login() {
                var ref;
                try {
                    ref = location.href.match(/ref=(.*)/);
                    ref = (ref != 'null') ? decodeURIComponent(ref[1]) : '<?php echo base_url(); ?>admin';
                    console.log(ref);
                    var patt = /<?php echo str_replace(array('/'), array('\/'), base_url()); ?>admin/;
                    ref = (patt.test(ref)) ? ref : '<?php echo base_url() ?>admin';

                    console.log(ref);
                } catch (e) {
                    ref = '<?php echo base_url() ?>admin';
                }

                if (!$("#login_form").validationEngine('validate')) {
                    return false;
                }

                $.ajax({
                    url: '<?php echo base_url(); ?>admin/check_login',
                    type: 'POST',
                    data: {
                        username: $('#username').val().trim(),
                        password: md5($('#password').val()),
                    },
                    datatype: 'json',
                    async: false,
                    success: function (data) {
                        try {
                            var data = JSON.parse(data);
                            if (data.status) {
                                location.href = ref;
                            } else {
                                alert2(data.message);
                            }
                        } catch (err) {
                            alert2("Lỗi: " + err.message);
                        }
                    }
                });

            }

        </script>
        <style type="text/css">
            .ui-dialog{padding:0px!important;}
            .ui-dialog-titlebar{border:none!important;border-bottom:none!important;}
            .ui-dialog .formError {z-index: 99999!important;}
            .ui-widget-content{font-size:1em!important;}
            a.ui-tabs-anchor{outline:none}

            body{background-color:#F7F7F7;}
            #login_form{margin:45% 30px 7px 30px; padding:20px 50px; border:3px solid #ddd; border-radius:2px; box-shadow: 0px 0px 6px #000;}
            #login_form label{display:block; margin-top:7px;}
            #login_form input[type="text"], #login_form input[type="password"]{display:block; width:100%;}
            .btn-primary, .btn-primary:hover {
                background-color: #222A2D;
                color: #bac2c8;
                border:none;
            }
            .lost_password{margin-top:20px;}
            a:hover{text-decoration:none;}

        </style>
    </head>

    <body>
        <div id="wraper">
            <div id="main">
                <div class="row">
                    <div class="col col-4"></div>
                    <div class="col col-4">
                        <form action="<?php echo base_url() ?>admin/check_login" method="POST" id="login_form" class="" onsubmit="return false;">

                            <label>Tên đăng nhập</label>
                            <input value="0977201157" type="text" name="username" id="username" class="form-control input-sm validate[required]" maxlength="255" autocomplete="off" autofocus />

                            <label>Mật khẩu</label>
                            <input value="107621" type="password" name="password" id="password" class="form-control input-sm validate[required]" maxlength="255" />

                            <br />
                            <input type="submit" class="btn btn-primary btn-sm" value="Đăng Nhập" onclick="check_login()" />
                            <a href="<?php echo base_url() ?>admin/lost_password" class="right bold italic lost_password">Quên mật khẩu</a>

                        </form>
                    </div>
                    <div class="col col-4"></div>
                </div>
            </div>
            <div id="footer">
                <div id="alert_dialog"></div>
                <div id="confirm_dialog"></div>
                <div id="form_dialog"></div>
            </div>
        </div>
    </body>

</html>