<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>.:: Khôi Phục Mật Khẩu - Admin Panel ::.</title>

        <!-- Base require -->
        <link  rel="stylesheet" href="http://localhost/backend/public/css/global.css" />
        <script src="http://localhost/backend/public/js/jquery.min.js"></script>

        <!-- Include one of jquery-ui styles. -->
        <link rel="stylesheet" href="http://localhost/backend/public/plugin/jquery-ui/jquery-ui.min.css"  type="text/css" />
        <!-- Include jquery-ui script file. -->
        <script src="http://localhost/backend/public/plugin/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

        <!-- Include one of validationengine styles. -->
        <link rel="stylesheet" href="http://localhost/backend/public/plugin/validationengine/css/validationEngine.jquery.css"  type="text/css" />
        <!-- Include validationengine script file. -->
        <script src="http://localhost/backend/public/plugin/validationengine/js/jquery.validationEngine.js" type="text/javascript"></script>
        <script src="http://localhost/backend/public/plugin/validationengine/js/languages/jquery.validationEngine-vi.js" type="text/javascript"></script>

        <script type="text/javascript">
            function alert2(mess) {
                $('#alert_dialog').empty();
                $('#alert_dialog').append(mess);
                $('#alert_dialog').dialog({
                    modal: true,
                    title: 'Thông Báo!',
                    dialogClass: "dialogzindex",
                    autoOpen: true,
                    width: 'auto',
                    resizable: false,
                    buttons: {
                        'Đóng': function () {
                            $(this).dialog("close");
                            $('#alert_dialog').empty();
                        }
                    }
                });
            }
			
			var action = false;

            function reset_password() {
				document.getElementById("btn-reset_password").value = 'Đang khôi phục....';
                if (!$("#reset_password_form").validationEngine('validate')) {
                    return false;
                }
				
				
				
				if(!action){
					
					action = true;
					$.ajax({
						url: '<?php echo base_url(); ?>admin/reset_password',
						type: 'POST',
						async: false,
						data: {
							username: $('#username').val().trim(),
							email: $('#email').val().trim(),
						},
						datatype: 'html',
						async: false,
						success: function (data) {
							try {
								var data = JSON.parse(data);
								if (data.status) {
									//$('#username,#email').val('');
									alert2("Khôi phục mật khẩu thành công, vui lòng truy cập email để lấy mật khẩu!")
								} else {
									alert2(data.message);
								}
							} catch (err) {
								alert2("Lỗi: " + err.message);
							}
							
						}
					});
				}
				
				action = false;
				//document.getElementById("btn-reset_password").value = 'Khôi Phục';

            }

        </script>
        <style type="text/css">
            .ui-dialog{padding:0px!important;}
            .ui-dialog-titlebar{border:none!important;border-bottom:none!important;}
            .ui-dialog .formError {z-index: 99999!important;}
            .ui-widget-content{font-size:1em!important;}
            a.ui-tabs-anchor{outline:none}

            body{background-color:#F7F7F7;}
            #reset_password_form{margin:45% 30px auto 30px; padding:20px 50px; border:3px solid #ddd; border-radius:2px; box-shadow: 0 0 6px #000;}
            #reset_password_form label{display:block; margin-top:7px;}
            #reset_password_form input[type="text"], #reset_password_form input[type="password"]{display:block; width:100%;}
            .btn-primary, .btn-primary:hover {
                background-color: #222A2D;
                color: #bac2c8;
                border:none;
            }
            .lost_password{margin-top:15px;}
            .reset_password-note{color:red; padding-top:7px;}
            a:hover{text-decoration:none;}
        </style>
    </head>

    <body>
        <div id="wraper">
            <div id="main">
                <div class="row">
                    <div class="col col-4"></div>
                    <div class="col col-4">
                        <form action="<?php echo base_url() ?>admin/reset_password" method="POST" id="reset_password_form" class="" onsubmit="reset_password();
                                                        return false;">

                            <label>Tên đăng nhập</label>
                            <input type="text" name="username" id="username" class="form-control input-sm validate[required,custom[onlyLetterNumber]]" maxlength="255" autocomplete="off" autofocus />

                            <label>Email</label>
                            <input type="text" name="email" id="email" class="form-control input-sm validate[required,custom[email]]" maxlength="255" autocomplete="off" />

                            <br />
                            <input type="submit" class="btn btn-primary btn-sm" id="btn-reset_password" value="Khôi Phục" />
                            <a href="<?php echo base_url() ?>admin/login" class="right bold italic lost_password login">Về đăng nhập</a>
                            <div class="clearfix"></div>

                        </form>
                        <div class="text-center small italic reset_password-note">** Mật khẩu khôi phục chỉ có thời hạn trong 3 giờ</div>
                    </div>
                    <div class="col col-4"></div>
                </div>
            </div>
            <div id="footer">
                <div id="alert_dialog"></div>
                <div id="confirm_dialog"></div>
                <div id="form_dialog"></div>
            </div>
        </div>
    </body>

</html>