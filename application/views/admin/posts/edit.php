<form action="<?php echo base_url() ?>admin/posts/update" method="POST" id="create_post">
    <div class="row">
        <div class="col col-9">
            <input type="hidden" name="id" value ="<?php echo (isset($post['id'])) ? $post['id'] : ''; ?>" />
            <label>Tiêu đề bài viết</label>
            <input type="text" class="form-control input-sm validate[required]" name="title" value="<?php echo (isset($post['title'])) ? $post['title'] : ''; ?>" id="title" placeholder="Tiêu đề bài viết"/>

            <label>Mô tả bài viết</label>
            <textarea name="description" class="form-control" rows="5" placeholder="Mô tả bài viết"><?php echo (isset($post['description'])) ? $post['description'] : ''; ?></textarea>

        </div>
        <div class="col col-3">
            <div style="margin:7px 10px;">
                <label>Hình đại diện</label>
                <div style="height:170px; background-color:#aaa; margin:7px 0px;" class="text-center" >
                    <div></div>
                    <input type="hidden" value="<?php echo (isset($post['thumbnail'])) ? $post['thumbnail'] : ''; ?>" name="thumbnail" />
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col col-9">
            <label>Chi tiết bài viết</label>
            <textarea name="detail" id="detail" class="form-control" rows="17" placeholder="Chi tiết bài viết"><?php echo (isset($post['detail'])) ? $post['detail'] : ''; ?></textarea>
        </div>
        <div class="col col-3">
            <div style="margin:7px 10px;">
                <label><input type="checkbox" name="categoryAll" id="categoryAll" onclick="toggle_all_checkbox(this, 'category[]')" title="Chọn Tất Cả"/> Chuyên mục</label>
                <div style="border:1px solid #ddd; height:250px; width:100%; overflow:scroll; overflow-x:hidden; padding:0px 7px; white-space: nowrap;">
                    <?php foreach ($categories as $item): ?>
                        <label style="font-weight:normal" ><?php echo $item['name'] ?></label>
                    <?php endforeach ?>
                </div>

                <label>Từ khóa</label>
                <input type="text" class="form-control input-sm" name="tag" id="tag" placeholder="tag" value="<?php echo (isset($tags)) ? $tags : ''; ?>"/>

                <label>Trạng Thái</label>
                <select name="status" id="status" class="form-control input-sm">
                    <option value="1" <?php echo (isset($post['status']) && $post['status'] == 1 ) ? 'selected="selected"' : ''; ?>>Hiện</option>
                    <option value="0" <?php echo (isset($post['status']) && $post['status'] == 0 ) ? 'selected="selected"' : ''; ?>>Ẩn</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col col-9">
            <br />
            <input type="submit" value="Lưu" class="btn btn-primary btn-sm right"/>
        </div>
        <div class="col col-3">
        </div>
    </div>
</form>
<script type="text/javascript">

    update_chk_parent('categoryAll', 'category[]');

    tinymce.init({
        selector: "textarea#detail",
        //theme: "modern",
        //skin: "flat",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor responsivefilemanager",
                    //"adddrive"
        ],
        toolbar: "code undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | forecolor backcolor | bullist numlist outdent indent | link image media responsivefilemanager | emoticons preview fullscreen",
        image_advtab: true,
        external_filemanager_path: "<?php echo base_url() ?>public/plugin/responsive_filemanager/filemanager/",
        filemanager_title: "Quản Lý File",
        external_plugins: {"filemanager": "<?php echo base_url() ?>public/plugin/responsive_filemanager/filemanager/plugin.min.js"},
        language: 'vi',
        relative_urls: false,
        //remove_script_host: false,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ],
        height: 350,
        /* file_browser_callback: function(field_name, url, type, win) { 
         //selectData('picasa');
         tinymce.activeEditor.windowManager.close();
         } */
    });

    $('#tag').tagit({
        allowSpaces: true,
        //availableTags: ["c++", "java", "php", "javascript", "ruby", "python", "c"],
        //autocomplete: {delay: 0, minLength: 1}
    });

    $('li .ui-autocomplete-input').autocomplete({
        source: "<?php echo base_url(); ?>admin/tags/get_tags",
        minLength: 2,
        delay: 400,
    });

    $("#create_post").validationEngine();

</script>
