<style type="text/css">
    tr.jtable-data-row>td:nth-child(10), thead>tr:first-child>th:nth-child(10){display:none;}
    #jtable-toolbarsearch-status{display:none;}
</style>
<div id="data"></div>
<script type="text/javascript">
    function get_data() {
        $('#data').jtable({
            title: "Bài Viết",
            paging: true,
            pageSize: 20,
            sorting: true,
            defaultSorting: 'created_date DESC',
            selecting: true,
            multiselect: false,
            toolbarsearch: true,
            selectingCheckboxes: true,
            actions: {
                listAction: '<?php echo base_url(); ?>admin/posts/get_data',
                updateAction: '<?php echo base_url(); ?>admin/posts/quick_update',
                deleteAction: '<?php echo base_url(); ?>admin/posts/delete'
            },
            toolbar: {
                items: [
                    {
                        'icon': '',
                        'text': '<span class="jtable-toolbar-item-icon ui-icon ui-icon-plus"></span> Thêm bài viết',
                        click: function () {
                            location.href = '<?php echo base_url() ?>admin/posts/create';
                        },
                    },
                    {
                        'icon': '',
                        'text': '<span class="jtable-toolbar-item-icon ui-icon ui-icon-refresh"></span> Refresh',
                        click: function () {
                            $('#data').jtable('reload');
                        },
                        'cssClass': 'right'
                    }
                ]
            },
            fields: {
                id: {
                    title: 'ID',
                    key: true,
                    create: false,
                    edit: false,
                    list: false
                },
                /* thumbnail: {
                 title: 'Ảnh Đại Diện',
                 listClass: 'text-center',
                 display: function (data) {
                 if (data.record.thumbnail != '') {
                 return '<img  src="' + data.record.thumbnail + '" alt="' + data.record.title + '" style="max-width:89px; max-height:50px;" />';
                 }
                 return '';
                 },
                 }, */
                title: {
                    title: 'Tiêu Đề',
                    width: '50%',
                    inputClass: 'validate[required]',
                },
                category_name: {
                    title: 'Chuyên Mục',
                    width: '10%',
                    edit: false
                },
                tag_name: {
                    title: 'Từ Khóa',
                    width: '10%',
                },
                username: {
                    title: 'Tác Giả',
                    width: '7%',
                    edit: false
                },
                created_date: {
                    title: 'Ngày',
                    width: '5%',
                    type: 'date',
                    displayFormat: 'dd-mm-yy',
                    display: function (data) {
                        if (data.record.created_date != '') {
                            var r_date = data.record.created_date
                            return r_date.substring(8, 10) + '-' + r_date.substring(5, 7) + '-' + r_date.substring(0, 4) + ' ' + r_date.substring(11, 21);
                        }
                    },
                    edit: false
                },
                status: {
                    title: 'Hiện',
                    //type: 'checkbox',
                    //values: { '0' : 'Passive', '1' : 'Active' },
                    //setOnTextClick: false,
                    options: [{"Value": "1", "DisplayText": "Hiện"}, {"Value": "0", "DisplayText": "Ẩn"}],
                    width: '2%',
                    listClass: 'text-center'
                },
                duplicate: {
                    width: '2%',
                    listClass: 'text-center',
                    create: false,
                    edit: false,
                    sorting: false,
                    display: function (data) {
                        return '<a href="javscript:void(0)" title="Tạo bản sao" onclick="duplicate(' + data.record.id + ');"><img src="<?php echo base_url() ?>public/plugin/jtable/themes/basic/duplicate.png" style="width:16px;eight:16px;" alt="Tạo bản sao" /></a>';
                    }
                },
                edit: {
                    width: '2%',
                    listClass: 'text-center',
                    create: false,
                    edit: false,
                    sorting: false,
                    display: function (data) {
                        return '<a href="<?php echo base_url(); ?>admin/posts/edit?post_id=' + data.record.id + '" title="Sửa bài viết"><img src="<?php echo base_url() ?>public/plugin/jtable/themes/lightcolor/edit.png" style="width:16px;eight:16px;" alt="Sửa bài viết" /></a>';
                    }
                },
            },
            formCreated: function (event, data) {
                data.form.validationEngine();
                data.form[0].style.minWidth = "500px";
                var $dialogDiv = data.form.closest('.ui-dialog');
                $dialogDiv.position({
                    my: "center",
                    at: "center",
                    of: window
                });

                $('#Edit-tag_name').tagit({
                    allowSpaces: true,
                });

                $('li .ui-autocomplete-input').autocomplete({
                    source: "<?php echo base_url(); ?>admin/tags/get_tags",
                    minLength: 2,
                    delay: 400,
                });
            },
            formSubmitting: function (event, data) {
                return data.form.validationEngine('validate');
            },
            formClosed: function (event, data) {
                data.form.validationEngine('hide');
                data.form.validationEngine('detach');
            },
            rowUpdated: function () {
                $('#data').jtable('reload');
            },
            rowInserted: function (event, data) {
                $(data.row).dblclick(function () {
                    $(this).trigger('click');
                    $(this).find('.jtable-edit-command-button').trigger('click');
                });
            }
        });
        //get_data_filter();
        $('#data').jtable('reload');
    }

    function get_data_filter() {
        $('#data').jtable('load', {
            //_l_id: get_value_element("jtable-toolbarsearch-id"),
            _l_title: get_value_element("jtable-toolbarsearch-title"),
            _l_category_name: get_value_element("jtable-toolbarsearch-category_name"),
            _l_tag_name: get_value_element("jtable-toolbarsearch-tag_name"),
            _l_username: get_value_element("jtable-toolbarsearch-username"),
            _d_created_date: get_value_element("jtable-toolbarsearch-created_date"),
            p_w_status: get_value_element("jtable-toolbarsearch-status_w"),
        });
    }

    function duplicate(id) {
        $.ajax({
            url: '<?php echo base_url(); ?>admin/posts/duplicate',
            type: 'POST',
            data: {
                id: id,
            },
            datatype: 'html',
            async: false,
            success: function (data) {
                try {
                    var data = JSON.parse(data);
                    if (data.Result == 'OK') {
                        alert2('Tạo bản sao thành công!');
                        $('#data').jtable('reload');
                    } else {
                        alert2(data.Message);
                    }
                } catch (err) {
                    alert2("Không thể lấy được dữ liệu trả về!")
                }
            }
        });
    }

    get_data();

    $(document).ready(function () {
        $('#jtable-toolbarsearch-duplicate, #jtable-toolbarsearch-edit').remove();
        $('th.jtable-toolbarsearch-reset').attr("colspan", "2");
        $('#jtable-toolbarsearch-status').after('<select name="status" id="jtable-toolbarsearch-status_w" class="jtable-toolbarsearch"><option value="">Chọn</option><option value="1">Hiện</option><option value="0">Ẩn</option></select>');
        $('#jtable-toolbarsearch-status').remove();
    })
</script>
