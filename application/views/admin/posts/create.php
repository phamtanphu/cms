<form action="<?php echo base_url() ?>admin/posts/insert" method="POST" id="create_post">
    <div class="row">
        <div class="col col-9">
            <label>Tiêu đề bài viết</label>
            <input type="text" class="form-control input-sm validate[required]" name="title" id="title" placeholder="Tiêu đề bài viết"/>

            <label>Mô tả bài viết</label>
            <textarea name="description" class="form-control" rows="5" placeholder="Mô tả bài viết"></textarea>

        </div>
        <div class="col col-3">
            <div style="margin:7px 10px;">
                <b>Hình đại diện: 
					<label for="choose-thumbnail" style="font-weight:normal; cursor:pointer; display:inline;" >Chọn File...
						<input type="file" id="choose-thumbnail" accept="image/*" class="hidden"/>
					</label>
				</b>
                <div style="height:170px; background-color:#aaa; margin:7px 0px;" class="text-center" >
                    <div id="thumbnailPreview"></div>
                    <input type="hidden" value="" name="thumbnail" />
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col col-9">
            <label>Chi tiết bài viết</label>
            <textarea name="detail" id="detail" class="form-control" rows="17" placeholder="Chi tiết bài viết"></textarea>
        </div>
        <div class="col col-3">
            <div style="margin:7px 10px;">
                <label><input type="checkbox" name="categoryAll" id="categoryAll" onclick="toggle_all_checkbox(this, 'category[]')" title="Chọn Tất Cả"/> Chuyên mục</label>
                <div style="border:1px solid #ddd; height:250px; width:100%; overflow:scroll; overflow-x:hidden; padding:0px 7px; white-space: nowrap;">
                    <?php foreach ($categories as $item): ?>
                        <label style="font-weight:normal" ><?php echo $item['name'] ?></label>
                    <?php endforeach ?>
                </div>

                <label>Từ khóa</label>
                <input type="text" class="form-control input-sm" name="tag" id="tag" placeholder="tag"/>

                <label>Trạng Thái</label>
                <select name="status" id="status" class="form-control input-sm">
                    <option value="1">Hiện</option>
                    <option value="0">Ẩn</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col col-9">
            <br />
            <input type="submit" value="Lưu" class="btn btn-primary btn-sm right"/>
        </div>
        <div class="col col-3">
        </div>
    </div>
</form>
<script type="text/javascript">

	var $inputImage = $('#choose-thumbnail');
	var URL = window.URL || window.webkitURL;
	var blobURL;

	if (URL) {
		$inputImage.change(function () {
			var files = this.files;
			var file;
			if (files && files.length) {
				file = files[0];
				if (/^image\/\w+$/.test(file.type)) {
					blobURL = URL.createObjectURL(file);
					$('#thumbnailPreview').html('<img src="' + blobURL + '" id="image" />');
					$('#image').cropper({
						viewMode: 3,
						//aspectRatio: 16 / 9,
						dragMode: 'move',
						autoCropArea: 1,
						restore: false,
						modal: false,
						//guides: true,
						//highlight: true,
						cropBoxMovable: false,
						cropBoxResizable: false,
						//minCropBoxWidth: 250,
						//minCropBoxHeight: 250 * 9 / 16
					});
				} else {
					alert2('Chọn một hình ảnh');
				}
			}
		});
	}

    update_chk_parent('categoryAll', 'category[]');

    tinymce.init({
        selector: "textarea#detail",
        //theme: "modern",
        //skin: "flat",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor responsivefilemanager",
                    //"adddrive"
        ],
        toolbar: "code undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | forecolor backcolor | bullist numlist outdent indent | link image media responsivefilemanager | emoticons preview fullscreen",
        image_advtab: true,
        external_filemanager_path: "<?php echo base_url() ?>public/plugin/responsive_filemanager/filemanager/",
        filemanager_title: "Quản Lý File",
        external_plugins: {"filemanager": "<?php echo base_url() ?>public/plugin/responsive_filemanager/filemanager/plugin.min.js"},
        language: 'vi',
        relative_urls: false,
        //remove_script_host: false,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ],
        height: 350,
        /* file_browser_callback: function(field_name, url, type, win) { 
         //selectData('picasa');
         tinymce.activeEditor.windowManager.close();
         } */
    });

    $('#tag').tagit({
        allowSpaces: true,
        //availableTags: ["c++", "java", "php", "javascript", "ruby", "python", "c"],
        //autocomplete: {delay: 0, minLength: 1}
    });

    $('li .ui-autocomplete-input').autocomplete({
        source: "<?php echo base_url(); ?>admin/tags/get_tags",
        minLength: 2,
        delay: 400,
    });

    $("#create_post").validationEngine();

</script>