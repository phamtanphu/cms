<style type="text/css">
    #data .jtable-title{display:none!important;}
</style>
<div class="row">
    <div class="col col-4">
        <form action="<?php echo base_url() ?>admin/categories/insert_data" method="POST" id="category_form" class="category_form" onsubmit="insert_category();
                return false;">
            <label>Chuyện Mục</label>
            <input type="text" name="category_name" id="category_name" class="form-control input-sm validate[required]" maxlength="255" />

            <label>Slug</label>
            <input type="text" name="category_slug" id="category_slug" class="form-control input-sm" maxlength="255" />

            <label>Cha</label>
            <select name="category_parent" id="category_parent" class="form-control input-sm">
                <option value="0">[ Không ]</option>
            </select>

            <label>Mô Tả</label>
            <textarea name="category_description" id="category_description" class="form-control" rows="3" maxlength="512"></textarea>

            <input type="submit" class="btn btn-primary btn-sm" value="Thêm chuyên mục" />
        </form>
    </div>
    <div class="col col-8">
        <div id="data"></div>
    </div>
</div>
<script type="text/javascript">
    function get_data() {
        $('#data').jtable({
            title: "&nbsp;",
            paging: false,
            //pageSize: 25,
            sorting: true,
            defaultSorting: 'id ASC',
            selecting: true,
            multiselect: false,
            toolbarsearch: true,
            selectingCheckboxes: true,
            deleteConfirmation: function (data) {
                data.deleteConfirmMessage = 'Chuyên mục "' + data.record.name + '" và toàn bộ chuyên mục con của chuyên mục "' + data.record.name + '" sẽ bị xóa!';
            },
            actions: {
                listAction: '<?php echo base_url(); ?>admin/categories/get_data',
                ///createAction: '<?php echo base_url(); ?>admin/categories/insert_data',
                updateAction: '<?php echo base_url(); ?>admin/categories/update_data',
                deleteAction: '<?php echo base_url(); ?>admin/categories/delete_data'
            },
            toolbar: {
                items: [
                    {
                        'icon': '',
                        'text': '<span class="jtable-toolbar-item-icon ui-icon ui-icon-refresh"></span> Refresh',
                        click: function () {
                            get_data_filter();
                        },
                        'cssClass': 'right'
                    }
                ]
            },
            fields: {
                id: {
                    title: 'ID',
                    key: true,
                    list: false,
                    create: false,
                    edit: false,
                    width: '2%',
                    visibility: 'hidden',
                },
                name: {
                    title: 'Chuyên Mục',
                    inputClass: 'validate[required]',
                },
                slug: {
                    title: 'Slug',
                },
                description: {
                    title: 'Mô Tả',
                    type: 'textarea',
                    width: '20%'
                },
                parent: {
                    title: 'Cha',
                    list: false,
                    options: [{"Value": "0", "DisplayText": "Không"}],
                },
            },
            formCreated: function (event, data) {

                if (data.formType === 'edit') {
                    $('#Edit-name').val($('#Edit-name').val().replace(/^_+/, ''));
                    $('#Edit-parent').html(load_category_parent_change(data.record.id));
                }

                data.form.validationEngine();
            },
            formSubmitting: function (event, data) {
                return data.form.validationEngine('validate');
            },
            formClosed: function (event, data) {
                data.form.validationEngine('hide');
                data.form.validationEngine('detach');
            },
            rowUpdated: function () {
                $('#data').jtable('reload');
            },
            recordsLoaded: function () {
                load_category_parent(get_value_element2('category_parent'));
            },
            rowInserted: function (event, data) {
                $(data.row).dblclick(function () {
                    $(this).find('.jtable-edit-command-button').trigger('click');
                    $(this).trigger('click');
                });
            }
        });
        get_data_filter();
    }

    function get_data_filter() {

        $('#data').jtable('load', {
            //_l_id: get_value_element("jtable-toolbarsearch-id"),
            _l_name: get_value_element("jtable-toolbarsearch-name"),
            _l_slug: get_value_element("jtable-toolbarsearch-slug"),
            _l_description: get_value_element("jtable-toolbarsearch-description"),
        });
    }

    function insert_category() {
        if (!$("#category_form").validationEngine('validate')) {
            return false;
        }

        $.ajax({
            url: '<?php echo base_url(); ?>admin/categories/insert_data',
            type: 'POST',
            data: {
                name: get_value_element('category_name'),
                slug: get_value_element('category_slug'),
                description: get_value_element('category_description'),
                parent: get_value_element2('category_parent'),
            },
            datatype: 'html',
            async: false,
            success: function (data) {
                try {
                    var data = JSON.parse(data);
                    if (data.Result == 'OK') {
                        $('#data').jtable('reload');
                        $("#category_form input[type='text']").val('');
                        //$("#category_form select option:selected").attr("selected", false);
                        $("#category_form textarea").val('');
                        $("#category_form input[type='text']:visible:first").focus();

                    } else {
                        if (data.Message == 'Vui lòng đăng nhập!') {
                            load_login_form();
                            return;
                        }
                        alert2(data.Message);
                    }
                } catch (err) {
                    alert2("Lỗi: " + err.message);
                }
            }
        });

    }

    function load_category_parent(curent) {

        var htm = '<option value="0">[ Không ]</option>';
        $.ajax({
            url: '<?php echo base_url(); ?>admin/categories/get_data',
            type: 'POST',
            datatype: 'html',
            async: false,
            success: function (data) {
                try {
                    var data = JSON.parse(data);
                    if (data.Result == 'OK') {
                        var select = ''
                        for (var i = 0; i < data.Records.length; i++) {
                            select = (data.Records[i]['id'] == curent) ? 'selected' : '';
                            htm += '<option value="' + data.Records[i]['id'] + '" ' + select + '>' + data.Records[i]['name'] + '</option>';
                        }
                    } else {
                        htm = '<option value="0">[[ Lỗi: Không load được chuyên mục ]]</option>';
                    }
                } catch (err) {
                    htm = '<option value="0">[[ Lỗi: Không load được chuyên mục ]]</option>';
                }
            }
        });

        $('#category_parent').html(htm);

    }

    function load_category_parent_change(curent) {
        var htm = '<option value="0">[ Không ]</option>';
        $.ajax({
            url: '<?php echo base_url(); ?>admin/categories/get_data',
            type: 'POST',
            datatype: 'html',
            async: false,
            success: function (data) {
                try {
                    var data = JSON.parse(data);
                    if (data.Result == 'OK') {

                        var select = '';
                        var disable = '';
                        var arr_disable = new Array(curent);
                        var parent = 0;

                        for (var i = 0; i < data.Records.length; i++) {
                            parent = (data.Records[i]['id'] == curent) ? data.Records[i]['parent'] : parent;
                        }

                        for (var i = 0; i < data.Records.length; i++) {

                            select = (data.Records[i]['id'] == parent) ? 'selected' : '';

                            if (arr_disable.indexOf(data.Records[i]['parent']) >= 0) {
                                arr_disable.push(data.Records[i]['id']);
                            }

                            disable = (arr_disable.indexOf(data.Records[i]['id']) >= 0) ? 'disabled = "disabled"' : '';

                            htm += '<option value="' + data.Records[i]['id'] + '" ' + select + ' ' + disable + ' >' + data.Records[i]['name'] + '</option>';
                        }

                    } else {

                        htm = '<option value="0">[[ Lỗi: Không load được chuyên mục ]]</option>';
                    }
                } catch (err) {
                    htm = '<option value="0">[[ Lỗi: Không load được chuyên mục ]]</option>';
                }
            }
        });
        return htm;
    }

    get_data();

</script>
