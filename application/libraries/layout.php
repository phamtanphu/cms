<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of layout
 *
 * @author Pham Tan Phu
 */
class Layout {

    private $CI = null;

    public function __construct() {
        $this->CI = &get_instance();
    }

    public function view($view_name, $data = array()) {

        $view = $this->CI->load->view($view_name, $data);
        $data['view_name'] = $view;
        $this->CI->load->view('layout', $data);
    }

    public function admin_view($view_name, $data = array()) {
		
			$view = $this->CI->load->view('admin/' . $view_name, $data, TRUE);
			$data['view'] = $view;
			$data['view_name'] = $view_name; // for css and js in layout

			$data['user'] = $this->CI->session->userdata('logged_user');
			$data['user']['avatar'] = ($this->CI->session->userdata('logged_user')['avatar'] != '') ? $this->CI->session->userdata('logged_user')['avatar'] : base_url() . 'public/admin/avatars/user.png';
			//$data['user']['personalization'] = json_decode($this->CI->session->userdata('logged_user')['personalization'], true);

			$this->CI->load->view('admin/layout', $data);

    }

}
