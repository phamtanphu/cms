<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Auth
 *
 * @author Pham Tan Phu
 */
class Auth {

    private $CI = null;

    public function __construct() {
        $this->CI = &get_instance();
        session_start();
    }

    public function check_logged() {
        if (!$this->CI->session->userdata('logged_user')) {
            redirect(base_url() . 'admin/login', 'location');
        }
    }

}
