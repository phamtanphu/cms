<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_slider
 *
 * @author PHAMTANPHU
 */
class m_slider extends MY_Model {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->_table = 'slider';
    }

    public function toggle_show_hide($id) {
        return $this->db->query("UPDATE slider SET status = IF(status = 1,0,1) WHERE id = " . $id);
    }

    public function order_slider($id, $action) {

        $this->db->trans_start();

        switch ($action) {
            case 'top':
                $maxorder = $this->get_maxorder();
                $this->update(array('order' => $maxorder + 1), array("id" => $id));
                break;
            case 'up':
                $slider = $this->get_data("*", array("id" => $id), array(), "", 0, 1);
                if (count($slider) == 1) {
                    $slider_up = $this->get_data("*", array("order >" => $slider[0]['order']), array(), "order ASC", 0, 1);
                    if (count($slider_up) > 0) {
                        $this->db->query("UPDATE slider SET `order` =" . $slider[0]['order'] . " WHERE `order` = " . $slider_up[0]['order']);
                        $this->update(array("order" => $slider_up[0]['order']), array("id" => $id));
                    } else {
                        $slider_equal = $this->get_data("*", array("order" => $slider[0]['order']), array(), "", 0, 1);
                        if (count($slider_equal) > 0) {
                            $this->db->query("UPDATE slider SET `order` = `order` + 1 WHERE id = " . $id);
                        }
                    }
                }
                break;
            case 'down':
                $slider = $this->get_data("*", array("id" => $id), array(), "", 0, 1);
                if (count($slider) == 1) {
                    $slider_down = $this->get_data("*", array("order <" => $slider[0]['order']), array(), "order DESC", 0, 1);
                    if (count($slider_down) > 0) {
                        $this->db->query("UPDATE slider SET `order` =" . $slider[0]['order'] . " WHERE `order` = " . $slider_down[0]['order']);
                        $this->update(array("order" => $slider_down[0]['order']), array("id" => $id));
                    } else {
                        $slider_equal = $this->get_data("*", array("order" => $slider[0]['order']), array(), "", 0, 1);
                        if (count($slider_equal) > 0) {
                            $this->db->query("UPDATE slider SET `order` = `order` - 1 WHERE id = " . $id);
                        }
                    }
                }
                break;
            case 'bottom':
                $minorder = $this->get_minorder();
                $this->update(array('order' => ($minorder - 1)), array("id" => $id));
                break;
        }

        //print_r($this->db->queries);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_complete();
            return TRUE;
        }
    }

    public function get_maxorder() {
        $q = $this->db->select("MAX(`order`) as maxorder")
                ->get($this->_table);
        return $q->row_array()['maxorder'];
    }

    public function get_minorder() {
        $q = $this->db->select("MIN(`order`) as minorder")
                ->get($this->_table);
        return $q->row_array()['minorder'];
    }

}
