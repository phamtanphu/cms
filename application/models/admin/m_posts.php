<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_post
 *
 * @author PHAMTANPHU
 */
class m_posts extends MY_Model {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->_table = 'posts';
    }

    public function get_post($where, $like, $order, $start, $limit) {

        $q = $this->db->select("p.id, title, category_name, tag_name, p.status, u.username, created_date", FALSE)
                ->join("(SELECT pc.post_id, GROUP_CONCAT(c.name) as category_name FROM post_category pc INNER JOIN categories c ON pc.category_id = c.id GROUP BY pc.post_id) as pc2", "p.id = pc2.post_id")
                ->join("(SELECT pt.post_id, GROUP_CONCAT(t.name) as tag_name FROM post_tag pt INNER JOIN tags t ON pt.tag_id = t.id GROUP BY pt.post_id) as pt2", "p.id = pt2.post_id", "LEFT")
                ->join("user u", "p.created_user = u.id")
                ->where($where)
                ->like($like)
                ->order_by($order)
                ->get("posts p", $limit, $start);

        //print_r($this->db->queries); exit();

        return $q->result_array();
    }

    public function get_num_post($where, $like) {

        return $this->db->select("p.id", FALSE)
                        ->join("(SELECT pc.post_id, GROUP_CONCAT(c.name) as category_name FROM post_category pc INNER JOIN categories c ON pc.category_id = c.id GROUP BY pc.post_id) as pc2", "p.id = pc2.post_id")
                        ->join("(SELECT pt.post_id, GROUP_CONCAT(t.name) as tag_name FROM post_tag pt INNER JOIN tags t ON pt.tag_id = t.id GROUP BY pt.post_id) as pt2", "p.id = pt2.post_id", "LEFT")
                        ->join("user u", "p.created_user = u.id")
                        ->where($where)
                        ->like($like)
                        ->count_all_results("posts p");
    }

    public function insert_post($post_data, $categories, $tags) {

        $this->db->trans_start();

        $this->db->insert($this->_table, $post_data);

        $post_id = $this->get_maxid();

        if (!is_array($categories) || count($categories) == 0) {
            $categories = array(0);
        }

        $this->db->query("INSERT IGNORE INTO post_category(post_id, category_id) VALUES(" . $post_id . ", " . implode("), (" . $post_id . ", ", $categories) . ")");

        if ($tags != '') {

            $this->db->query("INSERT IGNORE INTO tags(name) VALUES('" . str_replace(",", "'), ('", $tags) . "')");

            $this->db->query("INSERT INTO post_tag(post_id, tag_id) SELECT " . $post_id . ", id FROM tags WHERE name IN('" . str_replace(",", "','", $tags) . "')");
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_complete();
            return TRUE;
        }
    }

    public function update_post($post_data, $categories, $tags) {

        $this->db->trans_start();

        $post_id = $post_data['id'];
        unset($post_data['id']);
        $post_data['edit_date'] = date("Y-m-d H:i:s");

        $this->db->update($this->_table, $post_data, array("id" => $post_id));

        if (!is_array($categories) || count($categories) == 0) {
            $categories = array(0);
        }
        $this->db->delete("post_category", array("post_id" => $post_id));
        $this->db->query("INSERT IGNORE INTO post_category(post_id, category_id) VALUES(" . $post_id . ", " . implode("), (" . $post_id . ", ", $categories) . ")");

        $this->db->delete("post_tag", array("post_id" => $post_id));
        if ($tags != '') {
            $this->db->query("INSERT IGNORE INTO tags(name) VALUES('" . str_replace(",", "'), ('", $tags) . "')");
            $this->db->query("INSERT INTO post_tag(post_id, tag_id) SELECT " . $post_id . ", id FROM tags WHERE name IN('" . str_replace(",", "','", $tags) . "')");
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_complete();
            return TRUE;
        }
    }

    public function quick_update_post($post_data, $tags, $post_id) {

        $this->db->trans_start();

        $post_data['edit_date'] = date("Y-m-d H:i:s");

        $this->db->update($this->_table, $post_data, array("id" => $post_id));

        $this->db->delete("post_tag", array("post_id" => $post_id));
        if ($tags != '') {
            $this->db->query("INSERT IGNORE INTO tags(name) VALUES('" . str_replace(",", "'), ('", $tags) . "')");
            $this->db->query("INSERT INTO post_tag(post_id, tag_id) SELECT " . $post_id . ", id FROM tags WHERE name IN('" . str_replace(",", "','", $tags) . "')");
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_complete();
            return TRUE;
        }
    }

    public function duplicate_post($post_id) {

        $this->db->trans_start();

        $this->db->query("INSERT INTO posts(title, slug, description, thumbnail, detail, created_user, status) SELECT title, slug, description, thumbnail, detail, ".$this->session->userdata('logged_user')['id'].", status FROM posts WHERE id=" . $post_id);
        $id = $this->m_posts->get_maxid();
        $this->db->query("INSERT INTO post_category(post_id, category_id) SELECT " . $id . ", category_id FROM post_category WHERE post_id =" . $post_id);
        $this->db->query("INSERT INTO post_tag(post_id, tag_id) SELECT " . $id . ", tag_id FROM post_tag WHERE post_id =" . $post_id);

        //print_r($this->db->queries); exit();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_complete();
            return TRUE;
        }
    }

    public function delete_post($post_id) {
        $this->db->trans_start();

        $this->db->delete("post_category", array("post_id" => $post_id));
        $this->db->delete("post_tag", array("post_id" => $post_id));
        $this->db->delete("posts", array("id" => $post_id));

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_complete();
            return TRUE;
        }
    }

    public function get_catecory_of_post($where = array(), $like = array()) {

        $q = $this->db->select("category_id")
                ->where($where)
                ->get("post_category");

        //print_r($this->db->queries); exit();

        return $q->result_array();
    }

    public function get_tag_of_post($where = array(), $like = array()) {

        $q = $this->db->select("GROUP_CONCAT(name) as tag_name")
                ->join("post_tag pt", "t.id = pt.tag_id")
                ->where($where)
                ->get("tags t");

        //print_r($this->db->queries); exit();

        return $q->row_array();
    }

}
