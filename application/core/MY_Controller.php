<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MY_Controller
 *
 * @author Pham Tan Phu
 */
class MY_Controller extends CI_Controller {

    protected $layout = array();

    public function __construct() {
        parent::__construct();
    }

}

class MY_Admin_Controller extends CI_Controller {

    public function __construct() {

        parent::__construct();
        
        //print_r($this); 

        if (!$this->session->userdata('logged_user')) {

            if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest')) {

                $info = array(
                    'status' => FALSE,
                    'message' => "Vui lòng đăng nhập!",
                    'Result' => "ERROR",
                    'Message' => "Vui lòng đăng nhập!", // Do not edit, It is like loadloginform extension jtable
                );
                echo json_encode($info);
            } else {
                redirect(base_url() . 'admin/login?ref=' . urlencode(base_url() . uri_string() . '?' . $_SERVER['QUERY_STRING']), 'location');
            }
        }
    }

}
