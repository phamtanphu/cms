<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MY_Model
 *
 * @author hieulh
 */
class MY_Model extends CI_Model {

    protected $_table = null;

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get_all($order_by = 'id ASC') {
        $query = $this->db->select($this->_table . ".*")
                ->order_by($this->_table . '.' . $order_by)
                ->get($this->_table);
				
        return ($query->num_rows() > 0)?$query->result_array(): array();
    }

    public function get_data($field = array(), $where = array(), $like = array(), $order = '', $start = '0', $limit = '') {
        if (isset($field) && is_array($field) && count($field)) {
            $this->db->select(implode(", ", $field));
        } else if (isset($field) && $field != '') {
            $this->db->select($field);
        } else {
            $this->db->select("*");
        }
		
        $this->db->where($where);
        $this->db->like($like);
		if($order !=''){
			$this->db->order_by($order);
		}
        if (isset($limit) && trim($limit) != '') {
            $query = $this->db->get($this->_table, intval($limit), intval($start));
        } else {
            $query = $this->db->get($this->_table);
        }
		//print_r($this->db->queries); exit();
        return ($query->num_rows() > 0)?$query->result_array(): array();
    }

    public function get_num_rows($where = array(), $like = array()) {
        $count = $this->db->select($this->_table . ".id")
				->where($where)
                ->like($like)
                ->count_all_results($this->_table);
		//print_r($this->db->queries); exit();
        return $count;
    }

    public function get_maxid() {
        $q = $this->db->select("MAX(id) as maxid")
                ->get($this->_table);
        return $q->row_array()['maxid'];
    }
	
	public function get_minid() {
        $q = $this->db->select("MIN(id) as minid")
                ->get($this->_table);
        return $q->row_array()['minid'];
    }

    public function create() {
        
    }

    public function insert($data = array()) {
        $query = $this->db->insert($this->_table, $data);
        return $query;
    }

    public function insert_ignore($data_batch) {
        return $query = $this->db->query("INSERT IGNORE INTO " . $this->_table . "(" . array_keys($data_batch[0]) . ") VALUES(" . implode("),(", array_map(function($arr) {
                            return implode(", ", $arr);
                        }, $data_batch)) . ")");
    }

    public function update($data = array(), $where = array()) {
        $query = $this->db->update($this->_table, $data, $where);
        return $query;
    }

    public function duplicate($field = array(), $where = array()) {

        //$this->db->_compile_select();
        $this->db->select(implode(", ", $field));
        $this->db->where($where);
        $this->db->get($this->_table);
        $sql_select = $this->db->last_query();

        $sql = "INSERT INTO " . $this->_table . "(" . implode(", ", $field) . ")(" . $sql_select . ")";
        return $this->db->query($sql);
    }

    public function delete($where = array(), $limit = NULL) {
        $query = $this->db->delete($this->_table, $where, $limit);
        return $query;
    }

    public function is_exist($where = array(), $like = array()) {
        if ($this->get_num_rows($where, $like) > 0) {
            return true;
        }
        return false;
    }

}
