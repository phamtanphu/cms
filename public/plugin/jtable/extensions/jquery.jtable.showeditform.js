(function ($) {

    //extension members
    $.extend(true, $.hik.jtable.prototype, {

        showEditForm: function(val) {
            var $row = null;
            if (typeof(val) == 'number') {
                $row = this.getRowByKey(val);
                if ($row == null)
                    throw "Invalid key.";
            } else
                $row = val;

            if (!$row.hasClass('jtable-data-row'))
                throw "This is not a valid jtable data row";

            this._showEditForm($row);
        }

    });

})(jQuery);