(function ($) {
    //extension members
    $.extend(true, $.hik.jtable.prototype, {
		/** Overrides Method **/
		_showError: function (message) {
			if(message == 'Vui lòng đăng nhập!'){
				load_login_form();
				return;
			}
            this._$errorDialogDiv.html(message).dialog('open');
        }
    });

})(jQuery);